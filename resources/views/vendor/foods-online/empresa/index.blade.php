@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Organización
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Organización</div>
        <div class="panel-body">
            @if($empresa)
                <div class="row">
                    <div class="col-xs-8 col-lg-5">
                        <img src="{{asset('img/foods-online/empresa/'.$empresa->logo)}}"
                             class="img-responsive"
                             alt="...">
                    </div>

                    <div class="form-group">
                        <b>NIT : </b>
                        {{$empresa->nit}}
                    </div>

                    <div class="form-group">
                        <b>Nombre : </b>
                        {{$empresa->nombre}}
                    </div>

                    <div class="form-group">
                        <b>Dirección : </b>
                        {{$empresa->direccion}}
                    </div>
                    <div class="form-group">
                        <b>Teléfono(s) : </b>
                        {{$empresa->telefono}}
                    </div>
                    <div class="form-group">
                        <b>Celular(es) : </b>
                        {{$empresa->celular}}
                    </div>
                    <div class="form-group">
                        <b>Sitio Web : </b>
                        <a href="http://{{$empresa->website}}" target="_blank">{{$empresa->website}}</a>
                    </div>
                    <div class="form-group">
                        {{$empresa->municipio->departamento->nombre}} -
                        {{$empresa->municipio->nombre}}
                    </div>

                    <div class="form-group">
                        <a href="{{route('empresa.edit',$empresa->id)}}" class="btn btn-warning" title="Editar">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>
            @else
                <hr>
                <div class="text-center">
                    <a href="{{ route('empresa.create') }}" class="btn btn-info">Registrar mi organización</a>
                </div>
            @endif
        </div>
    </div>
@endsection


