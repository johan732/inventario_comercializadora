@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración de productos
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Configuración de productos</div>
        <div class="panel-body">

            <a href="{{ route('compras.configuracion.productos.create') }}" class="btn btn-info">Registrar nuevo
                producto</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'compras.configuracion.productos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar producto...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($productos)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Tipo de producto</th>
                    <th>Nombre</th>
                    <th>Unidad de medida</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($productos As $producto)

                        <tr>
                            <td>{{$producto->TipoProducto->nombre}}</td>
                            <td>{{$producto->nombre}}</td>
                            <td>{{$producto->UnidadDeMedida->nombre}} - ({{ $producto->UnidadDeMedida->abreviatura }})
                            </td>
                            <td>
                                <a href="{{route('compras.configuracion.productos.edit',$producto->id)}}"
                                   class="btn btn-warning">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </a>

                                @if($producto->eliminado == 0)
                                    <a href="{{route('compras.configuracion.productos.destroy',$producto->id)}}"
                                       onclick=" return confirm('Desea deshabilitar este producto?')"
                                       class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                    </a>
                                @else
                                    <a href="{{route('compras.configuracion.productos.habilitar',$producto->id)}}"
                                       onclick=" return confirm('Desea habilitar este producto?')"
                                       class="btn btn-success">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                        </span>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $productos->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado productos
                </p>
            @endif
        </div>
    </div>
@endsection


