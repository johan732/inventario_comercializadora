@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear Producto
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('compras.configuracion.productos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear Producto</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'compras.configuracion.productos.store', 'method' => 'POST','files' => true]) !!}

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('compras_tipo_producto_id','*Tipo de producto') !!}
                            {!! Form::select('compras_tipo_producto_id',$tipoProducto,null,['class' => 'form-control select-padre', 'id'=>'select-departamento','placeholder' => 'seleccione una opción', 'required']) !!}
                        </div>


                        <div class="form-group">
                            {!! Form::label('nombre','*Nombre del producto') !!}
                            {!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre del producto...', 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('general_unidad_medida_id','*Unidad de medida') !!}
                            {!! Form::select('general_unidad_medida_id',$unidadesDeMedida,null,['class' => 'form-control select-padre', 'id'=>'select-departamento','placeholder' => 'seleccione una opción', 'required']) !!}
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>

    <script>
        /**
         * Función que habilita o deshabilita el campo de min ymax
         * @param idCheckbox
         */
        function habilitarCamposRango(idCheckbox) {
            document.getElementById(idCheckbox).onchange = function () {
                document.getElementById('min' + idCheckbox).disabled = !this.checked;
                document.getElementById('max' + idCheckbox).disabled = !this.checked;
            };
        }
    </script>
@endsection

