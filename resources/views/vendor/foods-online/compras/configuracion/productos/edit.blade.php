@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Editar Producto
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('compras.configuracion.productos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Editar Producto</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['compras.configuracion.productos.update',$producto], 'method'=>'PUT','files' => true]) !!}

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('compras_tipo_producto_id','*Tipo de producto') !!}

                            <select name="compras_tipo_producto_id" class='form-control select-padre'
                                    id='select-departamento' placeholder='seleccione una opción' required>
                                <option></option>
                                @foreach($tipoProductos As $tipoProducto)
                                    <option value="{{$tipoProducto->id}}"
                                            @if($tipoProducto->eliminado == 1) disabled @endif
                                            @if($tipoProducto->id == $producto->compras_tipo_producto_id) selected @endif>
                                        {{$tipoProducto->nombre}}
                                        @if($tipoProducto->eliminado == 1) (Deshabilitado) @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            {!! Form::label('nombre','*Nombre del producto') !!}
                            {!! Form::text('nombre',$producto->nombre,['class' => 'form-control', 'placeholder' => 'Nombre del producto...', '']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('general_unidad_medida_id','*Unidad de medida') !!}
                            {!! Form::select('general_unidad_medida_id',$unidadesDeMedida,$producto->general_unidad_medida_id,['class' => 'form-control select-padre', 'id'=>'select-departamento','placeholder' => 'seleccione una opción', 'required']) !!}
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>

    <script>
        function habilitarCamposRango(idCheckbox) {
            document.getElementById(idCheckbox).onchange = function () {
                document.getElementById('min' + idCheckbox).disabled = !this.checked;
                document.getElementById('max' + idCheckbox).disabled = !this.checked;
            };
        }
    </script>
@endsection

