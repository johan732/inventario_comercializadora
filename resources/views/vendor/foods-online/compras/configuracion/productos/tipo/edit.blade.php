@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración de análisis tipo de producto
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('compras.configuracion.productos.tipo.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Configuración de análisis de tipo de producto</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['compras.configuracion.productos.tipo.update',$tipoProducto], 'method'=>'PUT','files' => true]) !!}

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('compras_tipo_producto_id','Nombre tipo de producto:') !!}
                            {!! Form::text('nombre',$tipoProducto->nombre,['class' => 'form-control', 'placeholder' => 'Nombre tipo de producto ...', 'required']) !!}
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>
@endsection

