@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración tipos de productos
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Configuración tipos de productos</div>
        <div class="panel-body">

            <a href="{{ route('compras.configuracion.productos.tipo.create') }}" class="btn btn-info">
                Registrar nuevo tipo de producto
            </a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'compras.configuracion.productos.tipo.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar tipo de producto...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($tipoProductos)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Tipo de producto</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($tipoProductos As $tipoProducto)

                        <tr>
                            <td>{{$tipoProducto->nombre}}</td>
                            <td>
                                <a href="{{route('compras.configuracion.productos.tipo.edit',$tipoProducto->id)}}"
                                   class="btn btn-warning">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </a>
                                @if($tipoProducto->eliminado == null)
                                    <a href="{{route('compras.configuracion.productos.tipo.destroy',$tipoProducto->id)}}"
                                       onclick=" return confirm('Desea deshabilitar este tipo de producto?')"
                                       class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                    </a>
                                @else
                                    <a href="{{route('compras.configuracion.productos.tipo.habilitar',$tipoProducto->id)}}"
                                       onclick=" return confirm('Desea habilitar este producto?')"
                                       class="btn btn-success">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                        </span>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $tipoProductos->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado tipos de productos
                </p>
            @endif
        </div>
    </div>
@endsection


