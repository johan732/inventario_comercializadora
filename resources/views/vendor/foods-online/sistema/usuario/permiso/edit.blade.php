@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Permisos por rol
@endsection


@section('main-content')

    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('permiso.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Actualizar permisos</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['permiso.update',$rol], 'method'=>'PUT']) !!}

                <div class="form-group">
                    <h2 class="text-center">
                        {{$rol->nombre}}
                    </h2>
                </div>


                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Seleccione los enlaces a los cuales desea dar permisos
                        </div>

                        <div class="panel-body">
                            {!! $menu !!}
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::submit('Actualizar',['class' => 'btn btn-success'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-rol').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>

    <!--
        Esta función JQuery consiste en cambiar el estado de un padre o hijo
        * Si se selecciona un hijo debe marcarse su padre
        * si se selecciona un padre deden marcarse los hijos
        -->
    <script>
        $(function () {
            $(".autoCheckbox").on("click",function () {
                var expr="li input[type=checkbox]",$this=$(event.target);
                while ($this.length) {
                    $input=$this.closest("li").find(expr);
                    if ($input.length) {
                        if ($this[0]==event.target) {
                            checked = $this.prop("checked");
                            $input.prop("checked", checked).css("opacity","1.0");
                        }
                        checked=$input.is(":checked");
                        $this.prop("checked", checked).css("opacity",
                            (checked && $input.length!= $this.closest("li").find(expr+":checked").length)
                                ? "1.0" : "1.0");
                    }
                    $this=$this.closest("ul").closest("li").find(expr.substr(3)+":first");
                }
            });
        })
    </script>
@endsection