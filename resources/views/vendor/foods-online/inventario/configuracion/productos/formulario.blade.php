<!-- /.col -->
<div class="col-md-9">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#configuracion" data-toggle="tab">Configuración</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="configuracion">

                {!! Form::open(['route' => ['inventario.configuracion.productos.update',$producto], 'method'=>'PUT','files'=>true]) !!}


                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('unidadesxcaja','*Unidades por caja') !!}
                            {!! Form::number('unidadesxcaja',$configuracionProducto ? $configuracionProducto->unidadesxcaja : null,['class' => 'form-control','min'=>0,'placeholder' => 'Unidades por caja']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('stock_minimo','*Stock mínimo') !!}
                            {!! Form::number('stock_minimo',$configuracionProducto ? $configuracionProducto->stock_minimo : null,['class' => 'form-control','step'=>'any','min'=>0,'placeholder' => 'Stock mínimo', 'required']) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group text-center">
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}

            </div>

        </div>
    </div>
</div>
