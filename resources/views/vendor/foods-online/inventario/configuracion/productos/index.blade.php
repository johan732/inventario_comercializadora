@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración de productos de inventarios
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Configuración de productos de inventario</div>
        <div class="panel-body">

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'inventario.configuracion.productos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar producto...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($productos)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Tipo de producto</th>
                    <th>Nombre</th>
                    <th>Unidad de medida</th>
                    <th>Ficha técnica</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($productos As $producto)

                        <tr>
                            <td>{{$producto->TipoProducto->nombre}}</td>
                            <td>{{$producto->nombre}}</td>
                            <td>{{$producto->UnidadDeMedida->nombre}} - ({{ $producto->UnidadDeMedida->abreviatura }})
                            </td>
                            <td>
                                @if($producto->ficha_tecnica_pdf != '')
                                    <a href="{{asset('docs/compras/configuracion/productos/fichas_tecnicas/'.$producto->ficha_tecnica_pdf)}}"
                                       target="_blank">
                                        Ficha técnica {{$producto->nombre}}
                                    </a>
                                @else
                                    ---
                                @endif
                            </td>
                            <td>
                                <a href="{{route('inventario.configuracion.productos.edit',$producto->id)}}"
                                   class="btn btn-warning">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $productos->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado productos
                </p>
            @endif
        </div>
    </div>
@endsection


