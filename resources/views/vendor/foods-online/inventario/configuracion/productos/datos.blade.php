<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Datos del producto -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    {{--<img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">--}}

                    <h3 class="profile-username text-center">{{$producto->nombre}}</h3>

                    <p class="text-muted text-center">{{$producto->TipoProducto->nombre}}</p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Tipo: </b>
                            <label class="pull-right">{{$producto->TipoProducto->nombre}}</label>
                        </li>
                        <li class="list-group-item">
                            <b>Unidad de medida: </b>
                            <label class="pull-right">{{$producto->UnidadDeMedida->nombre}}</label>
                        </li>

                        <li class="list-group-item">
                            <b>Ficha técnica: </b>
                            @if($producto->ficha_tecnica_pdf != '')
                                <a class="pull-right"
                                   href="{{asset('docs/compras/configuracion/productos/fichas_tecnicas/'.$producto->ficha_tecnica_pdf)}}"
                                   target="_blank">
                                    ver
                                </a>
                            @endif
                        </li>
                        <li class="list-group-item">
                            <b>Habilitado </b>
                            <label class="pull-right">
                                @if(!$producto->eliminado)
                                    <span class="label label-success">Habilitado</span>
                                @else
                                    <span class="label label-danger">No habilitado</span>
                                @endif
                            </label>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

    @include('vendor.foods-online.inventario.configuracion.productos.formulario')
    <!-- /.col -->
    </div>
    <!-- /.row -->

    <div>

    </div>
</section>