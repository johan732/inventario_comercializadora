@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Editar Producto
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('inventario.configuracion.productos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Editar Producto</div>
        <div class="panel-body">
            <div class="form-group">

                @include('vendor.foods-online.inventario.configuracion.productos.datos')
            </div>
        </div>
    </div>
@endsection


