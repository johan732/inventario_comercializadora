@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración de bodegas
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Configuración deb bodegas</div>
        <div class="panel-body">

            <a href="{{ route('inventario.configuracion.bodegas.create') }}" class="btn btn-info">Registrar nuevo
                tipo de bodegas</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'inventario.configuracion.bodegas.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar bodega...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($bodegas)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Nombre</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($bodegas As $bodega)

                        <tr>
                            <td>{{$bodega->nombre}}</td>
                            <td>
                                <div class="text-center">
                                    <a href="{{route('inventario.configuracion.bodegas.edit',$bodega->id)}}"
                                       class="btn btn-warning">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    </a>

                                    @if($bodega->habilitado == null)
                                        <a href="{{route('inventario.configuracion.bodegas.destroy',$bodega->id)}}"
                                           onclick=" return confirm('Desea deshabilitar esta bodegas?')"
                                           class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                        </a>
                                    @else
                                        <a href="{{route('inventario.configuracion.bodegas.habilitar',$bodega->id)}}"
                                           onclick=" return confirm('Desea habilitar esta bodegas?')"
                                           class="btn btn-success">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                        </span>
                                        </a>
                                    @endif
                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $bodegas->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado bodegas
                </p>
            @endif
        </div>
    </div>
@endsection


