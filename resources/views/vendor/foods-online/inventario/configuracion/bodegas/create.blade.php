@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear bodega
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('inventario.configuracion.bodegas.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear bodega</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'inventario.configuracion.bodegas.store', 'method' => 'POST']) !!}

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('nombre','* Nombre de la bodega') !!}
                            {!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre de la bodega...', 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('inventario_tipo_almacenamiento_id','*Tipo de almacenamiento') !!}
                            {!! Form::select('inventario_tipo_almacenamiento_id',$tiposAlmacenamiento,null,['class' => 'form-control select-padre','id'=>'select-tipo-producto','placeholder' => '']) !!}
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('habilitado',1) !!}
                                    Deshabilitada
                                </label>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Registrar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado ",
            disable_search_threshold: 0
        });
    </script>
@endsection

