@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear tipo de almacenamiento para bodegas
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('inventario.configuracion.almacenamiento.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear tipo de almacenamiento para bodegas</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['inventario.configuracion.almacenamiento.update',$tipoAlmacenamiento], 'method'=>'PUT']) !!}

                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="form-group">
                            {!! Form::label('nombre','* Nombre del tipo de almacenamiento') !!}
                            {!! Form::text('nombre',$tipoAlmacenamiento->nombre,['class' => 'form-control', 'placeholder' => 'Nombre del tipo de almacenamiento...', 'required']) !!}
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('eliminado',1,$tipoAlmacenamiento->habilitado) !!}
                                    deshabilitado
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

