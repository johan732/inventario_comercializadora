@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración de tipo de almacenamiento para bodegas
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Configuración de tipo de almacenamiento para bodegas</div>
        <div class="panel-body">

            <a href="{{ route('inventario.configuracion.almacenamiento.create') }}" class="btn btn-info">Registrar nuevo
                tipo de almacenamiento</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'inventario.configuracion.almacenamiento.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar tipo de almacenamiento...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($tiposAlmacenamiento)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Nombre</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($tiposAlmacenamiento As $tipoAlmacenamiento)

                        <tr>
                            <td>{{$tipoAlmacenamiento->nombre}}</td>
                            <td>
                                <div class="text-center">
                                    <a href="{{route('inventario.configuracion.almacenamiento.edit',$tipoAlmacenamiento->id)}}"
                                       class="btn btn-warning">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    </a>

                                    @if($tipoAlmacenamiento->habilitado == null)
                                        <a href="{{route('inventario.configuracion.almacenamiento.destroy',$tipoAlmacenamiento->id)}}"
                                           onclick=" return confirm('Desea deshabilitar este tipo de almacenamiento?')"
                                           class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                        </a>
                                    @else
                                        <a href="{{route('inventario.configuracion.almacenamiento.habilitar',$tipoAlmacenamiento->id)}}"
                                           onclick=" return confirm('Desea habilitar este tipo de almacenamiento?')"
                                           class="btn btn-success">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                        </span>
                                        </a>
                                    @endif
                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $tiposAlmacenamiento->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado tipos de almacenamiento
                </p>
            @endif
        </div>
    </div>
@endsection


