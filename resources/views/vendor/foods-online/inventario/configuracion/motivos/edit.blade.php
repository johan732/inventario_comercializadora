@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Crear motivo de movimiento
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('inventario.configuracion.motivos.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Crear motivo de movimiento</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => ['inventario.configuracion.motivos.update',$motivo], 'method'=>'PUT']) !!}

                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="form-group">
                            {!! Form::label('nombre','Nombre del motivo de movimiento *') !!}
                            {!! Form::text('nombre',$motivo->nombre,['class' => 'form-control', 'placeholder' => 'Nombre del motivo de movimiento...', 'required']) !!}
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('habilitado',1,$motivo->eliminado) !!}
                                    Deshabilitado
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

