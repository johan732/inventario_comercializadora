@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración de motivos de movimiento
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Configuración de motivos</div>
        <div class="panel-body">

            <a href="{{ route('inventario.configuracion.motivos.create') }}" class="btn btn-info">Registrar motivo de
                movimiento</a>

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'inventario.configuracion.motivos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar motivo...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($motivos)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Nombre</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($motivos As $motivo)

                        <tr>
                            <td>{{$motivo->nombre}}</td>
                            <td>
                                <a href="{{route('inventario.configuracion.motivos.edit',$motivo->id)}}"
                                   class="btn btn-warning">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </a>

                                @if($motivo->habilitado == null)
                                    <a href="{{route('inventario.configuracion.motivos.destroy',$motivo->id)}}"
                                       onclick=" return confirm('Desea deshabilitar este motivo?')"
                                       class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                        </span>
                                    </a>
                                @else
                                    <a href="{{route('inventario.configuracion.motivos.habilitar',$motivo->id)}}"
                                       onclick=" return confirm('Desea habilitar este motivo?')"
                                       class="btn btn-success">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                        </span>
                                    </a>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $motivos->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado motivos de movimiento
                </p>
            @endif
        </div>
    </div>
@endsection


