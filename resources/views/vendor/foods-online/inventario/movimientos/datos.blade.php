<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Datos del producto -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    {{--<img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">--}}

                    <h3 class="profile-username text-center">Tipo de movimiento</h3>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                {{--Se crean los 3 tipos de movimientos para el inventario:
                                    1. Entrada
                                    2. Salida
                                    3. Traslado--}}
                                <div class="radio">
                                    <label>
                                        {!! Form::radio('tipo_movimiento', 1,null,['class'=>'radio-movimiento','required']); !!}
                                        Entrada
                                    </label>
                                </div>

                                <div class="radio">
                                    <label>
                                        {!! Form::radio('tipo_movimiento', 2,null,['class'=>'radio-movimiento','required']); !!}
                                        Salida
                                    </label>
                                </div>

                                <div class="radio">
                                    <label>
                                        {!! Form::radio('tipo_movimiento', 3,null,['class'=>'radio-movimiento','required']); !!}
                                        Traslado
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="form-group">
                {!! Form::label('observaciones','Observaciones') !!}
                {!! Form::textarea('observaciones',null,['class' => 'form-control textarea-content']) !!}
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    @include('vendor.foods-online.inventario.movimientos.formulario')
    <!-- /.col -->
    </div>
    <!-- /.row -->

    <div>

    </div>
</section>