<!-- /.col -->
<div class="col-md-9">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#configuracion" data-toggle="tab">Datos de envio </a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="configuracion">


                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('inventario_bodegas_id_origen','*Bodega',['id'=>'label_bodega_origen']) !!}
                            {!! Form::select('inventario_bodegas_id_origen',$bodegas,null,['class' => 'form-control select-padre','id'=>'select_bodega_origen','placeholder' => 'Selecciona una opción']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('compras_tipo_producto_id','*Tipo de producto ') !!}
                            {!! Form::select('compras_tipo_producto_id',$tipoProducto,null,['class' => 'form-control select-padre','id'=>'select_tipo_producto','placeholder' => 'Selecciona una opción']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('compras_productos_id','*Producto ') !!}
                            {!! Form::select('compras_productos_id',[],null,['class' => 'form-control select-padre','id'=>'select_producto','placeholder' => 'Selecciona una opción']) !!}
                        </div>

                        <div class="form-group hide" id="div_campo_precio">
                            {!! Form::label('precio','*Precio ') !!}
                            {!! Form::text('precio',null,['class' => 'form-control','id'=>'campo_precio', 'placeholder' => '$0.00']) !!}
                        </div>

                        <div class="form-group" id="div_lista_precio">
                            {!! Form::label('precio','*Precio ') !!}
                            {!! Form::select('precio',[],null,['class' => 'form-control select-padre','id'=>'select_precio','placeholder' => 'Selecciona una opción']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('cantidad','*Cantidad ',['id'=>'label_cantidad']) !!}
                            {!! Form::number('cantidad',null,['class' => 'form-control', 'id'=>'campo_cantidad','placeholder' => 'Cantidad...', 'required','min'=>1]) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('inventario_motivos_movimiento_id','*Motivo movimiento ') !!}
                            {!! Form::select('inventario_motivos_movimiento_id',$motivosMovimiento,null,['class' => 'form-control select-padre','id'=>'select_motivo','placeholder' => 'Selecciona una opción']) !!}
                        </div>

                        <div class="form-group" id="div_bodega_destino">
                            {!! Form::label('inventario_bodegas_id_destino','*Bodega destino') !!}
                            {!! Form::select('inventario_bodegas_id_destino',$bodegas,null,['class' => 'form-control select-padre','id'=>'select_bodega_destino','required'=>'required']) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group text-center">
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary'])!!}
                </div>


            </div>

        </div>
    </div>
</div>
