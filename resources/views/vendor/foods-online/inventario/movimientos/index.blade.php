@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Movimientos
@endsection

@section('main-content')

    <div class="panel panel-default">
        <div class="panel-heading">Realizar movimiento en inventario</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'inventario.movimientos.guardar', 'method' => 'POST']) !!}
                @include('vendor.foods-online.inventario.movimientos.datos')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        /**
         * Oculta el campo de la bodega destino al cargar la página, se hace esto debido a que presenta problemas al poner la clase hide
         * al div que contiene la lista
         * */
        $(document).ready(function () {
            /** Agregamos la clase que oculta el campo para seleccionar la bodega destino **/
            $("#div_bodega_destino").addClass("hide");
        });
    </script>

    <script>
        /**
         * Reinicia los valores seleccionados en una lista
         * @param id el id de la lista que queremos reiniciar
         * */
        function reiniciarLista(id) {
            $("#" + id).prop('selectedIndex', -1);
            $('#' + id).trigger("chosen:updated");
        }

        /**
         * Permite asignar los campos correspondientes dependiendo del tipo de movimiento que desea realizar
         *  1.- Entrada
         *  2.- Salida
         *  3.- Traslado
         * */
        $(".radio-movimiento").change(function () {

            /** ------------------ REINICIO DE CAMPOS CUANDO CAMBIAN -------------------------  **/
            /** Quitamos el máximo valor que puede tener el producto**/
            $("#campo_cantidad").removeAttr("max");
            /** Cambiamos el html del label de la bodega**/
            $("#label_bodega_origen").html("*Bodega");
            /** Cambiamos el html del label de cantidad**/
            $("#label_cantidad").html("*Cantidad");
            /** Ponemos el campo cantidad sin ningún valor **/
            $("#campo_cantidad").val(null);
            /** Ponemos el campo del precio sin ningún valor **/
            $("#campo_precio").val(null);

            /** Agregamos la clase que oculta el campo para seleccionar la bodega destino **/
            $("#div_bodega_destino").addClass("hide");
            /** Deshabilitamos la lista que contiene la bodega de destino**/
            $("#select_bodega_destino").prop("disabled", true).trigger("chosen:updated");

            /** Deshabilitamos el campo para escribir el precio**/
            $("#campo_precio").prop("disabled", true);

            /** Deshabilitamos la lista que contiene precios **/
            $("#select_precio").prop("disabled", true).trigger("chosen:updated");

            /** reiniciar todas las listas **/
            reiniciarLista('select_bodega_origen');
            reiniciarLista('select_tipo_producto');
            reiniciarLista('select_producto');
            reiniciarLista('select_precio');
            reiniciarLista('select_motivo');
            reiniciarLista('select_bodega_destino');

            /** ------------------ ///////////////////////////////// -------------------------  **/

            /** Obtenemos el valor del checkbox marcado **/
            var valorTipoMovimiento = $(this).val();
            /**
             * Si es una entrada a inventario, debemos mostrar el campo para digitar el precio y la cantidad, la cantidad no
             * tendrá un limite máximo para el ingreso
             */
            if (valorTipoMovimiento == 1) {
                /** Quitamos la clase que oculta el campo para digitar el precio*/
                $("#div_campo_precio").removeClass("hide");
                /** Deshabilitamos el campo para escribir el precio**/
                $("#campo_precio").prop("disabled", false);
                /** Ponemos el campo para digitar el precio requerido**/
                $("#campo_precio").prop("required", true);

                /** Agregamos la clase que oculta la lista de precios**/
                $("#div_lista_precio").addClass("hide");

            }
            /**
             * Si es una salida, solo se permite seleccionar un precio y el campo de cantidad tiene un tope max que es el valor
             * que se encuentra en inventario.
             * */
            else if (valorTipoMovimiento == 2) {
                /** Agregamos la clase que oculta el campo del precio  **/
                $("#div_campo_precio").addClass("hide");

                /** Quitamos la clase que oculta el campo para seleccionar un precio **/
                $("#div_lista_precio").removeClass("hide");
                /** Deshabilitamos la lista que contiene precios **/
                $("#select_precio").prop("disabled", false).trigger("chosen:updated");
                /** Deshabilitamos el campo del precio**/
                $("#div_campo_precio").prop("disabled", true);
                /** Ponemos la lista para seleccionar el precio como requerida  */
                $("#select_precio").prop("required", true);

            } else if (valorTipoMovimiento == 3) {
                /** Agregamos la clase que oculta el campo del precio  **/
                $("#div_campo_precio").addClass("hide");

                /** Quitamos la clase que oculta el campo para seleccionar un precio **/
                $("#div_lista_precio").removeClass("hide");
                /** Deshabilitamos la lista que contiene precios **/
                $("#select_precio").prop("disabled", false).trigger("chosen:updated");
                /** Ponemos la lista para seleccionar el precio como requerida  */
                $("#select_precio").prop("required", true);

                /** Quitamos la clase que oculta el campo para seleccionar la bodega destino **/
                $("#div_bodega_destino").removeClass("hide");
                /** Habilitamos la lista que contiene la bodega de destino**/
                $("#select_bodega_destino").prop("disabled", false).trigger("chosen:updated");
                /** Ponemos la lista para seleccionar el precio como requerida  */
                $("#select_bodega_destino").prop("required", true);
                /** Cambiamos el html del label de la bodega para indicar que será la bodega de origen en caso de un traslado**/
                $("#label_bodega_origen").html("*Bodega origen");
            }
        });
    </script>

    <script>
        /**
         * Script para el cambio de las listas dependientes
         * Se ejecuta cuando se selecciona un tipo producto, carga los productos del tipo seleccionado en otra lista
         */
        $("#select_tipo_producto").change(function () {

            /** Obtenemos los valores de la lista multiple y los pasamos a la función **/
            var selectedValues = $(this).val();
            /** Método que carga la lista dependiende:
             * 1.- Lista del cual toma el valor
             * 2.- Lista donde se pondrán los nuevos valores
             * 3.- Ruta de donde tomará los valores**/
            onSelectDependiente('select_tipo_producto', 'select_producto', 'productos/')

        });
    </script>

    <script>
        /**
         * Script para el cambio de las listas dependientes
         * Se ejecuta cuando se selecciona un producto, carga los precio que se encuentran del producto en la bodega seleccionada
         */
        $("#select_producto, #select_bodega_origen").change(function () {

            /** Obtenemos los valores de la lista multiple y los pasamos a la función **/
            var valueBodega = $('#select_bodega_origen').val();
            var valueProducto = $('#select_producto').val();

            /** Llamamos la función que cargará los precios solo cuando tengamos un valor de bodega y de producto**/
            if (valueBodega && valueProducto) {
                /** Método que carga la lista dependiende:
                 * 1.- valores que se pasan en la ruta (ejemplo: ruta/valor1/valor2/...../..)
                 * 2.- Lista donde se pondrán los nuevos valores
                 * 3.- Ruta de donde tomará los valores**/
                onSelectDependiente2(valueBodega + '/' + valueProducto, 'select_precio', 'precios/');
            }
        });
    </script>
    <script>
        /**
         * Script para el cambio de las listas dependientes
         * Se ejecuta cuando se selecciona un precio, pone la cantidad máxima que hay en inventario para realizar un traslado o realizar
         * un movimiento de salida
         */
        $("#select_precio").change(function () {

            /** Obtenemos los valores de la lista multiple y los pasamos a la función **/
            var valueprecio = $('#select_precio').val();

            /** Llamamos la función que cargará los precios solo cuando tengamos un valor de bodega y de producto**/
            if (valueprecio) {
                /**
                 * Obtenemos los valores de la ruta
                 */
                $.get('cantidad/' + valueprecio, function (data) {
                    /** Agregamos el atributo de MAX al campo cantidad para no permitir un valor mayor al que se encuentra en inventario **/
                    $("#campo_cantidad").attr("max", data.cantidad);
                    $("#label_cantidad").html("*Cantidad (Disponible: " + data.cantidad + ")");
                });
            }
        });
    </script>
@endsection


