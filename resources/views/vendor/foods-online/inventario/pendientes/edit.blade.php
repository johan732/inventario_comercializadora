@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Ingreso de orden de compra aprobada
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('inventario.pendientes.index')}}" class="btn btn-default">
                Volver
            </a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            Ingreso de orden de compra aprobada
        </div>

        <div class="panel-body">
            <!-- Incluimos los datos del solicitante de la orden de compra-->
            @include('vendor.foods-online.compras.ordenes.solicitante')

            {{--<!-- Incluimos los datos de la cotización -->--}}
            @include('vendor.foods-online.compras.ordenes.datos')

             {{--Incluimos el formulario para el ingreso a inventario--}}
            @include('vendor.foods-online.inventario.pendientes.formularioIngreso')
        </div>
    </div>
@endsection
