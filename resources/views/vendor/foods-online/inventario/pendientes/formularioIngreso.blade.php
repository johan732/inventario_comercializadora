<!-- Formulario para guardar las cantidades recibidas en inventario  -->
{!! Form::open(['route' => ['inventario.pendientes.guardar',$orden], 'method'=>'POST']) !!}
<table class="table table-bordered">
    <thead>
    <tr class="info">
        <th class="text-center">Estado</th>
        <th>Certificado</th>
        <th>Referencia</th>
        <th>Tipo</th>
        <th>Producto</th>
        <th>Lote</th>
        <th>Cantidad</th>
        <th>Bodega destino</th>
        <th>Precio</th>
        <th>Total</th>
    </tr>
    </thead>

    <tbody>
    @php
        /** Guarda el valor total de toda la orden recibida **/
        $totalValorOrden = 0;
        $contador=-1;
    @endphp

    <!-- Leemos todos los productos de la cotización -->
    @foreach($orden->Productos As $producto)
        @php
            /** Guarda la cantidad de lotes recibidos por producto**/
            $totalLotes = $producto->Recibido->where('estado',3)->count();

            /** Controla el span de los datos en común de los lotes**/
            $controlSpan = 0;
        @endphp

        {{-- Leemos todos los lotes diferentes que se recibieron y están conformes por calidad --}}
        @foreach($producto->Recibido->where('estado',3)->where('certificado','!=',null) As $key => $productoRecibido)
            @php
                $contador++;
                    /** Acumula el valor por lote -> precio*cantidad**/
                    $totalValorOrden+=$producto->precio * $productoRecibido->cantidad;
            @endphp

            {{-- Campo oculto con el id de recibido para guardar cada registro relacionado con lo recibido  --}}
            {!! Form::hidden('compras_ordenes_recibido_id['.$contador.']',$productoRecibido->id,null) !!}

            {{-- Campo oculto con el id del tipo de producto para guardar cada registro por lote   --}}
            {!! Form::hidden('compras_tipo_producto_id['.$contador.']',$producto->TipoProducto->id,null) !!}

            {{-- Campo oculto con el id del producto para guardar cada registro por lote   --}}
            {!! Form::hidden('compras_productos_id['.$contador.']',$producto->id,null) !!}
            <tr>
                <td>
                    <div class="text-center">
                        @include('vendor.foods-online.compras.ordenes.recibir.estadoProducto')
                    </div>
                </td>

                <td>
                    <div class="text-center">
                        {{-- Comprobamos si  el lote tiene certificado de calidad para mostrarlo,
                        de lo contrario mostramos el botón para subirlo --}}
                        @if($productoRecibido ->certificado)
                            <a href="{{asset('/docs/compras/ordenes/certificado_calidad/orden_'.$orden->id.'/'.$productoRecibido->certificado)}}"
                               target="_blank">
                                Certificado
                            </a>
                        @else
                            -
                        @endif
                    </div>
                </td>

                <!--Comprobamos que sea el primer Lote que se va a leer para mostrar la celda con el rowspan -->
                @if(!$controlSpan)
                    <td rowspan="{{$totalLotes}}" class="aling-middle text-center">
                        {{$producto->referencia ? $producto->referencia: "-"}}
                    </td>
                    <td rowspan="{{$totalLotes}}" class="aling-middle text-center">
                        {{$producto->TipoProducto->nombre}}
                    </td>
                    <td rowspan="{{$totalLotes}}" class="aling-middle text-center">
                        {{$producto->Producto->nombre}}
                    </td>
                @endif


                <td>
                    <div class="text-center">
                        {{$productoRecibido->lote}}
                    </div>
                </td>

                <td>
                    <div class="text-center">
                        {{$productoRecibido->cantidad}}
                    </div>
                </td>

                <td>
                    {!! Form::select('inventario_bodegas_id['.$contador.']',$bodegas,null,['class' => 'form-control select-padre','placeholder' => '','required']) !!}
                </td>

                {{--<td class="text-center">--}}
                {{--{!! Form::text('cantidad['.$key.']',$productoRecibido->cantidad,--}}
                {{--['class' => 'form-control cantidad'.$producto->id,'placeholder'=>'Cantidad...', 'min'=>'0','max'=>$productoRecibido->cantidad,'required']) !!}--}}
                {{--</td>--}}


                {{-- Comprobamos que sea el primer Lote que se va a leer para mostrar la celda con el rowspan
                    y sumamos 1 cuando comprube para que muestre los datos en comùn que tienen los lotes una sola vez --}}
                @if(!$controlSpan++)
                    <td rowspan="{{$totalLotes}}" class="aling-middle text-center">
                        $ {{number_format($producto->precio)}}
                    </td>
                @endif
                <td class="aling-middle text-left">
                    <label class="text-bold" id="labelTotal{{$producto->id}}">
                        $ {{number_format($producto->precio * $productoRecibido->cantidad)}}
                    </label>
                </td>
            </tr>
        @endforeach

    @endforeach

    <tr>
        <td class="text-right text-bold" colspan="9">
            Total
        </td>
        <td class="text-bold text-left">
            <label class="text-bold">
                $ {{number_format($totalValorOrden)}}
            </label>
        </td>
    </tr>

    </tbody>
</table>

<div class="form-group text-center">
    {!! Form::submit('Ingresar a inventario',['class' => 'btn btn-primary'])!!}
</div>

<!--- Formulario para guardar los certificados -->
{!! Form::close() !!}
