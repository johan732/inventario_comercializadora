@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Pendientes ingreso a inventario
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Pendientes por ingreso a inventario</div>
        <div class="panel-body">

            <!-- BUSCADOR DE TAGS -->
            {!! Form::open(['route' => 'inventario.pendientes.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('buscar',null, ['class' => 'form-control', 'placeholder' => 'buscar orden...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
            </div>
            {!! Form::close() !!}
        <!-- FIN DEL BUSCADOR -->

            @if(count($ordenes)>0)
                <table class="table table-hover">
                    <thead>
                    <th>Estado</th>
                    <th>Orden N°</th>
                    <th>Fecha de la orden</th>
                    <th>Proveedor</th>
                    <th>Fecha de entrega</th>
                    <th>Solicitante</th>
                    <th>Crédito</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($ordenes As $orden)

                        @foreach($orden->Productos As $producto)
                        @endforeach
                        <tr>
                            <td>
                                @include('vendor.foods-online.compras.ordenes.estadosOrden')
                            </td>
                            <td>{{$orden->id}}</td>
                            <td>{{$orden->fecha_orden}}</td>
                            <td>{{$orden->Proveedor->razon_social}}</td>
                            <td>{{$orden->fecha_entrega ? $orden->fecha_entrega : "-"}}</td>
                            <td>{{$orden->User->name}}</td>
                            <td>{{$orden->credito ? "SI":"NO"}}</td>

                            <td>
                                <a href="#" class="btn btn-default" data-toggle="modal"
                                   data-target="#modalVerCotizacion{{$orden->id}}">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true">
                                        </span>
                                </a>

                                <!-- Modal -->
                                <div class="modal fade" id="modalVerCotizacion{{$orden->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                    Datos de la orden de compra
                                                </h4>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Incluimos el resumen de toda la orden de compra  -->
                                                @include('vendor.foods-online.compras.ordenes.resumen')

                                                {{--Comprobamos si la cotización fue aceptada
                                                para pedir la fecha prevista de entrega que se acuerda con el proveedor--}}
                                                @include('vendor.foods-online.compras.ordenes.fechaEntrega')
                                            </div><!-- Close modal-body -->

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancelar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Botón ingreso de resultados-->
                                <a href="{{route('inventario.pendientes.ingresar',$orden->id)}}"
                                   class="btn btn-warning" title="Ingresar resultados">
                                    <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $ordenes->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado ordenes de compra
                </p>
            @endif
        </div>
    </div>
@endsection


