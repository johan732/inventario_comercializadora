@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Configuración de productos
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Movimientos inventario</div>
        <div class="panel-body">

            @if(count($movimientosInventario)>0)
                <table class="table table-stripped">
                    <thead>
                    <th>Tipo Mov</th>
                    <th>Fecha</th>
                    <th>Producto</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                    <th>Precio prom.</th>
                    <th>Bod. Orig</th>
                    <th>Bod. Dest</th>
                    <th>Observaciones</th>
                    <th>Responsable</th>
                    <th></th>
                    </thead>

                    <tbody>
                    @foreach($movimientosInventario As $movimientoInventario)

                        @php
                            $label="label-danger";
                            $tipo="No definido";

                                if ($movimientoInventario->tipo_movimiento == 1) {
                                   $label = "label-primary";
                                   $tipo ="Entrada";
                               } else if ($movimientoInventario->tipo_movimiento == 2) {
                                   $label = "label-default";
                                   $tipo ="Salida";
                               } else if ($movimientoInventario->tipo_movimiento == 3) {
                                   $label = "label-success";
                                   $tipo ="Traslado";
                               }
                        @endphp
                        <tr>
                            <td><span class="label {{$label}}">{{$tipo}}</span></td>
                            <td>{{$movimientoInventario->created_at->toDateString()}}</td>
                            <td>{{$movimientoInventario->Producto->nombre}}</td>
                            <td>${{$movimientoInventario->precio}}</td>
                            <td>{{$movimientoInventario->cantidad}}</td>
                            <td>${{$movimientoInventario->total}}</td>
                            <td>${{$movimientoInventario->precio_promedio}}</td>
                            <td>${{$movimientoInventario->BodegaOrigen->nombre}}</td>
                            <td>
                                {{$movimientoInventario->tipo_movimiento == 3 ? $movimientoInventario->BodegaDestino->nombre : "-"}}</td>
                            <td>{{$movimientoInventario->observaciones}}</td>
                            <td>{{$movimientoInventario->User->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $movimientosInventario->render() !!}
            @else
                <hr>
                <p class="bg-warning text-center">
                    No se han registrado productos
                </p>
            @endif
        </div>
    </div>
@endsection


