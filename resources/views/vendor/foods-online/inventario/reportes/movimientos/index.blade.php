@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Reporte de movimientos
@endsection


@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">Reporte de movimientos</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'inventario.reporte.movimientos.consultar', 'method' => 'POST']) !!}

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Fecha
                            </div>
                            <div class="panel-body">
                                {!! Form::label('fecha_desde','* Desde:') !!}
                                {!! Form::date('fecha_desde',null,['class' => 'form-control', 'required']) !!}

                                {!! Form::label('fecha_hasta','* Hasta:') !!}
                                {!! Form::date('fecha_hasta',null,['class' => 'form-control', 'required']) !!}

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Bodega
                            </div>
                            <div class="panel-body">
                                {!! Form::label('inventario_bodegas_id','* Bodega:') !!}
                                {!! Form::select('inventario_bodegas_id',$bodegas,null,['class' => 'form-control select-padre','id'=>'select_bodega','placeholder' => 'Selecciona una opción']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Producto
                            </div>
                            <div class="panel-body">
                                {!! Form::label('compras_productos_id','* Producto:') !!}
                                {!! Form::select('compras_productos_id',$productos,null,['class' => 'form-control select-padre','id'=>'select_producto','placeholder' => 'Selecciona una opción']) !!}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group text-center">
                    {!! Form::submit('Consultar',['class' => 'btn btn-primary'])!!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-padre').chosen({
            placeholder_text_single: "Selecciona una opción",
            no_results_text: "No se han encontrado "
        });
    </script>
@endsection

