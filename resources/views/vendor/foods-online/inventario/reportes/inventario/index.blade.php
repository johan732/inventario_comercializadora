@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Inventario
@endsection

@section('main-content')

    <div class="panel panel-default">
        <div class="panel-heading">Realizar movimiento en inventario</div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::open(['route' => 'inventario.movimientos.guardar', 'method' => 'POST']) !!}
                @include('vendor.foods-online.inventario.reportes.inventario.datos')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        /**
         * Script para el cambio de las listas dependientes
         * Se ejecuta cuando se selecciona una bodega o un tipo de producto, se consulta el inventario de la  bodega con dicho producto
         */
        $("#select_bodega, #select_tipo_producto").change(function () {

            /** Obtenemos los valores de la lista multiple y los pasamos a la función **/
            var valueBodega = $('#select_bodega').val();
            var valueTipoProducto = $('#select_tipo_producto').val();

            console.log(valueBodega+" - "+valueTipoProducto);

            /** Llamamos la función que cargará los precios solo cuando tengamos un valor de bodega y de producto**/
            if (valueBodega && valueTipoProducto) {

                formulario = '';

                /**
                 * Obtenemos los valores de la ruta
                 */
                $.get('inventario/' + valueBodega + '/' + valueTipoProducto, function (data) {

                    console.log("data length: "+data.length );

                    if (data.length > 0) {

                        formulario += '<table class="table table-bordered">';
                        formulario += '<thead>';
                        formulario += '<th class="text-center">#</th>';
                        formulario += '<th>Producto</th>';
                        formulario += '<th>precio </th>';
                        formulario += '<th>Cantidad</th>';
                        formulario += '<th></th>';
                        formulario += '</thead>';
                        formulario += '<tbody>';

                        for (var i = 0; i < data.length; i++) {

                            /** Clase que tendrá cada fila de la tabla **/
                            clase = 'default';
                            label = '';
                            disabled = '';

                            /** Si la cantidad es menor a la cantidad minima requerida en stock, ponemos la clase warning **/
                            if (data[i].cantidad == 0) {
                                clase = 'danger';
                                label = 'label label-danger';
                            }
                            else if (data[i].cantidad <= data[i].stock) {
                                clase = 'warning';
                                label = 'label label-warning';
                            }

                            if (data[i].movimientos == 0) {
                                disabled = 'disabled';
                            }

                            formulario += '<tr class="' + clase + '">';
                            formulario += '<td class="text-center">' + (i + 1) + '</td>';
                            formulario += '<td><span class="' + label + '">' + data[i].productName + '</span></td>';
                            formulario += '<td>$ ' + data[i].precio + '</td>';
                            formulario += '<td><span class="' + label + '">' + data[i].cantidad + " (" + data[i].um + ')</span></td>';
                            formulario += '<td class="text-center">';
                            formulario += '<a ' + disabled + ' href="#" class="btnModal btn btn-default" data-toggle="modal" data-target="#modalVerMov" id="' + data[i].id + '">' +
                                '<span class="far fa-clipboard" aria-hidden="true"></span></a>';
                            formulario += '</td>';
                            formulario += '</tr>';
                        }

                        formulario += '</tbody>';
                        formulario += '</table>';


                    }
                    else {
                        formulario = '<h4 class="text-center"> No se encontró el tipo de producto en esta bodega </h4>';
                    }

                    /** Cambiamos el valor del div con el id formularioPrecios por el formulario que hemos creado**/
                    $('#divTabla').html(formulario);

                });

            }
        });
    </script>
    <script>
        var bt_count = 0;
        $("#divTabla").on("click", ".btnModal", function () {
            inventarioId = $(this).attr('id');

            $.get('movimientos/' + inventarioId, function (data) {

                formulario = '';

                if (data.length > 0) {


                    formulario += '<table class="table table-bordered">';
                    formulario += '<thead>';
                    formulario += '<th>Tipo mov.</th>';
                    formulario += '<th>Fecha</th>';
                    formulario += '<th>Producto</th>';
                    formulario += '<th>precio </th>';
                    formulario += '<th>Cantidad</th>';
                    formulario += '<th>B. Orig.</th>';
                    formulario += '<th>B. Dest.</th>';
                    formulario += '<th>Motivo</th>';
                    formulario += '<th>Observaciones</th>';
                    formulario += '<th>Responsable</th>';
                    formulario += '</thead>';
                    formulario += '<tbody>';

                    for (var i = 0; i < data.length; i++) {

                        label = "label-danger";
                        tipo = "-";

                        console.log(data[i].tipo_movimiento);

                        if (data[i].tipo_movimiento == 1) {
                            label = "label-primary";
                            tipo ="Entrada";
                        } else if (data[i].tipo_movimiento == 2) {
                            label = "label-default";
                            tipo ="Salida";
                        } else if (data[i].tipo_movimiento == 3) {
                            label = "label-success";
                            tipo ="Traslado";
                        }

                        formulario += '<tr>';
                        formulario += '<td><span class="label ' + label + '">' + tipo + '</span></td>';
                        formulario += '<td class="small">' + data[i].fecha + '</td>';
                        formulario += '<td class="small">' + data[i].productName + '</td>';
                        formulario += '<td class="small">$' + data[i].precio + '</td>';
                        formulario += '<td class="small">' + data[i].cantidad + " (" + data[i].um + ')</td>';
                        formulario += '<td class="small">' + data[i].bodegaOrigen + '</td>';
                        formulario += '<td class="small">' + data[i].bodegaDestino + '</td>';
                        formulario += '<td class="small">' + data[i].motivo + '</td>';
                        formulario += '<td class="small">' + data[i].observaciones + '</td>';
                        formulario += '<td class="small">' + data[i].responsable + '</td>';
                        formulario += '</tr>';
                    }

                    formulario += '</tbody>';
                    formulario += '</table>';
                }
                else {
                    formulario = '<h4 class="text-center"> No se encontró el tipo de producto en esta bodega </h4>';
                }

                /** Cambiamos el valor del div con el id divTablaMovimientos por el formulario que hemos creado**/
                $('#divTablaMovimientos').html(formulario);

            });
        });
    </script>
@endsection