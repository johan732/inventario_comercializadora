<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Datos del producto -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    {{--<img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">--}}

                    <h3 class="profile-username text-center">Inventario</h3>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('inventario_bodegas_id_origen','*Bodega',['id'=>'label_bodega_origen']) !!}
                                {!! Form::select('inventario_bodegas_id_origen',$bodegas,null,['class' => 'form-control select-padre','id'=>'select_bodega','placeholder' => 'Selecciona una opción']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('tipo_producto','*Tipo de producto ') !!}
                                {!! Form::select('tipo_producto',$tipoProducto,null,['class' => 'form-control select-padre','id'=>'select_tipo_producto','placeholder' => 'Selecciona una opción']) !!}
                            </div>
                        </div>
                    </div>

                    <h3 class="profile-username text-center">Guia de colores</h3>
                    <div class="form-group">

                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>Producto agotado</td>
                                <td class="text-center"><span class="label label-danger">&nbsp;</span></td>
                            </tr>
                            <tr>
                                <td>Limite stock</td>
                                <td class="text-center"><span class="label label-warning">&nbsp;</span></td>
                            </tr>
                            <tr>
                                <td>Entrada</td>
                                <td class="text-center"><span class="label label-primary">&nbsp;</span></td>
                            </tr>
                            <tr>
                                <td>Salida</td>
                                <td class="text-center"><span class="label label-default">&nbsp;</span></td>
                            </tr>
                            <tr>
                                <td>Traslado</td>
                                <td class="text-center"><span class="label label-success">&nbsp;</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

            <!-- /.box -->
        </div>
        <!-- /.col -->
    @include('vendor.foods-online.inventario.reportes.inventario.formulario')
    <!-- /.col -->
    </div>
    <!-- /.row -->

    <div>

    </div>
</section>