<!-- /.col -->
<div class="col-md-9">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#configuracion" data-toggle="tab">Producto en inventario </a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="configuracion">
                <div id="divTabla"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalVerMov" tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Movimientos
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Incluimos el reporte de movimientos para ese producto  -->
                        <div id="divTablaMovimientos"></div>
                    </div>
                </div>
            </div><!-- Close modal-body -->

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
