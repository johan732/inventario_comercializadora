<?php
use App\Http\Controllers\sistema\menu\MenuController;
use App\src\empresa\Empresa;

/** @var  $userLogeado Usuario actual en el sistema */
$userLogeado = Auth::user()->ID_rol;

/** @var  $menu Menu asignado al rol */

/**
 * AVISO IMPORTANTE
 */
/** Se debe enviar la variable $userLogeado pero lo enviamos así temporalmente debe cambiarse cuando se libere*/
$menu = MenuController::construirMenu(0, null);

/** @var  $empresa  contiene los datos de la empresa */
$empresa = Empresa::first();
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">


        @if($empresa!=null  and $empresa->logo!='')

            <div class="col-xs-9" style="padding-left: 5em; padding-top: 1em">
                <img src="{{asset('img/foods-online/empresa/'.$empresa->logo)}}" class="img-responsive" alt="...">
            </div>
        @endif


    <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Auth::user()->img ? Auth::user()->img :asset('img/avatar5.png') }}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}
                    </a>
                </div>
            </div>
    @endif

    <!-- search form (Optional) -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control"
                       placeholder="{Buscar..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menú</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active">
                <a href="{{ url('home') }}">
                    <i class='fa fa-home'></i>
                    <span>
                        {{ trans('adminlte_lang::message.home') }}
                    </span>
                </a>
            </li>

            <!-- Llamamos a la variable que contiene el menú -->
            {!! $menu !!}
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
