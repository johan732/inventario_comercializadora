<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple sidebar-mini">

<div id="app">
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <!-- Incluimos la libreria flash, para mostrar las ventanas con los mensajes -->
            @include('flash::message')

            <!-- Incluimos la vista que contiene los errores, para mostrarlos en caso de que se presenten -->
            @include('vendor.foods-online.sistema.errors.errors')
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>

<script src="{{asset('plugins/jquery/jquery-3.1.0.js')}}"></script>

@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

<!-- Usamos la libreria chosen, para dar estilo a las listas depegables -->
<script src="{{asset('plugins/chosen/chosen.jquery.js')}}"></script>

<!-- Usamos la libreria trumbowyg para dar estilo y agregar funciones a los text area -->
<script src="{{asset('plugins/trumbowyg/trumbowyg.js')}}"></script>

<!-- Usamos este script para cuando sean necesarios cargar listas dependientes -->
<script src="{{asset('js/selectDependiente.js')}}"></script>
<script src="{{asset('js/selectDependiente2.js')}}"></script>
<!-- Usamos este script cuando sean necesario cargar lista dependientes multiples -->
<script src="{{asset('js/selectMultipleDependiente.js')}}"></script>
<!-- Usamos este script para dar formato de moneda en js -->
<script src="{{asset('js/number-format.js')}}"></script>
<!--  Script para que siempre aplique el estilo de las listas -->
<script>
    $('.select-padre').chosen({
        placeholder_text_single: "Selecciona una opción",
        no_results_text: "No se han encontrado "
    });
</script>
@yield('js')
</body>
</html>
