INSTALACIÓN DE COMPOSER

Controladores de forma plural, y los modelos de forma singular

1.https://styde.net/instalacion-de-composer-y-laravel-en-windows/

CREAR PROYECTO DE LARAVEL
1. nos vamos a la ruta: cd C:\xampp\htdocs

2. Creamos el proyecto
composer create-project laravel/laravel nombre_del_proyecto --prefer-dist

3. realizar prueba
vendor/bin/phpunit

4. creamos una migracion
php artisan make:migration create_notes_table --create=notes

5.Ejecutamos la migracion
php artisan migrate

*Para ver los comandos de ayuda
php artisan list

-migrate:install
-migrate:refresh
-migrate:status

*permite alterar una tabla ya existente
php artisan make:migration add_categories_column_to_notes --table=notes


6. Deshacer la última migracion
php artisan migrate:rollback

7. Ejecutar php en la consola
php artisan tinker

8. crear un controlador
php artisan make:controller nombreControlador

9. ver listado de rutas creadas
php artisan route:list

10. para la BD de pruebas
php artisan migrate --database=mysql_tests

11. Para crear registros de prueba
php artisan make:seeder NoteTableSeeder

12. Ejecuta los seeders
php artisan db:seed

13. Elimina las tablas y ejecuta los seeders
 php artisan migrate:refresh --seed

 14. Crear un modelo
 php artisan make:model Category

15 relaciones muchos a muchos, la que se creó en la migracion
$article->tags()->attach(id_relaciona_articulo)

16. crear request
php artisan make:request UserRequest

---------------------------------------------- GIT -------------------------------------------------------------
http://www.gabrielsaldana.org/platica_git.pdf
https://www.youtube.com/watch?v=4C83fGtiQ48	

 * Configurar identidad
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com

* Listar configuración
git config --list

* Ayuda git
$ git help <comando>
$ git <comando> --help

* Inicializar un repositorio en un directorio existente
$ git init

* Añadir directorios y poner commits
-para agregar todos los archivos
$ git add *.c

$ git add README
$ git commit –m 'versión inicial del proyecto'

*Clonando un repositorio existente
git clone [url] --> $ git clone git://github.com/schacon/grit.git NombreDirectorio

*Comprobando el estado de tus archivos
$ git status

*Seguimiento de nuevos archivos
$ git add ARCHIVOSEGUIR

*Preparando archivos modificados
$ git status

*Ignorando archivos
crear un archivo llamado .gitignore, Ejemplo: 
--
$ cat .gitignore
*.[oa]
*~
--

Las reglas para los patrones que pueden ser incluidos en el archivo .gitignore son:

Las líneas en blanco, o que comienzan por #, son ignoradas.
Puedes usar patrones glob estándar.
Puedes indicar un directorio añadiendo una barra hacia delante (/) al final.
Puedes negar un patrón añadiendo una exclamación (!) al principio.

Ejemplo:
--
# a comment – this is ignored
# no .a files
*.a
# but do track lib.a, even though you're ignoring .a files above
!lib.a
# only ignore the root TODO file, not subdir/TODO
/TODO
# ignore all files in the build/ directory
build/
# ignore doc/notes.txt, but not doc/server/arch.txt
doc/*.txt
# ignore all .txt files in the doc/ directory
doc/**/*.txt
--

* Viendo tus cambios preparados y no preparados: Saber exactamente lo que ha cambiado, no sólo qué archivos fueron modificados.
$ git status

*Confirmando tus cambios
$ git commit
$ git commit -m "Comentario"

*Clonar archivos
git clone git://github.com/schacon/simplegit-progit.git

*Subir archivos
git push 	



	