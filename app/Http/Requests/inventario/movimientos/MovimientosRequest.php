<?php

namespace App\Http\Requests\inventario\movimientos;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class MovimientosRequest extends FormRequest
{
    /**
     * @var Route
     */
    private $route;

    /**
     * MenuRequest constructor.
     * @param Route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inventario_bodegas_id_origen' => 'required',
            'compras_tipo_producto_id' => 'required',
            'compras_productos_id' => 'required',
            'inventario_motivos_movimiento_id' => 'required',
            'precio' => 'min:1|required',
            'inventario_bodegas_id_destino' => ($this->tipo_movimiento == 3) ? 'required' : '',
            'cantidad' => 'min:1|required'
        ];
    }
}
