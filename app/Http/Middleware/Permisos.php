<?php

namespace App\Http\Middleware;

use App\src\sistema\menu\Menu;
use App\src\sistema\usuario\permiso\Permiso;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Permisos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** Obtener la ruta actual y comparar con la ruta de cada menu para saber cual activar, pendiente implementación**/
        $ruta = Request::route()->getName();

        /** Obtenemos el usuario que está logeado **/
        $idRolActual = Auth::User()->Rol->id;

        /** Obtenemos el id del menú en la ruta que se está accediendo **/
        $idMenuActual = Menu::where('as', $ruta);


//        echo $ruta;
        /**
         * Si encuentra el menú, comprobamos los permisos
         */
        if ($idMenuActual->count()) {
            /**
             * Comprobamos los permisos para el rol en ese menú
             */
            $permisos = Permiso::where('ID_menu', $idMenuActual->first()->id)->where('ID_rol', $idRolActual);

            /**
             * Si tiene permiso, permitimos que siga, de lo contrario, lo redireccionamos al home y mostramos una alerta
             */
            if ($permisos->count()) {
                return $next($request);
            } else {
                flash('Usted no tiene permisos para este menú', 'danger');
                return redirect('home');
            }
        }

        /****
         * IMPORTANTE: Falta validar los permisos para crear, actualizar y eliminar, solo se están comproando los permisos para las opciones
         * que se muestran en el menú
         */

//        flash('No encuentro el menú', 'warning');
        return $next($request);

    }
}
