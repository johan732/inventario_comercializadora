<?php

namespace App\Http\Controllers\sistema\menu;

use App\Http\Requests\sistema\menu\MenuRequest;
use App\src\sistema\menu\Menu;
use App\src\sistema\usuario\permiso\Permiso;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    /**
     * Función para retornar todos los elementos de la tabla de los menús
     * @param $request datos del formulario
     * @return  una vista  donde se muestran todos los menús.
     */
    public function index(Request $request)
    {
        /**
         * Consultamos los menús que tengan un enlace padre con id>=0,
         * ya que el enlace de donde heredan todos será el id = 0 y este tendrá como padre -1
         */
        $menus = Menu::Search($request->buscar)->where('id_padre', '>=', 0)
            ->orderBy('id_padre', 'ASC')
            ->orderBy('orden', 'ASC')
            ->paginate(10);

        return view('vendor.foods-online.sistema.menu.index')->with('menus', $menus);
    }

    /**
     * Función para crear un menú, hacemos el llamado a la vista que
     * que pedirá los datos
     */
    public function create()
    {
        $menus = Menu::get();
        $maxOrden = $menus->max('orden');

        /**
         * Leemos todo los menus de la consulta y cambiamos el nombre del arreglo que se genera
         * esto para poder poner el padre e idenfiticar más fácil que menú asignaremos al crear o actulizar un menú
         */
        foreach ($menus As $menux) {

            $padre = "-";

            /** Si el id_padre es => a cero es porque el menú tiene padre */
            if ($menux->id_padre >= 0) {
                $padre = Menu::find($menux->id_padre);
                $padre = $padre->nombre;
            }

            /* Cambiamos el nombre del menú en el arreglo para pasarlo a la vista */
            $menux->nombre = $menux->id . " - " . $menux->nombre . " (" . $padre . ") - [" . $menux->src . "]";
        }

        /* Creamos la lista "id => nombre" de la lista de menus */
        $menus = $menus->pluck('nombre', 'id');

        return view('vendor.foods-online.sistema.menu.create')->with('menus', $menus)
            ->with('maxOrden', $maxOrden);
    }

    /**
     * Función para guardar un menú
     * @param Request $request
     */
    public function store(MenuRequest $request)
    {
        $menu = new Menu($request->all());

        if ($menu->icon == '') {
            $menu->icon = 'fa fa-angle-right';
        }

        $menu->save();
        flash('Se ha creado el menú correctamente', 'success');
        return redirect()->route('menu.index');
    }

    /**
     * Función que retorna una vista para editar un menú
     * @param $id el id del menú
     * @return mixed
     */

    public function edit($id)
    {

        $menus = Menu::get();

        /** @var  $menu Menu que es el padre */
        $menu = Menu::find($id);

        $maxOrden = $menu->max('orden');

        /**
         * Leemos todo los menus de la consulta y cambiamos el nombre del arreglo que se genera
         * esto para poder poner el padre e idenfiticar más fácil que menú asignaremos al crear o actulizar un menú
         */
        foreach ($menus As $menux) {

            $padre = "-";

            /** Si el id_padre es => a cero es porque el menú tiene padre */
            if ($menux->id_padre >= 0) {
                $padre = Menu::find($menux->id_padre);
                $padre = $padre->nombre;
            }

            /* Cambiamos el nombre del menú en el arreglo para pasarlo a la vista */
            $menux->nombre = $menux->id . " - " . $menux->nombre . " (" . $padre . ") - [" . $menux->src . "]";
        }

        /* Creamos la lista "id => nombre" de la lista de menus */
        $menus = $menus->pluck('nombre', 'id');


        return view('vendor.foods-online.sistema.menu.edit')->with('menu', $menu)
            ->with('menus', $menus)
            ->with('maxOrden', $maxOrden);
    }

    /**
     * Función que actualiza un menú en la base de datos
     * @param MenuRequest $request los datos que se van a actualizar
     * @param $id el id del menú
     * @return mixed
     */
    public function update(MenuRequest $request, $id)
    {
        $menu = Menu::find($id);
        $menu->fill($request->all());
        $menu->save();
        flash('Se ha actualizado el menú correctamente', 'success');
        return redirect()->route('menu.index');
    }

    /**
     * Función que elimina un menú de la base de datos
     * @param $id el id del menú
     * @return mixed
     */
    public function destroy($id)
    {
        /**
         * Consultamos si el id que será eliminado tiene enlaces hijos,
         * en caso de que los tenga no se podrá eliminar
         */
        $numeroHijos = self::tieneHijos($id);

        if (!$numeroHijos) {

            /** Eliminamos los permisos que tenga el menú **/
            $permisos = Permiso::where('ID_menu', $id)->delete();

            $menu = Menu::find($id);
            $menu->delete();
            flash(strtoupper($menu->nombre) . ', Se ha eliminado correctamente', 'danger');
            return redirect()->route('menu.index');
        } else {
            $menu = Menu::find($id);
            flash(strtoupper($menu->nombre) . ', No puede ser eliminado ya que está relacionado con otros enlaces', 'warning');
            return redirect()->route('menu.index');
        }
    }


    /**
     * Función para consultar si un id está relacionado con otros enlaces
     * @param $id_padre
     * @return bool
     */
    static private function tieneHijos($id_padre)
    {
        $hay = false;

        $menu = Menu::where('id_padre', $id_padre)->count();

        if ($menu > 0) {
            $hay = true;
        }

        return $hay;
    }


    /**
     * Función estática que permite crear el menú principal
     * @param int $id_padre
     * @return mixed
     */

    /**
     * Mètodo para construir el menù de acuerdo a los permisos del rol de cada usuario
     * @param int $id_padre el menù padre
     * @param int $id el usuario que solicita el menù
     * @return string
     */
    static public function construirMenu($id_padre = 0, $id = 0)
    {

        $etiquetaMenu = '';
        $menus = Menu::where('id_padre', $id_padre)->orderBy('orden', 'ASC')->get();
        $totalMenus = $menus->count();



        if ($totalMenus > 0) {
            $etiquetaMenu .= '';

            foreach ($menus As $menu) {

                /**
                 * AVISO IMPORTANTE
                 */
                /** Esta variable debe ir en false, se cambia temporalmente */
                $tienePermiso = true;

                /**
                 * Comprobamos si el id que se recibe no es null, y cambiamos el valor de la variable
                 * @param $tienePermiso para marcar el checkbox que tiene el permiso
                 */
                if ($id != null) {

                    $permisos = Permiso::where('ID_menu', $menu->id)
                        ->where('ID_rol', $id);

                    $totalPermiso = $permisos->count();

                    if ($totalPermiso > 0) {
                        $tienePermiso = true;
                    }
                }


                /**
                 * Consultamos si el menú que se está creando tiene hijos para crear en enlace
                 * true si tiene hijos, false si no tiene
                 */
                $numeroHijos = self::tieneHijos($menu->id);

                if ($numeroHijos && $tienePermiso) {

                    $etiquetaMenu .= '<li class="treeview">';
                    $etiquetaMenu .= '<a href="' . url($menu->src) . '">';
                    $etiquetaMenu .= '<i class="' . $menu->icon . ' "></i>';
                    $etiquetaMenu .= '<span> ' . $menu->nombre . '</span>';
                    $etiquetaMenu .= '<i> <label class="fa fa-angle-left pull-right"></label> </i>';
                    $etiquetaMenu .= '</a>';

                    $etiquetaMenu .= '<ul class="treeview-menu">';
                    $etiquetaMenu .= self::construirMenu($menu->id, $id);
                    $etiquetaMenu .= '</ul>';

                    $etiquetaMenu .= '</li>';
                } else if ($tienePermiso) {
                    $etiquetaMenu .= '<li class="treeview">';
                    $etiquetaMenu .= '<a href="' . url($menu->src) . '">';
                    $etiquetaMenu .= '<i class="' . $menu->icon . '"></i>';
                    $etiquetaMenu .= '<span> '. $menu->nombre . '</span>';
                    $etiquetaMenu .= '</a>';
                    $etiquetaMenu .= '</li>';
                }
            }
        }
        return $etiquetaMenu;
    }


    /**
     * Función para retornar el menú completo
     * esta función se crea inicialmente para la creación y actualización de permisos
     * @param int $id_padre
     * @param int $id id del rol que se va a consultar los permisos, para la creación de un permiso se envia este campo como vacio
     * @return string
     */
    static public function construirMenuCompleto($id_padre = 0, $id)
    {

        $etiquetaMenu = '';
        $menus = Menu::where('id_padre', $id_padre)->orderBy('orden', 'ASC')->get();
        $totalMenus = $menus->count();

        if ($totalMenus > 0) {
            $etiquetaMenu .= '';

            foreach ($menus As $menu) {

                $marcar = '';

                /**
                 * Comprobamos si el id que se recibe no es null, y cambiamos el valor de la variable
                 * @param $marcar para marcar el checkbox que tiene el permiso
                 */
                if ($id != null) {

                    $permisos = Permiso::where('ID_menu', $menu->id)
                        ->where('ID_rol', $id);

                    $totalPermiso = $permisos->count();

                    if ($totalPermiso > 0) {
                        $marcar = 'checked';
                    }
                }

                /**
                 * Consultamos si el menú que se está creando tiene hijos para crear en enlace
                 * true si tiene hijos, false si no tiene
                 */
                $numeroHijos = self::tieneHijos($menu->id);

                if ($numeroHijos) {

                    $etiquetaMenu .= '<ul class="autoCheckbox">';
                    $etiquetaMenu .= '<li class="">';
                    $etiquetaMenu .= '<a href="">';
                    $etiquetaMenu .= '<label class="checkbox">';
                    $etiquetaMenu .= '<input type="checkbox" value="' . $menu->id . '" name="ID_menu[]" ' . $marcar . ' >';
                    $etiquetaMenu .= $menu->nombre;
                    $etiquetaMenu .= '</label>';
                    $etiquetaMenu .= '</a>';
                    $etiquetaMenu .= '<ul class="autoCheckbox">';
                    $etiquetaMenu .= self::construirMenuCompleto($menu->id, $id);
                    $etiquetaMenu .= '</ul>';
                    $etiquetaMenu .= '</li>';
                    $etiquetaMenu .= '</ul>';
                } else {
                    $etiquetaMenu .= '<ul class="autoCheckbox">';
                    $etiquetaMenu .= '<li class="treeview">';
                    $etiquetaMenu .= '<a href="#">';
                    $etiquetaMenu .= '<label class="checkbox">';
                    $etiquetaMenu .= '<input type="checkbox" value="' . $menu->id . '" name="ID_menu[]" ' . $marcar . ' >';
                    $etiquetaMenu .= $menu->nombre;
                    $etiquetaMenu .= '</label>';
                    $etiquetaMenu .= '</a>';
                    $etiquetaMenu .= '</li>';
                    $etiquetaMenu .= '</ul>';
                }

            }
        }
        return $etiquetaMenu;
    }

    /**
     * Retorna el orden máximo que tienen los hijos
     * @param $id
     * @return mixed
     */
    public function getHijos($id)
    {
        return Menu::where('id_padre', $id)->max('orden');
    }
}
