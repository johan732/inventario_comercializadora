<?php

namespace App\Http\Controllers\sistema\usuario\permiso;

use App\Http\Controllers\sistema\menu\MenuController;
use App\Http\Requests\sistema\usuario\permiso\PermisoRequest;
use App\src\sistema\usuario\permiso\Permiso;
use App\src\sistema\usuario\rol\Rol;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PermisoController extends Controller
{
    /**
     * Función que lista los roles que tienen permisos asignados de la tabla "foods_permisos" agrupados por ID_rol
     * @return mixed
     */
    public function index(Request $request)
    {
        $permisos = Permiso::Search($request->buscar)->groupBy('ID_rol')->paginate(10);
        /**
         * @var $numeroEnlaces  contiene el total de enlaces de cada rol
         * Se envian los permisos que ya se han dado
         */
        $numeroEnlaces = $this->numeroEnlaces($permisos);

        return view('vendor.foods-online.sistema.usuario.permiso.index')
            ->with('permisos', $permisos)
            ->with('numeroEnlaces', $numeroEnlaces);
    }

    /**
     * Función que retorna la lista para crear los permisos de un rol de usuario
     */
    public function create()
    {
        /*
         * Consultamos los roles que ya tienen permisos
         */
        $permisos = Permiso::groupBy('ID_rol')->get();
        $permisosAsignados = $permisos->map(function ($permiso) {
            return $permiso->ID_rol;
        });
        /**
         * Consultamos los roles que aún no tienen permisos creados
         */
        $roles = Rol::whereNotIn('id', $permisosAsignados)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        $menu = MenuController::construirMenuCompleto(0, null);
        /**
         * Debemos pasar el menú con los checkbox
         */

        return view('vendor.foods-online.sistema.usuario.permiso.create')
            ->with('roles', $roles)
            ->with('menu', $menu);
    }

    public function store(PermisoRequest $request)
    {
        //dd($request);

        /**
         * Leemos todos los menús que se han seleccionado y creamos objetos
         * para guardar en la tabla de los permisos
         */
        foreach ($request->ID_menu As $ID_menu) {

            /** @var  $permiso objeto de la clase Permiso (Clase que maneja la tabla de los permisos) */
            $permiso = new Permiso();

            /** @var  ID_menu Variable que viene en el request desde el form */
            $permiso->ID_menu = $ID_menu;

            /** @var  ID_rol Variable que viene en el request desde el formulario */
            $permiso->ID_rol = $request->ID_rol;

            /** Guardamos el objeto en la tabla que manejala clase Permiso */
            $permiso->save();
        }

        flash('Se han asignado los permisos correctamente', 'success');
        return redirect()->route('permiso.index');

    }

    public function edit($id)
    {
        $rol = Rol::find($id);
        $menu = MenuController::construirMenuCompleto(0, $id);

        return view('vendor.foods-online.sistema.usuario.permiso.edit')
            ->with('rol', $rol)
            ->with('menu', $menu);
    }

    /**
     * Método para actualizar los permisos de un rol de usuario
     * @param PermisoRequest $request los datos de los nuevos o los permisos que se quitaron
     * @param $id el id del rol del cual se editan los permisos
     */
    public function update(PermisoRequest $request, $id)
    {
        /**
         * Leemos todos los checkbox marcados y consultamos en la BD si el usuario ya tiene permisos,
         * en caso de no tener permiso lo creamos,
         * al final borramos todos los permisos del rol que no estén en el arreglo de marcados
         */
        foreach ($request->ID_menu As $ID_menu) {

            $permiso = Permiso::where('ID_menu',$ID_menu)
                ->where('ID_rol',$id);
            $permiso = $permiso->count();

            /** Si el resultado es cero, entonces no tiene permiso a ese enlace, así que debemos insertarlo en la BD**/
            if($permiso == 0){
                /**
                 * Creamos un nuevo objeto del modelo Permiso y actualizamos sus atributos para insertar en la BD
                 */
                $permisoNuevo = new Permiso();
                $permisoNuevo->ID_menu = $ID_menu;
                $permisoNuevo->ID_rol = $id;
                $permisoNuevo->save();
            }
        }

        /**
         * Eliminamos los permisos que no se encuentran en el arreglo de checkbox marcados que se envió por el formulario
         */
        $permisos = Permiso::whereNotIn('ID_menu',$request->ID_menu)
            ->where('ID_rol',$id);
        $permisos->delete();


        flash('Se han actualizado los permisos correctamente', 'success');
        return redirect()->route('permiso.index');
    }

    public function destroy()
    {

    }

    /**
     * Función que retorna que el número de enlaces que hay en permisos
     * @param $ID_rol los permisos de los que se contarán los enlaces
     */
    private function numeroEnlaces($ID_rol)
    {
        $totalPermisos[] = 0;

        /**
         * Leemos los permisos que han llegado
         * @var  $key llave de los valores
         * @var  $value valor del arreglo
         */
        foreach ($ID_rol As $key => $value) {

            /** @var  $permisos Variable que guardará la consulta del permiso */
            $permisos = Permiso::where('ID_rol', $value->ID_rol);
            /** Total de permisos que se encontraron de cada rol */
            $totalPermisos[$key] = $permisos->count();

        }
        return $totalPermisos;
    }

}
