<?php

namespace App\Http\Controllers\sistema\usuario\usuario;

use App\Http\Requests\sistema\menu\MenuRequest;
use App\Http\Requests\sistema\usuario\usuario\UserRequest;
use App\src\sistema\usuario\rol\Rol;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Función que retorna los registros de la tabla de usuarios
     * @return mixed
     */
    public function index(Request $request)
    {
        $usuarios = User::Search($request->buscar)->where('habilitado',1)->orderBy('name','ASC')->paginate(10);
        return view('vendor.foods-online.sistema.usuario.usuario.index')->with('usuarios',$usuarios);
    }

    /*
     * Función que retorna la vista para crear un nuevo usuario del sistema
     */
    public function create()
    {
        $roles = Rol::orderBy('nombre','ASC');
        $roles = $roles->pluck('nombre','id');
        return view('vendor.foods-online.sistema.usuario.usuario.create')->with('roles',$roles);
        //
    }

    /**
     * Función para crear un usuario en la base de datos
     * @param UserRequest $request
     * @return mixed
     */
    public function store(UserRequest $request)
    {
        $user = new User($request->all());
        $user -> password = bcrypt($request->password);
        $user -> save();
        flash('Se ha creado el usuario correctamente','success');
        return redirect()->route('user.index');
    }

    /**
     *Función que retorna una vista para editar un usuario
     * @param $id el id del usuario
     * @return mixed
     */
    public function edit($id)
    {
        $roles = Rol::orderBy('nombre','ASC');
        $roles = $roles->pluck('nombre','id');

        $user = User::find($id);

       return view('vendor.foods-online.sistema.usuario.usuario.edit')->with('roles',$roles)
           ->with('user',$user);
    }

    /**
     * Función para actualizar un usuario en la base de datos
     * @param Request $request los datos que serán actualizados
     * @param $id el id del usuario
     * @return mixed
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);

        /**
         * Guardamos el password que el usuario tiene en la base de datos, en caso de que
         * no se vaya a cambiar la contraseña
         */
        $password = $user -> password;

        $user -> fill($request->all());

        /**
         * Comprobamos que el usuario no haya cambiado la contraseña para asignar la misma que tenía
         */
        if($request->password==NULL)
        {
            $user -> password = $password;
        }
        else
        {
            $user -> password = bcrypt($request->password);
        }

        /**
         * Comprobamos si el checkbox de habilitado está deshabilitado para asignar el valor de cero
         */
        if(!isset($request->habilitado))
        {
            $user -> habilitado = 0;
        }


        $user -> save();
        flash('Se ha actualizado el usuario correctamente','success');
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user -> delete();
        flash('Usuario eliminado correctamente','danger');
        return redirect()->route('user.index');
    }
}
