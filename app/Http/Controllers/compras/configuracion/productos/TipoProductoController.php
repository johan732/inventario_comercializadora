<?php

namespace App\Http\Controllers\compras\configuracion\productos;

use App\Http\Requests\compras\configuracion\productos\TipoProductoRequest;
use App\src\calidad\configuracion\analisis\TipoAnalisis;
use App\src\compras\configuracion\productos\TipoProducto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class TipoProductoController
 * @package App\Http\Controllers\compras\configuracion\productos
 */
class TipoProductoController extends Controller
{
    /**
     * Muestra una lista con los tipos de productos que hay en la BD
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipoProductos = TipoProducto::orderBy('nombre', 'ASC')->paginate(20);

        return view('vendor.foods-online.compras.configuracion.productos.tipo.index')->with('tipoProductos', $tipoProductos);
    }

    /**
     * Muestra el formulario para crear un nuevo tipo de producto
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.foods-online.compras.configuracion.productos.tipo.create');
    }

    /**
     * Guarda en la base de datos el nuevo tipo de producto
     * y sincroniza los análisis que se le hacen
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoProductoRequest $request)
    {
        $tipoProducto = new TipoProducto($request->all());
        $tipoProducto->save();

        flash('Se ha creado el tipo de producto correctamente', 'success');
        return redirect()->route('compras.configuracion.productos.tipo.index');
    }

    /**
     * Muestra el formualario para actualizar un tipo de producto y los análisis que requiere
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoProducto = TipoProducto::find($id);

        return view('vendor.foods-online.compras.configuracion.productos.tipo.edit')
            ->with('tipoProducto', $tipoProducto);
        //
    }

    /**
     * Actualiza en la base de datos el tipo de producto
     * que se envia desde el formulariod e actualización
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipoProductoRequest $request, $id)
    {
        $tipoProducto = TipoProducto::find($id);
        $tipoProducto->fill($request->all());
        $tipoProducto->save();

        flash('Se ha actualizado el tipo de producto correctamente', 'success');
        return redirect()->route('compras.configuracion.productos.tipo.index');
    }

    /**
     * Actualiza el campo eliminado de un tipo de producto
     * si el campo eliminado = 1, no se mostrará al crear productos
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipoProducto = TipoProducto::find($id);
        $tipoProducto->eliminado = 1;
        $tipoProducto->save();

        return redirect()->route('compras.configuracion.productos.tipo.index');
        //
    }

    /**
     * Actualiza el campo eliminado de un tipo de producto
     * si el campo eliminado = null, se mostrará al crear un producto
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function habilitar($id)
    {
        $tipoProducto = TipoProducto::find($id);
        $tipoProducto->eliminado = null;
        $tipoProducto->save();

        return redirect()->route('compras.configuracion.productos.tipo.index');
    }
}
