<?php

namespace App\Http\Controllers\compras\configuracion\productos;

use App\Http\Requests\compras\configuracion\productos\ProductoRequest;
use App\src\calidad\configuracion\analisis\Analisis;
use App\src\calidad\configuracion\analisis\TipoAnalisis;
use App\src\compras\configuracion\productos\AnalisisProducto;
use App\src\compras\configuracion\productos\Producto;
use App\src\compras\configuracion\productos\TipoProducto;
use App\src\sistema\configuracion\general\UnidadDeMedida;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

/**
 * Clase para la implementación de los métodos en la configuración de los productos de compra
 * Class productosController
 * @package App\Http\Controllers\compras\configuracion\productos
 */
class productosController extends Controller
{
    /**
     * Método que lista todos los productos en la base de datos
     * @return $this
     */
    public function index(Request $request)
    {
        $productos = Producto::Search($request->buscar)
            ->orderBy('nombre', 'ASC')
            ->paginate(20);

        return view('vendor.foods-online.compras.configuracion.productos.index')->with('productos', $productos);
    }

    /**
     * Método para crear un producto en la base de datos
     * @return $this
     */
    public function create()
    {
        /** Consultamos los tipos de productos disponibles**/
        $tipoProducto = TipoProducto::where('eliminado', null)->OrderBy('nombre', 'ASC')->pluck('nombre', 'id');

        /** consutalmos las unidades de medida que están disponibles **/
        $unidadesDeMedida = UnidadDeMedida::where('eliminado', null)->OrderBy('nombre', 'ASC')->get();

        /** Cambiamos el atributo nombre de la unidad de medida para conformarlo con más información **/
        foreach ($unidadesDeMedida As $unidadDeMedida) {
            $unidadDeMedida->nombre = $unidadDeMedida->nombre . " ( " . $unidadDeMedida->abreviatura . " )";
        }
        /** Creamos el arreglo con la llave y el valor de la lista**/
        $unidadesDeMedida = $unidadesDeMedida->pluck('nombre', 'id');

        return view('vendor.foods-online.compras.configuracion.productos.create')
            ->with('tipoProducto', $tipoProducto)
            ->with('unidadesDeMedida', $unidadesDeMedida);
    }

    /**
     * Método que guarda el producto que recibe del formulario de creación de producto
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductoRequest $request)
    {
        /** Creamos el nuevo producto y reeemplazamos los atributos por los que vienen del formulario**/
        $producto = new Producto($request->all());
        $producto->save();

        flash('Se ha creado el producto correctamente', 'success');
        return redirect()->route('compras.configuracion.productos.index');
    }

    /**
     * Método que retorna la vista con los datos del producto a editar
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        $tipoProductos = TipoProducto::OrderBy('nombre', 'ASC')->get();
        $unidadesDeMedida = UnidadDeMedida::where('eliminado', null)->OrderBy('nombre', 'ASC')->get();

        foreach ($unidadesDeMedida As $unidadDeMedida) {
            $unidadDeMedida->nombre = $unidadDeMedida->nombre . " ( " . $unidadDeMedida->abreviatura . " )";
        }

        /** Consultamos las unidades de medida de un producto**/
        $unidadesDeMedida = $unidadesDeMedida->pluck('nombre', 'id');



        return view('vendor.foods-online.compras.configuracion.productos.edit')
            ->with('producto', $producto)
            ->with('tipoProductos', $tipoProductos)
            ->with('unidadesDeMedida', $unidadesDeMedida);
    }

    /**
     * Método que actualiza los datos de un producto que se recibe del formulario de edición
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductoRequest $request, $id)
    {
        /**Buscamos el producto con el id en la tabla del ProductoInventario**/
        $producto = Producto::find($id);
        $producto->fill($request->all());
        $producto->save();

        flash('Se ha actualizado el producto correctamente', 'success');
        return redirect()->route('compras.configuracion.productos.index');
    }

    /**
     * Método que elimina un producto de la base de datos
     * se cambia el campo eliminado = 1 para indicar que no se mostrará más
     * no se elimina debido a que pueden haber registros en otras tablas con este producto
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);
        $producto->eliminado = 1;
        $producto->save();
        flash('Se ha deshabilitado el producto correctamente', 'danger');
        return redirect()->route('compras.configuracion.productos.index');
    }

    /**
     * Método habilita un producto de la base de datos
     * se cambia el campo eliminado = 0 para indicar que se mostrará
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function habilitar($id)
    {
        $producto = Producto::find($id);
        $producto->eliminado = null;
        $producto->save();
        flash('Se ha habilitado el producto correctamente', 'sucess');
        return redirect()->route('compras.configuracion.productos.index');
    }
}
