<?php

namespace App\Http\Controllers\inventario;

use App\Http\Requests\inventario\movimientos\MovimientosRequest;
use App\src\compras\configuracion\productos\Producto;
use App\src\compras\configuracion\productos\TipoProducto;
use App\src\inventario\configuracion\Bodega;
use App\src\inventario\configuracion\MotivoMovimiento;
use App\src\inventario\Inventario;
use App\src\inventario\InventarioMovimientos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Clase que controla las operaciones en inventario
 * Class InventarioController
 * @package App\Http\Controllers\inventario
 */
class InventarioController extends Controller
{

    /**
     * Muestra el formulario para el manejo del inventario
     * @return mixed
     */
    public function formularioMovimientos()
    {
        /** Bodegas disponibles**/
        $bodegas = Bodega::where('habilitado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        /** Tipos de producto **/
        $tipoProducto = TipoProducto::where('eliminado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        /** Motivos de movimiento**/
        $motivosMovimiento = MotivoMovimiento::where('habilitado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');

        return view('vendor.foods-online.inventario.movimientos.index')
            ->with('motivosMovimiento', $motivosMovimiento)
            ->with('tipoProducto', $tipoProducto)
            ->with('bodegas', $bodegas);
    }

    /**
     * Guarda el movimiento en la base de datos dependiendo del tipo de movimiento que desea realizar
     *  1.- Entrada
     *  2.- Salida
     *  3.- Traslado
     * @param Request $request Datos del formulario de movimientos
     * @return \Illuminate\Http\RedirectResponse
     */

    public function guardarMovimientos(MovimientosRequest $request)
    {
        /** Obtenemos el usuario actual **/
        $userActual = Auth::user();

        /** Si el tipo de movimiento es 1, es una entrada a inventario**/
        if ($request->tipo_movimiento == 1) {
            /**
             * Consultamos si el producto con el precio ya se encuentra en inventario, esto con el fin de no crear un nuevo registro
             * y simplemente actualizar la cantidad
             */
            $inventario = Inventario::where('compras_productos_id', $request->compras_productos_id)
                ->where('inventario_bodegas_id', $request->inventario_bodegas_id_origen)
                ->where('precio', $request->precio)->first();

            /**
             * Si el producto y el precio ya se encontraban en una bodega, debemos actualizar los valores en inventario y no crear un nuevo registro
             */
            if ($inventario) {
                $inventario->cantidad = $inventario->cantidad + $request->cantidad;
                $inventario->total = $inventario->cantidad * $inventario->precio;
            } /** Si el producto nose encontraba en inventario, debemos crear el registro y guardarlo en la BD **/
            else {

                /**
                 * Creamos el objeto del modelo de inventario para guardar la entrada
                 */
                $inventario = new Inventario($request->all());
                $inventario->precio = $request->precio;
                $inventario->cantidad = $request->cantidad;
                $inventario->total = $request->precio * $request->cantidad;
                $inventario->inventario_bodegas_id = $request->inventario_bodegas_id_origen;
                $inventario->users_id = $userActual->id;
                $inventario->roles_id = $userActual->ID_rol;
            }
            /** Guardamos los cambios en el inventario**/
            $inventario->save();

        } /** Si el tipo de movimiento es 2, es una salida de inventario**/
        else if ($request->tipo_movimiento == 2) {
            /** Cuando es una salida, el precio es una lista en la cual se guarda el id del inventario por esto es posible hacer el find
             * con el precio del producto **/
            $inventario = Inventario::find($request->precio);
            $inventario->cantidad = $inventario->cantidad - $request->cantidad;
            $inventario->total = $inventario->cantidad * $inventario->precio;
            $inventario->save();
        } /** Si el tipo de movimiento es 3, es un traslado a otra bodega **/
        else if ($request->tipo_movimiento == 3) {

            /**--------------------------------- SALIDA DE LA BODEGA ORIGEN ---------------------------------**/
            /** Cuando es una salida, el precio es una lista en la cual se guarda el id del inventario por esto es posible hacer el find
             * con el precio del producto **/
            $inventarioSalida = Inventario::find($request->precio);
            $inventarioSalida->cantidad = $inventarioSalida->cantidad - $request->cantidad;
            $inventarioSalida->total = $request->precio * $request->cantidad;
            $inventarioSalida->save();

            /**--------------------------------- ENTRADA A LA BODEGA DESTINO ---------------------------------**/

            /**
             * Consultamos si el producto con el precio ya se encuentra en inventario, esto con el fin de no crear un nuevo registro
             * y simplemente actualizar la cantidad
             */
            $inventario = Inventario::where('compras_productos_id', $request->compras_productos_id)
                ->where('inventario_bodegas_id', $request->inventario_bodegas_id_destino)
                ->where('precio', $inventarioSalida->precio)->first();

            /**
             * Si el producto y el precio ya se encontraban en una bodega, debemos actualizar los valores en inventario y no crear un nuevo registro
             */
            if ($inventario) {
                $inventario->cantidad = $inventario->cantidad + $request->cantidad;
            } /** Si el producto nose encontraba en inventario, debemos crear el registro y guardarlo en la BD **/
            else {
                /**
                 * Creamos el objeto del modelo de inventario para guardar la entrada
                 */
                $inventario = new Inventario($request->all());
                $inventario->precio = $inventarioSalida->precio;
                $inventario->inventario_bodegas_id = $request->inventario_bodegas_id_destino;
                $inventario->users_id = $userActual->id;
                $inventario->roles_id = $userActual->ID_rol;
            }
            /** Guardamos los cambios en el inventario**/
            $inventario->save();
        }

        /** Calculo del precio promedio **/
        $request->precio_promedio = $this->precioPromedio($request);
        /**
         * Guardamos el movimiento que se acaba de realizar en el inventario
         */
        $this->guardarMovimiento($request, $inventario->id);

        flash('Movimiento realizado en inventario con exito', 'success');
        return redirect()->route('inventario.movimientos');
    }

    /**
     * Calcula el precio promedio de un producto usando
     * el método del promedio ponderado o costo promedio ponderado (CPP)
     * @param $request información del producto y la bodega que se va calcular el precio
     */
    public function precioPromedio($request)
    {
        /** Contiene el precio promedio del producto**/
        $precioPromedio = 0;

        /**
         * Consultamos el inventario del producto en la bodega
         */
        $inventarioProductos = Inventario::where('compras_productos_id', $request->compras_productos_id)
            ->where('inventario_bodegas_id', $request->inventario_bodegas_id_origen)
            ->where('cantidad', '>', 0);

        /** Variables para sumar cantidades y totales (cantidad*precio)**/
        $sumaTotal = 0;
        $sumaCantidad = 0;

        /**
         * Leemos todos los productos que encontramos en la bodega,
         * sumamos las cantidades y los totales
         */
        foreach ($inventarioProductos->get() As $inventarioProducto) {
            $sumaTotal += $inventarioProducto->total;
            $sumaCantidad += $inventarioProducto->cantidad;
        }

        /** Si la suma de las cantidades es mayor que cero, entonces podemos calcular el precio promedio**/
        if ($sumaCantidad > 0) {
            $precioPromedio = $sumaTotal / $sumaCantidad;
        }

        /** Actualizamos el precio promedio de todos los productos de la bodega**/
        $inventarioProductos->update(['precio_promedio' => $precioPromedio]);

        return $precioPromedio;

    }

    /**
     * Almacena el registro en la tabla de movimientos
     * @param $request datos del movimiento
     * @param id identificador del registro que se acaba de guardar en inventario
     */
    public function guardarMovimiento($request, $id)
    {
        /** Obtenemos el usuario actual **/
        $userActual = Auth::user();

        $movimientoInventario = new InventarioMovimientos();
        $movimientoInventario->compras_tipo_producto_id = $request->compras_tipo_producto_id;
        $movimientoInventario->compras_productos_id = $request->compras_productos_id;
        $movimientoInventario->precio = $request->precio;
        $movimientoInventario->total = $request->precio * $request->cantidad;
        $movimientoInventario->precio_promedio = $request->precio_promedio;

        $movimientoInventario->inventario_bodegas_id = $request->inventario_bodegas_id_origen;
        $movimientoInventario->inventario_bodegas_id_destino = $request->inventario_bodegas_id_destino;
        $movimientoInventario->inventario_id = $id;
        $movimientoInventario->inventario_motivos_movimiento_id = $request->inventario_motivos_movimiento_id;
        $movimientoInventario->users_id = $userActual->id;
        $movimientoInventario->roles_id = $userActual->ID_rol;
        $movimientoInventario->tipo_movimiento = $request->tipo_movimiento;
        $movimientoInventario->cantidad = $request->cantidad;
        $movimientoInventario->observaciones = $request->observaciones;
        $movimientoInventario->save();
    }

    /**
     * Muestra el formulario para elegir que inventario se quiere ver
     * @return $this
     */
    public function formularioInventario()
    {
        /** Bodegas disponibles**/
        $bodegas = Bodega::where('habilitado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        /** Tipos de producto **/
        $tipoProducto = TipoProducto::where('eliminado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');

        return view('vendor.foods-online.inventario.reportes.inventario.index')
            ->with('tipoProducto', $tipoProducto)
            ->with('bodegas', $bodegas);
    }

    /**
     * Retorna el inventario de una bodega de un tipo de producto
     * @param $id_bodega el id de la bodega
     * @param $tipo_producto el id del tipo de producto
     * @return mixed
     */
    public function reporteInventario($id_bodega, $tipo_producto)
    {
        $inventarios = Inventario::where('inventario_bodegas_id', $id_bodega)
            ->where('compras_tipo_producto_id', $tipo_producto)
            ->where('cantidad', '>', 0)
            ->get();
        /**
         * Leemos todos los productos en inventario
         */
        foreach ($inventarios As $inventario) {
            /**
             * Asignamos nuevos atributos que tomará el Script JQuery
             */
            $inventario->productName = $inventario->Producto->nombre;
            $inventario->um = $inventario->Producto->UnidadDeMedida->abreviatura;
            $inventario->stock = 0;
            $inventario->movimientos = 0;
            /**
             * Comprobamos si el producto de inventario tiene una configuración para traer su stock minimo
             */
            if ($inventario->Producto->configInventario) {
                $inventario->stock = $inventario->Producto->configInventario->stock_minimo;
            }

            /**
             * Comprobamos si tiene movimientos para habilitar o deshabilitar el botón
             */
            if ($inventario->Movimientos) {
                $inventario->movimientos = 1;
            }
        }
        return $inventarios;
    }

    /**
     * Retorna los movimientos de un registro de inventario
     * @param $id_inventario el id del registro que queremos consultar
     * @return mixed
     */
    public function getMovimientos($id_inventario)
    {
        /**
         * Consultamos los movimientos de un registro de inventario
         */
        $movimientosInventario = InventarioMovimientos::where('inventario_id', $id_inventario)->get();

        foreach ($movimientosInventario As $movimientoInventario) {

            /**
             *  Asignamos el tipo de movimiento debido a que estos valores son fijos
             **/
            $tipoMovimiento = "-";

            /** Entrada **/
            if ($movimientoInventario->tipo_movimiento == 1) {
                $tipoMovimiento = "Entrada";
            } /** Salida **/
            else if ($movimientoInventario->tipo_movimiento == 2) {
                $tipoMovimiento = "Salida";
            } /** Traslado **/
            else if ($movimientoInventario->tipo_movimiento == 3) {
                $tipoMovimiento = "Traslado";
            }

            $movimientoInventario->fecha = $movimientoInventario->created_at->toDateString();
            $movimientoInventario->productName = $movimientoInventario->Inventario->Producto->nombre;
            $movimientoInventario->precio = $movimientoInventario->Inventario->precio;
            $movimientoInventario->cantidad = $movimientoInventario->cantidad;
            $movimientoInventario->um = $movimientoInventario->Inventario->Producto->UnidadDeMedida->abreviatura;
            $movimientoInventario->motivo = $movimientoInventario->MotivoMovimiento->nombre;
            $movimientoInventario->bodegaOrigen = $movimientoInventario->BodegaOrigen->nombre;
            $movimientoInventario->bodegaDestino = $movimientoInventario->tipo_movimiento == 3 ? $movimientoInventario->BodegaDestino->nombre : "-";
            $movimientoInventario->responsable = $movimientoInventario->User->name;
        }

        return $movimientosInventario;
    }

    /**
     * Retorna una colección de todos los productos de un tipo de producto
     * @param $id identificador del tipo de producto
     * @return mixed
     */
    public function getProductos($id)
    {
        $productos = Producto::where('eliminado', null)->where('compras_tipo_producto_id', $id)->get();

        foreach ($productos As $producto) {
            $producto->nombre = $producto->nombre . " (" . $producto->UnidadDeMedida->nombre . ")";
        }
        return $productos;
    }


    /**
     * Retorna una colección de todos los precios de un producto en una bodega
     * @param $id identificador del tipo de producto
     * @return mixed
     */
    public function getprecios($id_bodega, $id_producto)
    {
        $inventarioprecios = Inventario::where('compras_productos_id', $id_producto)
            ->where('inventario_bodegas_id', $id_bodega)
            ->where('cantidad', '>', 0)
            ->orderBy('precio', 'ASC')
            ->get();
        foreach ($inventarioprecios As $inventarioprecio) {
            $inventarioprecio->nombre = "$ " . $inventarioprecio->precio;

        }
        return $inventarioprecios;
    }

    /**
     * Retorna el inventario de un precio especifico
     * @param $id_inventario el id del inventario
     * @return mixed
     */
    public function getCantidad($id_inventario)
    {
        $inventario = Inventario::find($id_inventario);
        return $inventario;
    }


    public function formularioReporteMovimientos()
    {
        /** Bodegas disponibles**/
        $bodegas = Bodega::where('habilitado', null)->orderBy('nombre', 'ASC')->pluck('nombre', 'id');
        /** Tipos de producto **/
        $tipoProductos = TipoProducto::where('eliminado', null)->orderBy('nombre', 'ASC')->get();
        /** Lista de productos **/
        $productos[] = null;

        /**
         * Como se inicializa en null, sacamos ese valor de la lista para que no quede ningún elemento
         **/
        array_pop($productos);

        /**
         * Leemos todos los tipos de prodocto
         */
        foreach ($tipoProductos As $tipoProducto) {

            /** Creamos una lista vacia para meter los productos del tipo de producto que estamos leyendo **/
            $productosLista[] = null;
            /**
             * Como se inicializa en null, sacamos ese valor de la lista para que no quede ningún elemento
             **/
            array_pop($productosLista);

            /**
             * Leemos los productos del tipo de producto actual
             */
            foreach ($tipoProducto->Productos As $producto) {
                /** Agregamos los tipos de producto a la lista**/
                $productosLista[$producto->id] = $producto->nombre;
            }

            /** Si la lista de productos tiene algún producto, creamos un arreglo asociativo con el nombre del tipo de producto
             * y asignamos la lista de producto del mismo**/
            if (count($productosLista)) {
                $productos[$tipoProducto->nombre] = $productosLista;
            }
            /** Asignamos la variable que contiene la lista**/
            $productosLista = null;
        }

        /** Si no se encontraron productos, debemos asignar una lista vacia para enviar a select en la vista**/
        if (!count($productos)) {
            $productos[] = [];
        }

        return view('vendor.foods-online.inventario.reportes.movimientos.index')
            ->with('bodegas', $bodegas)
            ->with('productos', $productos);
    }

    public function consultarMovimientos(Request $request)
    {
        /**
         * Consultamos los movimientos de inventario
         */
        $movimientosInventario = InventarioMovimientos::where('inventario_bodegas_id', $request->inventario_bodegas_id)
            ->where('compras_productos_id', $request->compras_productos_id)
//            ->whereBetween('created_at', [$request->fecha_desde, $request->fecha_hasta])
            ->whereDate('created_at','>=' ,$request->fecha_desde)
            ->whereDate('created_at','<=' ,$request->fecha_hasta)
            ->orderBy('created_at', 'DESC')
            ->paginate(20);

//        dd($movimientosInventario);
        return view('vendor.foods-online.inventario.reportes.movimientos.movimientos')
            ->with('movimientosInventario', $movimientosInventario);
    }


}
