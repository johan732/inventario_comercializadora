<?php

namespace App\Http\Controllers\inventario\configuracion;

use App\src\inventario\configuracion\TipoAlmacenamiento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class AlmacenamientoController
 * Controlador para el manejo de los métodos de los tipos de almacenamiento de una bodega
 * @package App\Http\Controllers\inventario\configuracion
 */
class AlmacenamientoController extends Controller
{
    public function index()
    {
        /** Mostramos solo los productos que estén habilitados **/
        $tiposAlmacenamiento = TipoAlmacenamiento::orderBy('nombre', 'ASC')
            ->paginate(20);

        return view('vendor.foods-online.inventario.configuracion.almacenamiento.index')->with('tiposAlmacenamiento', $tiposAlmacenamiento);
    }

    /**
     * Método para crear un tipo de almacenamiento
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('vendor.foods-online.inventario.configuracion.almacenamiento.create');
    }

    /**
     * Crea un nuevo tipó de almacenamiento
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $tipoAlmacenamiento = new TipoAlmacenamiento($request->all());
        $tipoAlmacenamiento->save();

        flash('Se ha creado el tipo de almacenamiento correctamente', 'success');
        return redirect()->route('inventario.configuracion.almacenamiento.index');
    }

    /**
     * Método que retorna una vista los datos de un tipo de almacenamiento para editar
     * @param $id el id del tipo de almacenamiento que va a editar
     * @return $this
     */
    public function edit($id)
    {
        $tipoAlmacenamiento = TipoAlmacenamiento::find($id);
        return view('vendor.foods-online.inventario.configuracion.almacenamiento.edit')
            ->with('tipoAlmacenamiento', $tipoAlmacenamiento);
    }

    /**
     * Método que actualiza los datos de un tipo de almacenamiento que se recibe por un formulario de edición
     * @param Request $request los nuevos datos del tipo de almacenamiento
     * @param $id el id del tipo de almacenamiento que se va a actualizar
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TipoAnalisisRequest $request, $id)
    {
        $tipoAlmacenamiento = TipoAlmacenamiento::find($id);
        $tipoAlmacenamiento->fill($request->all());


        /** ----------- COMPROBACIÓN DE LOS CHECKBOX ----------- */

        /** Si no se marca el checbox de deshabilitado se cambia el valor para que lo desmarque */
        if (!$request->habilitado) {
            $tipoAlmacenamiento->habilitado = null;
        }
        /** ---------------------------------------------------- */

        $tipoAlmacenamiento->save();

        flash('Se ha actualizado el tipo de almacenamiento correctamente', 'success');
        return redirect()->route('inventario.configuracion.almacenamiento.index');

    }

    /**
     * Método que deshabilita un tipo de almacenamiento de la base de datos
     * se cambia el campo habilitad = 1 para indicar que está eliminado
     * no se elimina el registro debido a que pueden haber datos relacionados con este dato
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $tipoAlmacenamiento = TipoAlmacenamiento::find($id);
        $tipoAlmacenamiento->habilitado = 1;
        $tipoAlmacenamiento->save();

        flash('Se ha deshabilitado el tipo de almacenamiento correctamente', 'danger');
        return redirect()->route('inventario.configuracion.almacenamiento.index');
    }

    /**
     * Método que habilita un tipo de almacenamiento de la base de datos
     * se cambia el campo habilitado = null para indicar que no está eliminado
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function habilitar($id)
    {
        $tipoAlmacenamiento = TipoAlmacenamiento::find($id);
        $tipoAlmacenamiento->habilitado = null;
        $tipoAlmacenamiento->save();

        flash('Se ha habilitado el tipo de almacenamiento correctamente', 'success');
        return redirect()->route('inventario.configuracion.almacenamiento.index');

    }
}
