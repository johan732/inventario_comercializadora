<?php

namespace App\Http\Controllers\inventario\configuracion;

use App\src\compras\configuracion\productos\Producto;
use App\src\inventario\configuracion\ProductoInventario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


/**
 * Class ProductoController
 * Controlador para los métodos de configuración de un producto de inventario
 * @package App\Http\Controllers\inventario\configuracion
 */
class ProductoController extends Controller
{
    /**
     * Retorna todos los productos creados para cambiar la configuración para inventarios
     * @return $this
     */
    public function index()
    {
        /** Mostramos solo los productos que estén habilitados **/
        $productos = Producto::where('eliminado', null)
            ->orderBy('nombre', 'ASC')
            ->paginate(20);

        return view('vendor.foods-online.inventario.configuracion.productos.index')->with('productos', $productos);
    }

    /**
     * Permite la editar la configuración de un producto para el módulo de inventario
     * @param $id el id del producto que se va a editar
     * @return $this
     */
    public function edit($id)
    {
        /** Consultamos los datos del producto **/
        $producto = Producto::find($id);

        /** Consultamos la configuración del producto **/
        $configuracionProducto = ProductoInventario::where('compras_productos_id', $id)->first();

        return view('vendor.foods-online.inventario.configuracion.productos.edit')
            ->with('producto', $producto)
            ->with('configuracionProducto', $configuracionProducto);
    }

    public function update(Request $request, $id)
    {
        /** Comprobamos si la configuración del producto ya había sido creada  **/
        $productoInventario = ProductoInventario::where('compras_productos_id', $id)->first();

        /** Si encontró algún registro de este producto, actualizamos los valores **/
        if ($productoInventario) {
            $productoInventario->fill($request->all());
            $productoInventario->save();
        } /** Si no había una configuración creada, creamos el registro y lo guardamos **/
        else {
            $nuevaConfiguracion = new ProductoInventario($request->all());
            $nuevaConfiguracion->compras_productos_id = $id;
            $nuevaConfiguracion->save();
        }

        flash('Se ha actualizado el producto correctamente', 'success');
        return redirect()->route('inventario.configuracion.productos.index');

    }
}
