<?php

namespace App\Http\Controllers\inventario\configuracion;

use App\src\inventario\configuracion\MotivoMovimiento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MotivoController extends Controller
{
    /**
     * Muestra la lista de motivos para entradas y salidas creados
     * @return $this
     */
    public function index()
    {
        $motivos = MotivoMovimiento::orderBy("nombre", 'ASC')->paginate(20);
        return view('vendor.foods-online.inventario.configuracion.motivos.index')->with('motivos', $motivos);
    }

    /**
     * Muestra la vista para la creación de un motivo de movimiento
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('vendor.foods-online.inventario.configuracion.motivos.create');
    }

    /**
     * Guarda en la BD los datos del nuevo motivo de movimiento
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $motivo = new MotivoMovimiento($request->all());
        $motivo->save();

        flash('Se ha creado el motivo de movimiento corectamente', 'success');
        return redirect()->route('inventario.configuracion.motivos.index');
    }


    /**
     * Muestra el formulario para la edición de un motivo de movimiento de inventario
     * @param $id el id del movimiento
     * @return $this
     */
    public function edit($id)
    {

        $motivo = MotivoMovimiento::find($id);
        return view('vendor.foods-online.inventario.configuracion.motivos.edit')
            ->with('motivo', $motivo);
    }


    /**
     * Actualiza un motivo de movimiento de la BD
     * @param Request $request datos del formulario
     * @param $id el id del movimiento
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $motivo = MotivoMovimiento::find($id);
        $motivo->fill($request->all());
        $motivo->save();

        flash('Se ha actualizado el motivo de movimiento corectamente', 'success');
        return redirect()->route('inventario.configuracion.motivos.index');
    }


    /**
     * Método que deshabilita un motivo de movimiento de inventario de la base de datos
     * se cambia el campo habilitad = 1 para indicar que está eliminado
     * no se elimina el registro debido a que pueden haber datos relacionados con este dato
     * @param $id el id de la bodega
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $motivo = MotivoMovimiento::find($id);
        $motivo->habilitado = 1;
        $motivo->save();

        flash('Se ha deshabilitado el motivo de movimiento correctamente', 'danger');
        return redirect()->route('inventario.configuracion.motivos.index');
    }

    /**
     * Método que habilita  un motivo de movimiento de inventario de la base de datos
     * se cambia el campo habilitado = null para indicar que no está eliminado
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function habilitar($id)
    {
        $motivo = MotivoMovimiento::find($id);
        $motivo->habilitado = null;
        $motivo->save();

        flash('Se ha habilitado el motivo de movimiento correctamente', 'success');
        return redirect()->route('inventario.configuracion.motivos.index');

    }
}
