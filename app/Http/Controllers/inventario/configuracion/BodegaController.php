<?php

namespace App\Http\Controllers\inventario\configuracion;

use App\src\inventario\configuracion\Bodega;
use App\src\inventario\configuracion\TipoAlmacenamiento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BodegaController extends Controller
{
    /**
     * Muestra la lista de bodegas creadas
     * @return $this
     */
    public function index()
    {
        $bodegas = Bodega::orderBy("nombre", 'ASC')->paginate(20);
        return view('vendor.foods-online.inventario.configuracion.bodegas.index')->with('bodegas', $bodegas);
    }

    /**
     * Método para crear un tipo de almacenamiento
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        /** Mostramos solo los tipos de almacenamiento que estén habilitados **/
        $tiposAlmacenamiento = TipoAlmacenamiento::orderBy('nombre', 'ASC')->pluck('nombre', 'id');

        return view('vendor.foods-online.inventario.configuracion.bodegas.create')
            ->with('tiposAlmacenamiento', $tiposAlmacenamiento);
    }

    /**
     * Crea una bodega
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $bodega = new Bodega($request->all());
        $bodega->save();

        flash('Se ha creado la bodega', 'success');
        return redirect()->route('inventario.configuracion.bodegas.index');
    }


    /**
     * Muestra el formulario para la edición de una bodegas
     * @param $id el id de la bodega
     * @return $this
     */
    public function edit($id)
    {
        /** Mostramos solo los tipos de almacenamiento que estén habilitados **/
        $tiposAlmacenamiento = TipoAlmacenamiento::orderBy('nombre', 'ASC')->pluck('nombre', 'id');

        /** Consultamos la bodega que queremos editar **/
        $bodega = Bodega::find($id);

        return view('vendor.foods-online.inventario.configuracion.bodegas.edit')
            ->with('tiposAlmacenamiento', $tiposAlmacenamiento)
            ->with('bodega', $bodega);

    }


    /**
     * Actualiza los datos de una bodega
     * @param Request $request los datos del formulario
     * @param $id el id de la bodega que se actualiza
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $bodega = Bodega::find($id);
        $bodega->fill($request->all());

        /** ----------- COMPROBACIÓN DE LOS CHECKBOX ----------- */

        /** Si no se marca el checbox de deshabilitado se cambia el valor para que lo desmarque */
        if (!$request->habilitado) {
            $bodega->habilitado = null;
        }
        /** ---------------------------------------------------- */

        $bodega->save();

        flash('Se ha actualizado la bodega', 'success');
        return redirect()->route('inventario.configuracion.bodegas.index');
    }


    /**
     * Método que deshabilita una  bodega de la base de datos
     * se cambia el campo habilitad = 1 para indicar que está eliminado
     * no se elimina el registro debido a que pueden haber datos relacionados con este dato
     * @param $id el id de la bodega
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $bodega = Bodega::find($id);
        $bodega->habilitado = 1;
        $bodega->save();

        flash('Se ha deshabilitado la bodega correctamente', 'danger');
        return redirect()->route('inventario.configuracion.bodegas.index');
    }

    /**
     * Método que habilita una bodega la base de datos
     * se cambia el campo habilitado = null para indicar que no está eliminado
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function habilitar($id)
    {
        $bodega = Bodega::find($id);
        $bodega->habilitado = null;
        $bodega->save();

        flash('Se ha habilitado la bodega correctamente', 'success');
        return redirect()->route('inventario.configuracion.bodegas.index');

    }

}
