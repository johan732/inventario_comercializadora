<?php

namespace App\src\sistema\configuracion\general;

use Illuminate\Database\Eloquent\Model;

class UnidadDeMedida extends Model
{
    protected $table = 'general_unidad_medida';
    protected $fillable = ['nombre', 'abreviatura', 'eliminado'];
}
