<?php

namespace App\src\inventario\configuracion;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductoInventario
 * Modelo para la configuración de un producto en inventario
 * @package App\src\inventario\configuracion
 */
class ProductoInventario extends Model
{
    protected $table = 'inventario_configuracion_productos';
    protected $fillable = ['compras_productos_id', 'unidadesxcaja', 'stock_minimo'];

}
