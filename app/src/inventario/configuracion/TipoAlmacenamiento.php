<?php

namespace App\src\inventario\configuracion;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TipoAlmacenamiento
 * Modelo para el manejo del tipo almacenamiento que se tiene en una bodega
 * @package App\src\inventario\configuracion
 */
class TipoAlmacenamiento extends Model
{
    protected $table = 'inventario_tipo_almacenamiento';
    protected $fillable = ['nombre','habilitado'];
}
