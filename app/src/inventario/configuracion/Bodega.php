<?php

namespace App\src\inventario\configuracion;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bodega
 * Modelo para el manejo de las bodegas de inventario
 * @package App\src\inventario\configuracion
 */
class Bodega extends Model
{
    protected $table = 'inventario_bodegas';
    protected $fillable = ['nombre','inventario_tipo_almacenamiento_id','habilitado'];

    /**
     * Una bodega posee un tipo de almacenamiento
     * relación 1:N
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function TipoAlmacenamiento()
    {
        return $this->hasOne('App\src\inventario\configuracion\TipoAlmacenamiento', 'id', 'inventario_tipo_almacenamiento_id');
    }

}
