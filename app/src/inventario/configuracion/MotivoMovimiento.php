<?php

namespace App\src\inventario\configuracion;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MotivoMovimiento
 * Modelo para el manejo de los motivos de inventario
 * @package App\src\inventario\configuracion
 */
class MotivoMovimiento extends Model
{
    protected $table = 'inventario_motivos_movimiento';
    protected $fillable = ['nombre','habilitado'];
}
