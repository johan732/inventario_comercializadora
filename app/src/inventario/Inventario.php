<?php

namespace App\src\inventario;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Inventario
 * Modelo para el manejo de inventario
 * @package App\src\inventario
 */
class Inventario extends Model
{
    protected $table = 'inventario';
    protected $fillable = ['compras_ordenes_recibido_id', 'compras_tipo_producto_id', 'compras_productos_id', 'inventario_bodegas_id', 'users_id', 'roles_id', 'cantidad', 'precio','total','precio_promedio', 'observaciones'];

    /**
     * Cada elemento del inventario pertenece solo a una bodega
     * relación 1:N
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Bodega()
    {
        return $this->hasOne('App\src\inventario\configuracion\Bodega', 'id', 'inventario_bodegas_id');
    }

    /**
     * El ingreso a el inventario es realizada por un usuario
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User()
    {
        return $this->hasOne('App\User', 'id', 'users_id');
    }

    /**
     * El usuario que realizó el ingreso a inventario tenía un rol especifico cuando la creó
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Rol()
    {
        return $this->hasOne('App\src\sistema\usuario\rol\Rol', 'id', 'roles_id');
    }

    /**
     * Un registro de inventario puede tener muchos motivos
     * relación 1:N
     * @return mixed
     */
    public function Movimientos()
    {
        return $this->hasMany('App\src\inventario\InventarioMovimientos', 'inventario_id', 'id')
            ->orderBy('created_at', 'ASC');
    }

    /**
     * Cada registro de inventario se refiere a un solo tipo de producto
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function TipoProducto()
    {
        return $this->hasOne('App\src\compras\configuracion\productos\TipoProducto', 'id', 'compras_tipo_producto_id');
    }

    /**
     * Cada registro de inventario se refiere a un solo producto
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Producto()
    {
        return $this->hasOne('App\src\compras\configuracion\productos\Producto', 'id', 'compras_productos_id');
    }
}
