<?php

namespace App\src\inventario;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InventarioMovimientos
 * Clase para el manejo de los motivos de inventario
 * @package App\src\inventario
 */
class InventarioMovimientos extends Model
{
    protected $table = 'inventario_movimientos';
    protected $fillable = ['compras_tipo_producto_id', 'compras_productos_id', 'inventario_bodegas_id', 'inventario_bodegas_id_destino', 'inventario_id', 'inventario_motivos_movimiento_id', 'users_id', 'roles_id', 'tipo_movimiento', 'cantidad', 'precio', 'total', 'precio_promedio', 'observaciones', 'created_at'];

    /**
     * Un movimiento en el inventario es realizada por un usuario
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User()
    {
        return $this->hasOne('App\User', 'id', 'users_id');
    }

    /**
     * Un movimiento de inventario posee a un único motivo
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function MotivoMovimiento()
    {
        return $this->hasOne('App\src\inventario\configuracion\MotivoMovimiento', 'id', 'inventario_motivos_movimiento_id');
    }

    /**
     * Cada registro pertenece a un registro de inventario
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Inventario()
    {
        return $this->hasOne('App\src\inventario\Inventario', 'id', 'inventario_id');
    }


    /**
     * Cada movimiento en inventario se realiza de una bodega, en este caso siempre tendrá bodega origen
     * bodega destino unicamente cuando el tipo de movimiento sea traslado (3)
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function BodegaOrigen()
    {
        return $this->hasOne('App\src\inventario\configuracion\Bodega', 'id', 'inventario_bodegas_id');
    }

    /**
     * Cuando el tipo de movimiento es traslado(3) esta relación es posible y puede hacerse el llamado
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function BodegaDestino()
    {
        return $this->hasOne('App\src\inventario\configuracion\Bodega', 'id', 'inventario_bodegas_id_destino');
    }

    /**
     * Cada registro de inventario se refiere a un solo tipo de producto
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function TipoProducto()
    {
        return $this->hasOne('App\src\compras\configuracion\productos\TipoProducto', 'id', 'compras_tipo_producto_id');
    }

    /**
     * Cada registro de inventario se refiere a un solo producto
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Producto()
    {
        return $this->hasOne('App\src\compras\configuracion\productos\Producto', 'id', 'compras_productos_id');
    }

}
