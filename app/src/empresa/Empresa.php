<?php

namespace App\src\empresa;

use Illuminate\Database\Eloquent\Model;

/**
 * Módelo para la tabla que contiene los datos de la organización
 * Class Empresa
 * @package App\src\empresa
 */
class Empresa extends Model
{
    protected $table = 'empresa';
    protected $fillable = ['id', 'nit', 'nombre', 'direccion', 'logo', 'telefono', 'celular', 'website', 'municipios_id'];

    /**
     * Una empresa pertenece solo a un municipio
     * relación 1:N
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function municipio()
    {
        return $this->hasOne('App\src\sistema\municipios\Municipios', 'id', 'municipios_id');
    }
}
