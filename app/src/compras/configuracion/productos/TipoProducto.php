<?php

namespace App\src\compras\configuracion\productos;

use Illuminate\Database\Eloquent\Model;

class TipoProducto extends Model
{
    protected $table = 'compras_tipo_producto';
    protected $fillable = ['nombre','eliminado'];

    /**
     * Un Tipo de producto puede ser varios productos
     * relación N:N
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Productos()
    {
        return $this->hasMany('App\src\compras\configuracion\productos\Producto','compras_tipo_producto_id','id');
    }
}
