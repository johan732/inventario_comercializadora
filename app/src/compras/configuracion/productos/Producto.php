<?php

namespace App\src\compras\configuracion\productos;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de datos de la tabla de los productos
 * Class ProductoInventario
 * @package App\src\compras\configuracion\productos
 */
class Producto extends Model
{
    protected $table = 'compras_productos';
    protected $fillable = ['nombre', 'compras_tipo_producto_id', 'general_unidad_medida_id', 'eliminado'];

    /**
     * Un producto solo tiene un solo tipo de producto
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function TipoProducto()
    {
        return $this->hasOne('App\src\compras\configuracion\productos\TipoProducto', 'id', 'compras_tipo_producto_id');
    }

    /**
     * Un producto tiene solo una unidad de medida
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function UnidadDeMedida()
    {
        return $this->hasOne('App\src\sistema\configuracion\general\UnidadDeMedida', 'id', 'general_unidad_medida_id');
    }


    public function scopeSearch($query, $buscar)
    {
        return $query->where('nombre', 'LIKE', "%$buscar%");
    }

    /**
     * Un producto tiene una configuración en inventario
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function configInventario()
    {
        return $this->hasOne('App\src\inventario\configuracion\ProductoInventario', 'compras_productos_id', 'id');
    }

}
