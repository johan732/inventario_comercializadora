<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Usamos el middleware 'auth' para comprobar que solo un usuario que se ha logueado podrá acceder
 * a las rutas
 */
Route::group(['middleware' => ['auth', 'permisos']], function () {


    /** ----------------------------------------- MENUS ------------------------------------------ */
    /**
     * Para el manejo de los menús
     */
    Route::resource('menu', 'sistema\menu\MenuController');
    Route::get('menu/{id}/destroy', [
        'uses' => 'sistema\menu\MenuController@destroy',
        'as' => 'menu.destroy'
    ]);

    /** Ruta para los selects dependientes que consultan los hijos de un padre */
    Route::get('menu/hijos/{id}', [
        'uses' => 'sistema\menu\MenuController@getHijos',
        'as' => 'menu.hijos'
    ]);


    /** --------------------------------------- ROLES -------------------------------------------- */
    /**
     * Para el manejo de los roles
     */
    Route::resource('rol', 'sistema\usuario\rol\RolController');
    Route::get('rol/{id}/destroy', [
        'uses' => 'sistema\usuario\rol\RolController@destroy',
        'as' => 'rol.destroy'
    ]);

    /** ------------------------------------------ USUARIOS ----------------------------------------- */
    /**
     * Para el manejo de los roles
     */
    Route::resource('user', 'sistema\usuario\usuario\UserController');
    Route::get('user/{id}/destroy', [
        'uses' => 'sistema\usuario\usuario\UserController@destroy',
        'as' => 'user.destroy'
    ]);

    /** --------------------------------------- PERMISOS -------------------------------------------- */
    /**
     * Para el manejo de los permisos
     */
    Route::resource('permiso', 'sistema\usuario\permiso\PermisoController');
    Route::get('permiso/{id}/destroy', [
        'uses' => 'sistema\usuario\permiso\permisoController@destroy',
        'as' => 'permiso.destroy'
    ]);


    /** -------------------------------------- EMPRESA --------------------------------------------- */
    /**
     * Para el manejo de los datos de la empresa
     */
    Route::resource('empresa', 'empresa\EmpresaController');
    Route::get('empresa/{id}/destroy', [
        'uses' => 'empresa\EmpresaController@destroy',
        'as' => 'empresa.destroy'
    ]);

    /** Ruta para los selects dependientes que consultan los municipios de una departamento */
    Route::get('empresa/municipios/{id}', [
        'uses' => 'empresa\EmpresaController@getMunicipios',
        'as' => 'empresa.municipios'
    ]);


    /** ------------------------------------- COMPRAS - TIPO PRODUCTO ---------------------------------------------- */
    /**
     * Para la configuración de los tipos de productos que requieren análisis y que análisis se asignaran
     */
    Route::resource('compras/configuracion/productos/tipo', 'compras\configuracion\productos\TipoProductoController', ['names' => [
        'index' => 'compras.configuracion.productos.tipo.index',
        'create' => 'compras.configuracion.productos.tipo.create',
        'store' => 'compras.configuracion.productos.tipo.store',
        'edit' => 'compras.configuracion.productos.tipo.edit',
        'update' => 'compras.configuracion.productos.tipo.update',
        'show' => 'compras.configuracion.productos.tipo.show',
        'destroy' => 'compras.configuracion.productos.tipo.destroy',
    ]]);

    /**
     * Habilita el tipo de producto
     */
    Route::get('compras/configuracion/productos/tipo/{id}/habilitar', [
        'uses' => 'compras\configuracion\productos\TipoProductoController@habilitar',
        'as' => 'compras.configuracion.productos.tipo.habilitar'
    ]);

    /**
     * Deshabilita el tipo de producto
     */
    Route::get('compras/configuracion/productos/tipo/{id}/destroy', [
        'uses' => 'compras\configuracion\productos\TipoProductoController@destroy',
        'as' => 'compras.configuracion.productos.tipo.destroy'
    ]);

    /** ------------------------------------- COMPRAS - PRODUCTOS ---------------------------------------------- */
    /**
     * Para la configuración de los productos de compras
     */
    Route::resource('compras/configuracion/productos', 'compras\configuracion\productos\ProductosController', ['names' => [
        'index' => 'compras.configuracion.productos.index',
        'create' => 'compras.configuracion.productos.create',
        'store' => 'compras.configuracion.productos.store',
        'edit' => 'compras.configuracion.productos.edit',
        'update' => 'compras.configuracion.productos.update',
        'show' => 'compras.configuracion.productos.show',
        'destroy' => 'compras.configuracion.productos.destroy',
    ]]);

    Route::get('compras/configuracion/productos/{id}/habilitar', [
        'uses' => 'compras\configuracion\productos\ProductosController@habilitar',
        'as' => 'compras.configuracion.productos.habilitar'
    ]);

    Route::get('compras/configuracion/productos/{id}/destroy', [
        'uses' => 'compras\configuracion\productos\ProductosController@destroy',
        'as' => 'compras.configuracion.productos.destroy'
    ]);


    /** ------------------------------------- INVENTARIO - CONFIGURACIÓN - PRODUCTOS ---------------------------------------------- */
    /**
     * Para la configuración de los productos de inventario
     */
    Route::resource('inventario/configuracion/productos', 'inventario\configuracion\ProductoController', ['names' => [
        'index' => 'inventario.configuracion.productos.index',
        'create' => 'inventario.configuracion.productos.create',
        'store' => 'inventario.configuracion.productos.store',
        'edit' => 'inventario.configuracion.productos.edit',
        'update' => 'inventario.configuracion.productos.update',
        'show' => 'inventario.configuracion.productos.show',
        'destroy' => 'inventario.configuracion.productos.destroy',
    ]]);
    /** ------------------------------------- INVENTARIO - CONFIGURACIÓN - TIPOS DE ALMACENAMIENTO ---------------------------------------------- */
    /**
     * Para la configuración de los tipos de almacenamietno de inventario
     */
    Route::resource('inventario/configuracion/almacenamiento', 'inventario\configuracion\AlmacenamientoController', ['names' => [
        'index' => 'inventario.configuracion.almacenamiento.index',
        'create' => 'inventario.configuracion.almacenamiento.create',
        'store' => 'inventario.configuracion.almacenamiento.store',
        'edit' => 'inventario.configuracion.almacenamiento.edit',
        'update' => 'inventario.configuracion.almacenamiento.update',
        'show' => 'inventario.configuracion.almacenamiento.show',
        'destroy' => 'inventario.configuracion.almacenamiento.destroy',
    ]]);

    /** -------- habilitar tipo de almacenamiento -------- */
    Route::get('inventario/configuracion/almacenamiento/{id}/habilitar', [
        'uses' => 'inventario\configuracion\AlmacenamientoController@habilitar',
        'as' => 'inventario.configuracion.almacenamiento.habilitar'
    ]);

    /** -------- inhabilitar tipo de almacenamiento -------- */
    Route::get('inventario/configuracion/almacenamiento/{id}/destroy', [
        'uses' => 'inventario\configuracion\AlmacenamientoController@destroy',
        'as' => 'inventario.configuracion.almacenamiento.destroy'
    ]);
    /** ------------------------------------- INVENTARIO - CONFIGURACIÓN - BODEGAS ---------------------------------------------- */
    /**
     * Para la configuración de las bodegas de inventario
     */
    Route::resource('inventario/configuracion/bodegas', 'inventario\configuracion\BodegaController', ['names' => [
        'index' => 'inventario.configuracion.bodegas.index',
        'create' => 'inventario.configuracion.bodegas.create',
        'store' => 'inventario.configuracion.bodegas.store',
        'edit' => 'inventario.configuracion.bodegas.edit',
        'update' => 'inventario.configuracion.bodegas.update',
        'show' => 'inventario.configuracion.bodegas.show',
        'destroy' => 'inventario.configuracion.bodegas.destroy',
    ]]);

    /** -------- habilitar tipo de almacenamiento -------- */
    Route::get('inventario/configuracion/bodegas/{id}/habilitar', [
        'uses' => 'inventario\configuracion\BodegaController@habilitar',
        'as' => 'inventario.configuracion.bodegas.habilitar'
    ]);

    /** -------- inhabilitar tipo de almacenamiento -------- */
    Route::get('inventario/configuracion/bodegas/{id}/destroy', [
        'uses' => 'inventario\configuracion\BodegaController@destroy',
        'as' => 'inventario.configuracion.bodegas.destroy'
    ]);
    /** ------------------------------------- INVENTARIO - CONFIGURACIÓN - MOTIVOS MOVIMIENTO ---------------------------------------------- */
    /**
     * Para la configuración de las bodegas de inventario
     */
    Route::resource('inventario/configuracion/motivos', 'inventario\configuracion\MotivoController', ['names' => [
        'index' => 'inventario.configuracion.motivos.index',
        'create' => 'inventario.configuracion.motivos.create',
        'store' => 'inventario.configuracion.motivos.store',
        'edit' => 'inventario.configuracion.motivos.edit',
        'update' => 'inventario.configuracion.motivos.update',
        'show' => 'inventario.configuracion.motivos.show',
        'destroy' => 'inventario.configuracion.motivos.destroy',
    ]]);


    /** -------- habilitar motivo de movimiento  -------- */
    Route::get('inventario/configuracion/motivos/{id}/habilitar', [
        'uses' => 'inventario\configuracion\MotivoController@habilitar',
        'as' => 'inventario.configuracion.motivos.habilitar'
    ]);

    /** -------- inhabilitar motivo de movimiento -------- */
    Route::get('inventario/configuracion/motivos/{id}/destroy', [
        'uses' => 'inventario\configuracion\MotivoController@destroy',
        'as' => 'inventario.configuracion.motivos.destroy'
    ]);

    /** ------------------------------------- INVENTARIO - MOVIMIENTOS  ---------------------------------------------- */
    Route::get('inventario/movimientos', [
        'uses' => 'inventario\InventarioController@formularioMovimientos',
        'as' => 'inventario.movimientos'
    ]);

    Route::post('inventario/movimientos/guardar', [
        'uses' => 'inventario\InventarioController@guardarMovimientos',
        'as' => 'inventario.movimientos.guardar'
    ]);

    /** Ruta para los selects dependientes que consultan los productos de un tipo de producto */
    Route::get('inventario/productos/{id}', [
        'uses' => 'inventario\InventarioController@getProductos',
        'as' => 'inventario.movimiento.productos'
    ]);

    /** Ruta para los selects dependientes que consultan los precios producto en una bodega*/
    Route::get('inventario/precios/{id_bodega}/{id_producto}', [
        'uses' => 'inventario\InventarioController@getprecios',
        'as' => 'inventario.movimiento.productos'
    ]);

    /** Ruta para los selects dependientes que consultan la cantidad de un precio en inventario **/
    Route::get('inventario/cantidad/{id_inventario}', [
        'uses' => 'inventario\InventarioController@getCantidad',
        'as' => 'inventario.cantidad'
    ]);

    /** ------------------------------------- INVENTARIO - REPORTES - INVENTARIO  ---------------------------------------------- */
    /** Formulario para consulta de inventario  **/
    Route::get('inventario/reporte/inventario', [
        'uses' => 'inventario\InventarioController@formularioInventario',
        'as' => 'inventario.reporte.inventario.form'
    ]);

    /** Consulta el inventario de una bodega y un tipo de producto */
    Route::get('inventario/reporte/inventario/{id_bodega}/{id_producto}', [
        'uses' => 'inventario\InventarioController@reporteInventario',
        'as' => 'inventario.reporte.inventario'
    ]);

    /** Consulta los movimientos de un registro de inventario  */
    Route::get('inventario/reporte/movimientos/{id_inventario}', [
        'uses' => 'inventario\InventarioController@getMovimientos',
        'as' => 'inventario.reporte.inventario.movimientos'
    ]);

    /** ------------------------------------- INVENTARIO - REPORTES - MOVIMIENTOS  ---------------------------------------------- */

    /** Formulario para seleccionar lo que se quiere ver en los movimientos**/
    Route::get('inventario/reporte/movimientos', [
        'uses' => 'inventario\InventarioController@formularioReporteMovimientos',
        'as' => 'inventario.reporte.movimientos.index'
    ]);

    /** Consultar los movimientos con los parametros especificados **/
    Route::post('inventario/reporte/movimientos/consultar', [
        'uses' => 'inventario\InventarioController@consultarMovimientos',
        'as' => 'inventario.reporte.movimientos.consultar'
    ]);

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});