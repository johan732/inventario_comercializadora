/**
 * Función para crear un campo dinamicamente
 * @param proveedorId
 * @param productosId
 * @param ruta
 * @param divlId
 */
function crearCampos(proveedorId, productosId, ruta, divlId) {

    var controlBoton = null;
    formulario = '';
    //Función AJAX que cambia los valores de la lista dinamicamente
    $.get(ruta + proveedorId + '/' + productosId, function (data) {
        console.log(data);

        formulario += '<table class="table table-bordered">';
        formulario += '<thead>';
        formulario += '<th class="">#</th>';
        formulario += '<th class="text-center">Referencia</th>';
        formulario += '<th>Producto</th>';
        formulario += '<th>Precio</th>';
        formulario += '<th>Cantidad</th>';
        formulario += '<th>Total</th>';
        formulario += '</thead>';
        formulario += '<tbody>';

        /***
         * Leemos todos los valores seleccionados y creamos los campos de precio para cada producto
         */
        for (var i = 0; i < data.length; i++) {

            /** Asignamos estas variables para facilitar el id de los campos **/
            var campoCantidadId = "campoCantidad" + i;
            var labelTotalId = "totalProducto" + i;

            /** Si el producto no tiene referencia, ponemos un guión**/
            if (!data[i].referencia) {
                data[i].referencia = "-";
            }

            formulario += '<tr>';
            formulario += '<td class="text-center">' + (i + 1) + '</td>';
            formulario += '<td class="text-center">' + data[i].referencia + '</td>';
            formulario += '<td>' + data[i].nombre + '</td>';

            /** Si el producto tiene precio, lo mostramos
             * Si tiene precio aumentamos el valor de la variable controlBotón para saber si habilitamos el botón de guardar
             * **/
            if (data[i].precio) {
                controlBoton++;
                formulario += '<td>$' + data[i].precio + '</td>';
            }
            /** De lo contrario ponemos $0**/
            else {
                formulario += '<td>' + '-' + '</td>';
            }
            formulario += '<td>';
            formulario += '<div class="row">';
            formulario += '<div class="col-xs-5">';

            /**
             * Si el producto tiene precio, creamos un campo para ingresar la cantidad y llamamos a la función que cambia la etiqueta del total
             * */
            if (data[i].precio) {
                formulario += '<input type="text" class="form-control" name="cantidad[]"' +
                    ' min ="0" placeholder="cantidad..."' +
                    ' id="' + campoCantidadId + '"' +
                    'Onkeyup=cambiarTotal("' + campoCantidadId + '","' + labelTotalId + 'H","' + labelTotalId + '",' + data[i].precio + ',"totalProducto") required>';
            }
            /** Si no se tiene precio ponemos un campo deshabilitado**/
            else {
                formulario += '<input type="text" disabled>';
            }
            formulario += '</div>';
            formulario += '</div>';
            formulario += '</td>';
            formulario += '<td>';
            /** Etiqueta donde se pondrá el total(precio*cantidad) de cada producto **/
            formulario += '<label class="text-bold" id="' + labelTotalId + '">-</label>';
            /****/
            formulario += '<input type="hidden" id="' + labelTotalId + 'H' + '" class="totalProducto" value="0">';
            formulario += '</td>';
            formulario += '</tr>';
        }
        formulario += '<tr>';
        formulario += '<td colspan="5" class="text-right text-bold"> Total: </td>';
        formulario += '<td>';
        /** Etiqueta donde se pondrá el total(precio*cantidad) de cada producto **/
        formulario += '<label class="text-bold" id="totalProducto">$ 0</label>';

        formulario += '</td>';
        formulario += '</tr>';
        formulario += '</tbody>';
        formulario += '</table>';

        /** Cambiamos el valor del div con el id formularioPrecios por el formulario que hemos creado**/
        $('#' + divlId).html(formulario);

        /**
         * Comprobamos si todos los productos del proveedor no tienen precio
         * en tal caso, deshabilitamos el botón para guardar
         * */
        if(!controlBoton){
            $('#boton-guardar').prop( "disabled", true );
        }
        /**
         * Si algún producto del proveedor tiene precio, habilitamos el botón de guardar
         * */
        else {
            $('#boton-guardar').prop( "disabled", false );
        }
    });
}