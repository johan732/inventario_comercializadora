/**
 * Variable que lleva el número de elementos agregados por el método Crear campo
 */
var contador= 0;

function crearCampos(productoId,unidadMedida) {

    /**
     * Cuando se invoca al método se aumenta la variable para indicar que se ha ingresado un elemento más
     */
    ++contador;

    var divCantidadLote = "divCantidadLote" + productoId;
    var divCertificado = "divCertificado" + productoId;

    formularioCertificado = '<hr>';
    formularioCertificado += '<label class="btn btn-default btn-file" id="div'+productoId+contador+'">';
    formularioCertificado +='<span class="glyphicon glyphicon-open" aria-hidden="true"></span>';
    formularioCertificado += '<input type="file" name="certificado[]" id="'+productoId+contador+'" style="display: none;">';
    formularioCertificado += '<span class="file-control" id="certificado'+productoId+contador+'">Certificado</span>';
    formularioCertificado += '</label>';

    formulario = '<hr>';
    <!-- Campo oculto para enviar el id del producto con sus respectivos campos en el mismo indice -->
    formulario += '<input type="hidden" name="compras_ordenes_productos_id[]" value="' + productoId + '" required>';
    formulario += '<div class="row">';
    formulario += '<div class="col-xs-4">';
    formulario += '<input type="number"  class="form-control cantidad'+productoId+'" name="cantidad[]" placeholder="Cantidad...">';
    formulario += '</div>';
    formulario += '<div class="col-xs-1 text-center"> '+unidadMedida+' </div>';
    formulario += '<div class="col-xs-1 text-center"> - </div>';
    formulario += '<div class="col-xs-5">';
    formulario += '<input type="text"  class="form-control" name="lote[]" placeholder="Lote...">';
    formulario += '</div>';
    formulario += '</div>';

    /** Cambiamos el valor del div con el id formularioPrecios por el formulario que hemos creado**/
    $('#' + divCantidadLote).append(formulario);
    $('#' + divCertificado).append(formularioCertificado);
}
