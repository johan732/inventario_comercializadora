<!-- Script para la generación de selects dependientes -->
//    Este script actualiza los valores de otro select, es necesario crear una ruta y retornar en una colección los datos
//    que se cargarán en la siguiente lista, para este caso se creó la ruta "municipios/{id}" que retorna una lista de valores
//    dependiendo del id que se le pase-->

// Función para cargar los elementos en la nueva lista
//('el select que cambia','el select donde vamos a poner los datos','la ruta de donde se van a tomar los datos')

function onSelectDependiente(idSelectPadre, idSelectHijo, ruta) {

    var selectPadre = document.getElementById(idSelectPadre).value;

    // si no se ha seleccionado nada, entonces dejamos la lista vacia
    if (!selectPadre) {
        $('#' + idSelectHijo).html('<option>seleccione una opciónx</option>');
        return;
    }

    //console.log("RUTA = "+ruta+selectPadre);
    //Función AJAX que cambia los valores de la lista dinamicamente
    $.get(ruta + selectPadre, function (data) {
        //variable que contiene el html que se pondrá en la nueva lista
        var html_select = '<option>seleccione una opción</option>';

        for (var i = 0; i < data.length; i++) {
            html_select += '<option value ="' + data[i].id + '">' + data[i].nombre + '</option>';
        }

        // cambiamos el html de la nueva lista
//                document.getElementById(idSelectHijo).innerHTML = html_select;
        $('#' + idSelectHijo).html(html_select);
        // actualizamos el plugin de chosen para que se tomen los valores con la clase chosen
        $('#' + idSelectHijo).trigger("chosen:updated");
    });
}