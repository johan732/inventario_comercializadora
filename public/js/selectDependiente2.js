<!-- Script para la generación de selects dependientes -->
//    Este script actualiza los valores de otro select, es necesario crear una ruta y retornar en una colección los datos
//    que se cargarán en la siguiente lista

// Función para cargar los elementos en la nueva lista
//('valores que se pasaran  a la ruta que trae la colección','el select donde vamos a poner los datos','la ruta de donde se van a tomar los datos')

function onSelectDependiente2(values, idSelectHijo, ruta) {

    // si no se ha seleccionado nada, entonces dejamos la lista vacia
    if (!values) {
        $('#' + idSelectHijo).html('<option>seleccione una opciónx</option>');
        return;
    }

    //console.log("RUTA = "+ruta+selectPadre);
    //Función AJAX que cambia los valores de la lista dinamicamente
    $.get(ruta + values, function (data) {
        //variable que contiene el html que se pondrá en la nueva lista
        var html_select = '<option>seleccione una opción</option>';

        for (var i = 0; i < data.length; i++) {
            html_select += '<option value ="' + data[i].id + '">' + data[i].nombre + '</option>';
        }

        // cambiamos el html de la nueva lista
//                document.getElementById(idSelectHijo).innerHTML = html_select;
        $('#' + idSelectHijo).html(html_select);
        // actualizamos el plugin de chosen para que se tomen los valores con la clase chosen
        $('#' + idSelectHijo).trigger("chosen:updated");
    });
}