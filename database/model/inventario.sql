-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-09-2018 a las 19:53:26
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_productos`
--

CREATE TABLE `compras_productos` (
  `id` int(11) NOT NULL,
  `compras_tipo_producto_id` int(11) NOT NULL,
  `general_unidad_medida_id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `eliminado` int(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras_productos`
--

INSERT INTO `compras_productos` (`id`, `compras_tipo_producto_id`, `general_unidad_medida_id`, `nombre`, `eliminado`, `created_at`, `updated_at`) VALUES
(1, 3, 3, 'Azucar', NULL, '2018-02-22 12:13:38', '2018-09-25 15:58:24'),
(2, 3, 3, 'Leche en polvo', NULL, '2018-02-22 12:14:35', '2018-02-22 12:14:35'),
(3, 5, 6, 'Botella 1000', NULL, '2018-02-22 12:15:15', '2018-05-03 14:19:45'),
(4, 3, 3, 'Glucosa', NULL, '2018-03-21 16:15:20', '2018-03-21 16:15:20'),
(5, 3, 1, 'La sustancia X', NULL, '2018-03-22 12:33:57', '2018-03-22 12:33:57'),
(6, 3, 1, 'El elemento X', NULL, '2018-09-25 15:58:56', '2018-09-25 15:59:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_tipo_producto`
--

CREATE TABLE `compras_tipo_producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `eliminado` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras_tipo_producto`
--

INSERT INTO `compras_tipo_producto` (`id`, `nombre`, `eliminado`, `created_at`, `updated_at`) VALUES
(1, 'Dotación e implementos de seguridad', NULL, NULL, '2018-09-25 14:44:19'),
(2, 'Maquinaria', NULL, NULL, NULL),
(3, 'Materia prima', NULL, NULL, NULL),
(4, 'Insumos', NULL, NULL, NULL),
(5, 'Envases', NULL, '2018-07-13 05:00:00', '2018-07-13 05:00:00'),
(6, 'Productos químicos', NULL, NULL, NULL),
(7, 'Papeleria', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `indicativo` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nombre`, `indicativo`, `created_at`, `updated_at`) VALUES
(1, 'AMAZONAS', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(2, 'ANTIOQUIA', 4, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(3, 'ARAUCA', 7, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(4, 'ATLÁNTICO', 5, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(5, 'BOLÍVAR', 5, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(6, 'BOYACÁ', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(7, 'CALDAS', 6, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(8, 'CAQUETÁ', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(9, 'CASANARE', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(10, 'CAUCA', 2, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(11, 'CESAR', 5, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(12, 'CHOCÓ', 4, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(13, 'CÓRDOBA', 4, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(14, 'CUNDINAMARCA', 1, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(15, 'GUAINÍA', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(16, 'GUAVIARE', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(17, 'HUILA', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(18, 'LA GUAJIRA', 5, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(19, 'MAGDALENA', 5, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(20, 'META', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(21, 'NARIÑO', 2, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(22, 'NORTE DE SANTANDER', 7, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(23, 'PUTUMAYO', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(24, 'QUINDÍO', 6, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(25, 'RISARALDA', 6, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(26, 'SAN ANDRÉS Y ROVIDENCIA', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(27, 'SANTANDER', 7, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(28, 'SUCRE', 5, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(29, 'TOLIMA', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(30, 'VALLE DEL CAUCA', 2, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(31, 'VAUPÉS', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27'),
(32, 'VICHADA', 8, '2017-08-03 18:31:27', '2017-08-03 18:31:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `municipios_id` int(20) NOT NULL,
  `nit` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `logo` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `celular` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `municipios_id`, `nit`, `nombre`, `direccion`, `logo`, `telefono`, `celular`, `website`, `created_at`, `updated_at`) VALUES
(1, 1096, '123456789', 'KEVIN EL PAISA PUES', 'Carrera 26 27-109', 'logo_1538053812.jpg', '12345600', '1234567891', 'site.com', '2017-07-27 21:32:38', '2018-09-27 13:11:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general_unidad_medida`
--

CREATE TABLE `general_unidad_medida` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `abreviatura` varchar(20) DEFAULT NULL,
  `eliminado` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `general_unidad_medida`
--

INSERT INTO `general_unidad_medida` (`id`, `nombre`, `abreviatura`, `eliminado`) VALUES
(1, 'Litros', 'l', NULL),
(2, 'Mililitros', 'ml', NULL),
(3, 'Kilogramos', 'kg', NULL),
(4, 'Gramos', 'g', NULL),
(5, 'Miligramos', 'mg', NULL),
(6, 'Unidad', 'u', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `compras_tipo_producto_id` int(11) NOT NULL,
  `compras_productos_id` int(11) NOT NULL,
  `inventario_bodegas_id` int(11) NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `roles_id` int(10) NOT NULL,
  `precio` float DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `precio_promedio` float DEFAULT NULL,
  `observaciones` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_bodegas`
--

CREATE TABLE `inventario_bodegas` (
  `id` int(11) NOT NULL,
  `inventario_tipo_almacenamiento_id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL COMMENT '(null) Habilitado\n(1) Deshabilitado',
  `habilitado` int(11) DEFAULT NULL COMMENT '(null) Habilitado\n(1) Deshabilitado',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_bodegas`
--

INSERT INTO `inventario_bodegas` (`id`, `inventario_tipo_almacenamiento_id`, `nombre`, `habilitado`, `created_at`, `updated_at`) VALUES
(3, 1, 'Bogotá', NULL, '2018-05-08 17:46:32', '2018-05-15 12:57:06'),
(4, 1, 'Pasto', NULL, '2018-05-08 17:46:43', '2018-05-10 13:34:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_configuracion_productos`
--

CREATE TABLE `inventario_configuracion_productos` (
  `id` int(11) NOT NULL,
  `compras_productos_id` int(11) NOT NULL,
  `unidadesxcaja` int(11) DEFAULT '0',
  `stock_minimo` float DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_configuracion_productos`
--

INSERT INTO `inventario_configuracion_productos` (`id`, `compras_productos_id`, `unidadesxcaja`, `stock_minimo`, `created_at`, `updated_at`) VALUES
(1, 3, 10, 4, '2018-05-03 13:51:53', '2018-05-03 15:19:59'),
(2, 1, 50, 50, '2018-05-03 14:22:26', '2018-05-03 14:23:17'),
(3, 2, 0, 60, '2018-07-09 15:52:25', '2018-07-09 15:55:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_motivos_movimiento`
--

CREATE TABLE `inventario_motivos_movimiento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `habilitado` int(11) DEFAULT NULL COMMENT '(null) Habilitado\n(1) Deshabilitado',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_motivos_movimiento`
--

INSERT INTO `inventario_motivos_movimiento` (`id`, `nombre`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 'Devolución', NULL, '2018-05-15 12:44:55', '2018-09-25 16:01:10'),
(2, 'Ajuste', NULL, '2018-05-15 16:18:05', '2018-05-15 16:18:26'),
(3, 'Compra', NULL, '2018-09-25 16:00:52', '2018-09-25 16:00:52'),
(4, 'Venta', NULL, '2018-09-25 16:00:56', '2018-09-25 16:00:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_movimientos`
--

CREATE TABLE `inventario_movimientos` (
  `id` int(11) NOT NULL,
  `inventario_id` int(11) NOT NULL,
  `compras_tipo_producto_id` int(11) NOT NULL,
  `compras_productos_id` int(11) NOT NULL,
  `inventario_bodegas_id` int(11) NOT NULL COMMENT 'En caso de que sea un traslado (tipo_movimiento = 3) esta será la bodega origen',
  `inventario_bodegas_id_destino` int(11) DEFAULT NULL COMMENT 'Llave foranea de una bodega destino para cuando se realizan traslados',
  `inventario_motivos_movimiento_id` int(11) NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `roles_id` int(10) NOT NULL,
  `tipo_movimiento` int(11) DEFAULT NULL COMMENT 'Tipo de movimiento:\n1. Entrada\n2. Salida\n3. Traslado',
  `precio` float DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `precio_promedio` float DEFAULT NULL,
  `observaciones` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_tipo_almacenamiento`
--

CREATE TABLE `inventario_tipo_almacenamiento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `habilitado` int(11) DEFAULT NULL COMMENT '(null) Habilitado\n(1) Deshabilitado',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_tipo_almacenamiento`
--

INSERT INTO `inventario_tipo_almacenamiento` (`id`, `nombre`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 'Estivas', NULL, '2018-05-03 16:44:23', '2018-05-03 16:58:04'),
(2, 'Rack', NULL, '2018-06-28 12:39:35', '2018-06-28 12:39:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `as` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'guarda el nombre de la ruta de la forma: ruta.index; esto con el fin de comprobar permisos en el middleware',
  `orden` int(11) NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa fa-angle-right',
  `id_padre` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `nombre`, `src`, `as`, `orden`, `icon`, `id_padre`, `created_at`, `updated_at`) VALUES
(0, 'SIN PADRE', '#', NULL, 0, '', -1, NULL, NULL),
(1, 'Configuración', '#', NULL, 20, 'fa fa-cogs', 0, '2017-01-13 00:00:47', '2018-03-08 12:20:46'),
(2, 'Sistema', '#', NULL, 1, 'fa fa-desktop', 1, '2017-01-13 00:28:25', '2017-08-25 22:52:23'),
(3, 'Menú', 'menu', 'menu.index', 1, 'fa fa-angle-right', 2, '2017-01-13 01:26:47', '2017-01-19 00:01:17'),
(5, 'Roles', 'rol', 'rol.index', 1, 'fa fa-angle-right', 10, '2017-01-13 02:13:55', '2017-01-19 00:08:16'),
(10, 'Usuarios', '#', NULL, 2, 'fa fa-user', 1, '2017-01-14 02:09:04', '2017-08-25 22:54:02'),
(11, 'Usuario', 'user', 'user.index ', 2, 'fa fa-angle-right', 10, '2017-01-14 02:23:02', '2017-01-19 00:00:49'),
(12, 'Permisos', 'permiso', NULL, 3, 'fa fa-angle-right', 10, '2017-01-18 23:58:39', '2017-01-19 00:48:33'),
(13, 'Organización', 'empresa', NULL, 3, 'fas fa-building', 1, '2017-07-19 00:15:58', '2018-04-04 12:47:00'),
(16, 'Productos', 'compras/configuracion/productos', '', 1, 'fa fa-angle-right', 224, '2017-08-02 02:59:06', '2018-09-25 16:04:25'),
(206, 'Inventario', '#', '', 3, 'fa fa-archive', 0, '2018-03-08 12:20:21', '2018-09-25 13:39:14'),
(208, 'Movimientos', 'inventario/movimientos', 'inventario.movimientos', 2, 'fas fa-exchange-alt', 206, '2018-04-03 12:51:02', '2018-06-28 18:56:23'),
(212, 'Reportes', '#', NULL, 3, 'fa fas fa-file-alt', 206, '2018-04-04 13:15:41', '2018-04-04 13:26:21'),
(213, 'Inventario', 'inventario/reporte/inventario', 'inventario.reportes.inventario.index', 1, 'fa fa-angle-right', 212, '2018-04-04 13:29:04', '2018-04-04 13:29:04'),
(214, 'Movimientos', 'inventario/reporte/movimientos', 'inventario.reportes.movimientos.index', 2, 'fa fa-angle-right', 212, '2018-04-04 13:29:41', '2018-04-04 13:29:41'),
(215, 'Configuración', '#', NULL, 4, 'fa fa-cog', 206, '2018-04-04 13:30:55', '2018-04-04 13:31:37'),
(216, 'Bodegas', 'inventario/configuracion/bodegas', 'inventario.configuracion.bodegas.index', 2, 'fa fa-angle-right', 215, '2018-04-04 13:32:07', '2018-05-03 15:42:57'),
(217, 'Productos', 'inventario/configuracion/productos', 'inventario.configuracion.productos.index', 0, 'fa fa-angle-right', 215, '2018-04-27 14:21:15', '2018-09-26 19:03:46'),
(218, 'Motivos', 'inventario/configuracion/motivos', 'inventario.configuracion.motivos.index', 3, 'fa fa-angle-right', 215, '2018-04-27 14:21:55', '2018-05-03 15:43:26'),
(219, 'Almacenamiento', 'inventario/configuracion/almacenamiento', 'inventario.configuracion.almacenamiento.index', 1, 'fa fa-angle-right', 215, '2018-04-27 14:26:07', '2018-05-03 16:04:43'),
(223, 'Tipo', 'compras/configuracion/productos/tipo', 'compras.configuracion.productos.tipo.index', 1, 'fa fa-angle-right', 224, '2018-09-25 14:39:19', '2018-09-25 16:05:12'),
(224, 'Producto', '#', 'producto.conf', 5, 'fa fa-angle-right', 215, '2018-09-25 16:02:55', '2018-09-25 16:04:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` int(20) NOT NULL,
  `departamentos_id` int(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id`, `departamentos_id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 1, 'EL ENCANTO', '2017-08-03 18:32:08', '2017-08-03 18:32:28'),
(2, 1, 'LA CHORRERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(3, 1, 'LA PEDRERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(4, 1, 'LA VICTORIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(5, 1, 'LETICIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(6, 1, 'MIRITI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(7, 1, 'PUERTO ALEGRIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(8, 1, 'PUERTO ARICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(9, 1, 'PUERTO NARIÑO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(10, 1, 'PUERTO SANTANDER', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(11, 1, 'TURAPACA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(12, 2, 'ABEJORRAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(13, 2, 'ABRIAQUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(14, 2, 'ALEJANDRIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(15, 2, 'AMAGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(16, 2, 'AMALFI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(17, 2, 'ANDES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(18, 2, 'ANGELOPOLIS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(19, 2, 'ANGOSTURA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(20, 2, 'ANORI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(21, 2, 'ANTIOQUIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(22, 2, 'ANZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(23, 2, 'APARTADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(24, 2, 'ARBOLETES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(25, 2, 'ARGELIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(26, 2, 'ARMENIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(27, 2, 'BARBOSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(28, 2, 'BELLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(29, 2, 'BELMIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(30, 2, 'BETANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(31, 2, 'BETULIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(32, 2, 'BOLIVAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(33, 2, 'BRICEÑO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(34, 2, 'BURITICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(35, 2, 'CACERES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(36, 2, 'CAICEDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(37, 2, 'CALDAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(38, 2, 'CAMPAMENTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(39, 2, 'CANASGORDAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(40, 2, 'CARACOLI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(41, 2, 'CARAMANTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(42, 2, 'CAREPA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(43, 2, 'CARMEN DE VIBORAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(44, 2, 'CAROLINA DEL PRINCIPE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(45, 2, 'CAUCASIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(46, 2, 'CHIGORODO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(47, 2, 'CISNEROS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(48, 2, 'COCORNA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(49, 2, 'CONCEPCION', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(50, 2, 'CONCORDIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(51, 2, 'COPACABANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(52, 2, 'DABEIBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(53, 2, 'DONMATIAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(54, 2, 'EBEJICO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(55, 2, 'EL BAGRE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(56, 2, 'EL PENOL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(57, 2, 'EL RETIRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(58, 2, 'ENTRERRIOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(59, 2, 'ENVIGADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(60, 2, 'FREDONIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(61, 2, 'FRONTINO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(62, 2, 'GIRALDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(63, 2, 'GIRARDOTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(64, 2, 'GOMEZ PLATA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(65, 2, 'GRANADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(66, 2, 'GUADALUPE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(67, 2, 'GUARNE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(68, 2, 'GUATAQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(69, 2, 'HELICONIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(70, 2, 'HISPANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(71, 2, 'ITAGUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(72, 2, 'ITUANGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(73, 2, 'JARDIN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(74, 2, 'JERICO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(75, 2, 'LA CEJA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(76, 2, 'LA ESTRELLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(77, 2, 'LA PINTADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(78, 2, 'LA UNION', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(79, 2, 'LIBORINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(80, 2, 'MACEO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(81, 2, 'MARINILLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(82, 2, 'MEDELLIN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(83, 2, 'MONTEBELLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(84, 2, 'MURINDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(85, 2, 'MUTATA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(86, 2, 'NARINO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(87, 2, 'NECHI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(88, 2, 'NECOCLI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(89, 2, 'OLAYA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(90, 2, 'PEQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(91, 2, 'PUEBLORRICO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(92, 2, 'PUERTO BERRIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(93, 2, 'PUERTO NARE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(94, 2, 'PUERTO TRIUNFO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(95, 2, 'REMEDIOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(96, 2, 'RIONEGRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(97, 2, 'SABANALARGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(98, 2, 'SABANETA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(99, 2, 'SALGAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(100, 2, 'SAN ANDRES DE CUERQUIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(101, 2, 'SAN CARLOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(102, 2, 'SAN FRANCISCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(103, 2, 'SAN JERONIMO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(104, 2, 'SAN JOSE DE LA MONTAÑA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(105, 2, 'SAN JUAN DE URABA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(106, 2, 'SAN LUIS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(107, 2, 'SAN PEDRO DE LOS MILAGROS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(108, 2, 'SAN PEDRO DE URABA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(109, 2, 'SAN RAFAEL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(110, 2, 'SAN ROQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(111, 2, 'SAN VICENTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(112, 2, 'SANTA BARBARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(113, 2, 'SANTA ROSA DE OSOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(114, 2, 'SANTO DOMINGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(115, 2, 'SANTUARIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(116, 2, 'SEGOVIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(117, 2, 'SONSON', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(118, 2, 'SOPETRAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(119, 2, 'TAMESIS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(120, 2, 'TARAZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(121, 2, 'TARSO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(122, 2, 'TITIRIBI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(123, 2, 'TOLEDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(124, 2, 'TURBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(125, 2, 'URAMITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(126, 2, 'URRAO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(127, 2, 'VALDIVIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(128, 2, 'VALPARAISO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(129, 2, 'VEGACHI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(130, 2, 'VENECIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(131, 2, 'VIGIA DEL FUERTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(132, 2, 'YALI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(133, 2, 'YARUMAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(134, 2, 'YOLOMBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(135, 2, 'YONDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(136, 2, 'ZARAGOZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(137, 3, 'ARAUCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(138, 3, 'ARAUQUITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(139, 3, 'CRAVO NORTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(140, 3, 'FORTUL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(141, 3, 'PUERTO RONDON', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(142, 3, 'SARAVENA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(143, 3, 'TAME', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(144, 4, 'BARANOA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(145, 4, 'BARRANQUILLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(146, 4, 'CAMPO DE LA CRUZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(147, 4, 'CANDELARIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(148, 4, 'GALAPA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(149, 4, 'JUAN DE ACOSTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(150, 4, 'LURUACO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(151, 4, 'MALAMBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(152, 4, 'MANATI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(153, 4, 'PALMAR DE VARELA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(154, 4, 'PIOJO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(155, 4, 'POLO NUEVO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(156, 4, 'PONEDERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(157, 4, 'PUERTO COLOMBIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(158, 4, 'REPELON', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(159, 4, 'SABANAGRANDE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(160, 4, 'SABANALARGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(161, 4, 'SANTA LUCIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(162, 4, 'SANTO TOMAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(163, 4, 'SOLEDAD', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(164, 4, 'SUAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(165, 4, 'TUBARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(166, 4, 'USIACURI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(167, 5, 'ACHI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(168, 5, 'ALTOS DEL ROSARIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(169, 5, 'ARENAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(170, 5, 'ARJONA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(171, 5, 'ARROYOHONDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(172, 5, 'BARRANCO DE LOBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(173, 5, 'BRAZUELO DE PAPAYAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(174, 5, 'CALAMAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(175, 5, 'CANTAGALLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(176, 5, 'CARTAGENA DE INDIAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(177, 5, 'CICUCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(178, 5, 'CLEMENCIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(179, 5, 'CORDOBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(180, 5, 'EL CARMEN DE BOLIVAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(181, 5, 'EL GUAMO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(182, 5, 'EL PENION', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(183, 5, 'HATILLO DE LOBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(184, 5, 'MAGANGUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(185, 5, 'MAHATES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(186, 5, 'MARGARITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(187, 5, 'MARIA LA BAJA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(188, 5, 'MONTECRISTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(189, 5, 'MORALES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(190, 5, 'MORALES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(191, 5, 'NOROSI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(192, 5, 'PINILLOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(193, 5, 'REGIDOR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(194, 5, 'RIO VIEJO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(195, 5, 'SAN CRISTOBAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(196, 5, 'SAN ESTANISLAO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(197, 5, 'SAN FERNANDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(198, 5, 'SAN JACINTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(199, 5, 'SAN JACINTO DEL CAUCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(200, 5, 'SAN JUAN DE NEPOMUCENO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(201, 5, 'SAN MARTIN DE LOBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(202, 5, 'SAN PABLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(203, 5, 'SAN PABLO NORTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(204, 5, 'SANTA CATALINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(205, 5, 'SANTA CRUZ DE MOMPOX', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(206, 5, 'SANTA ROSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(207, 5, 'SANTA ROSA DEL SUR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(208, 5, 'SIMITI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(209, 5, 'SOPLAVIENTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(210, 5, 'TALAIGUA NUEVO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(211, 5, 'TUQUISIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(212, 5, 'TURBACO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(213, 5, 'TURBANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(214, 5, 'VILLANUEVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(215, 5, 'ZAMBRANO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(216, 6, 'AQUITANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(217, 6, 'ARCABUCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(218, 6, 'BELÉN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(219, 6, 'BERBEO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(220, 6, 'BETÉITIVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(221, 6, 'BOAVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(222, 6, 'BOYACÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(223, 6, 'BRICEÑO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(224, 6, 'BUENAVISTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(225, 6, 'BUSBANZÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(226, 6, 'CALDAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(227, 6, 'CAMPO HERMOSO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(228, 6, 'CERINZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(229, 6, 'CHINAVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(230, 6, 'CHIQUINQUIRÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(231, 6, 'CHÍQUIZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(232, 6, 'CHISCAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(233, 6, 'CHITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(234, 6, 'CHITARAQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(235, 6, 'CHIVATÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(236, 6, 'CIÉNEGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(237, 6, 'CÓMBITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(238, 6, 'COPER', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(239, 6, 'CORRALES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(240, 6, 'COVARACHÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(241, 6, 'CUBARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(242, 6, 'CUCAITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(243, 6, 'CUITIVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(244, 6, 'DUITAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(245, 6, 'EL COCUY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(246, 6, 'EL ESPINO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(247, 6, 'FIRAVITOBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(248, 6, 'FLORESTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(249, 6, 'GACHANTIVÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(250, 6, 'GÁMEZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(251, 6, 'GARAGOA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(252, 6, 'GUACAMAYAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(253, 6, 'GÜICÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(254, 6, 'IZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(255, 6, 'JENESANO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(256, 6, 'JERICÓ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(257, 6, 'LA UVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(258, 6, 'LA VICTORIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(259, 6, 'LABRANZA GRANDE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(260, 6, 'MACANAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(261, 6, 'MARIPÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(262, 6, 'MIRAFLORES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(263, 6, 'MONGUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(264, 6, 'MONGUÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(265, 6, 'MONIQUIRÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(266, 6, 'MOTAVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(267, 6, 'MUZO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(268, 6, 'NOBSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(269, 6, 'NUEVO COLÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(270, 6, 'OICATÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(271, 6, 'OTANCHE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(272, 6, 'PACHAVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(273, 6, 'PÁEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(274, 6, 'PAIPA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(275, 6, 'PAJARITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(276, 6, 'PANQUEBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(277, 6, 'PAUNA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(278, 6, 'PAYA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(279, 6, 'PAZ DE RÍO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(280, 6, 'PESCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(281, 6, 'PISBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(282, 6, 'PUERTO BOYACA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(283, 6, 'QUÍPAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(284, 6, 'RAMIRIQUÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(285, 6, 'RÁQUIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(286, 6, 'RONDÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(287, 6, 'SABOYÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(288, 6, 'SÁCHICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(289, 6, 'SAMACÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(290, 6, 'SAN EDUARDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(291, 6, 'SAN JOSÉ DE PARE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(292, 6, 'SAN LUÍS DE GACENO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(293, 6, 'SAN MATEO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(294, 6, 'SAN MIGUEL DE SEMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(295, 6, 'SAN PABLO DE BORBUR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(296, 6, 'SANTA MARÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(297, 6, 'SANTA ROSA DE VITERBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(298, 6, 'SANTA SOFÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(299, 6, 'SANTANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(300, 6, 'SATIVANORTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(301, 6, 'SATIVASUR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(302, 6, 'SIACHOQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(303, 6, 'SOATÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(304, 6, 'SOCHA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(305, 6, 'SOCOTÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(306, 6, 'SOGAMOSO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(307, 6, 'SORA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(308, 6, 'SORACÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(309, 6, 'SOTAQUIRÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(310, 6, 'SUSACÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(311, 6, 'SUTARMACHÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(312, 6, 'TASCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(313, 6, 'TIBANÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(314, 6, 'TIBASOSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(315, 6, 'TINJACÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(316, 6, 'TIPACOQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(317, 6, 'TOCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(318, 6, 'TOGÜÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(319, 6, 'TÓPAGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(320, 6, 'TOTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(321, 6, 'TUNJA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(322, 6, 'TUNUNGUÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(323, 6, 'TURMEQUÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(324, 6, 'TUTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(325, 6, 'TUTAZÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(326, 6, 'UMBITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(327, 6, 'VENTA QUEMADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(328, 6, 'VILLA DE LEYVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(329, 6, 'VIRACACHÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(330, 6, 'ZETAQUIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(331, 7, 'AGUADAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(332, 7, 'ANSERMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(333, 7, 'ARANZAZU', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(334, 7, 'BELALCAZAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(335, 7, 'CHINCHINÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(336, 7, 'FILADELFIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(337, 7, 'LA DORADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(338, 7, 'LA MERCED', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(339, 7, 'MANIZALES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(340, 7, 'MANZANARES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(341, 7, 'MARMATO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(342, 7, 'MARQUETALIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(343, 7, 'MARULANDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(344, 7, 'NEIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(345, 7, 'NORCASIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(346, 7, 'PACORA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(347, 7, 'PALESTINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(348, 7, 'PENSILVANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(349, 7, 'RIOSUCIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(350, 7, 'RISARALDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(351, 7, 'SALAMINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(352, 7, 'SAMANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(353, 7, 'SAN JOSE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(354, 7, 'SUPÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(355, 7, 'VICTORIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(356, 7, 'VILLAMARÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(357, 7, 'VITERBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(358, 8, 'ALBANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(359, 8, 'BELÉN ANDAQUIES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(360, 8, 'CARTAGENA DEL CHAIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(361, 8, 'CURILLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(362, 8, 'EL DONCELLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(363, 8, 'EL PAUJIL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(364, 8, 'FLORENCIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(365, 8, 'LA MONTAÑITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(366, 8, 'MILÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(367, 8, 'MORELIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(368, 8, 'PUERTO RICO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(369, 8, 'SAN  VICENTE DEL CAGUAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(370, 8, 'SAN JOSÉ DE FRAGUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(371, 8, 'SOLANO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(372, 8, 'SOLITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(373, 8, 'VALPARAÍSO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(374, 9, 'AGUAZUL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(375, 9, 'CHAMEZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(376, 9, 'HATO COROZAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(377, 9, 'LA SALINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(378, 9, 'MANÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(379, 9, 'MONTERREY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(380, 9, 'NUNCHIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(381, 9, 'OROCUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(382, 9, 'PAZ DE ARIPORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(383, 9, 'PORE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(384, 9, 'RECETOR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(385, 9, 'SABANA LARGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(386, 9, 'SACAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(387, 9, 'SAN LUIS DE PALENQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(388, 9, 'TAMARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(389, 9, 'TAURAMENA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(390, 9, 'TRINIDAD', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(391, 9, 'VILLANUEVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(392, 9, 'YOPAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(393, 10, 'ALMAGUER', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(394, 10, 'ARGELIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(395, 10, 'BALBOA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(396, 10, 'BOLÍVAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(397, 10, 'BUENOS AIRES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(398, 10, 'CAJIBIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(399, 10, 'CALDONO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(400, 10, 'CALOTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(401, 10, 'CORINTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(402, 10, 'EL TAMBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(403, 10, 'FLORENCIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(404, 10, 'GUAPI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(405, 10, 'INZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(406, 10, 'JAMBALÓ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(407, 10, 'LA SIERRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(408, 10, 'LA VEGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(409, 10, 'LÓPEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(410, 10, 'MERCADERES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(411, 10, 'MIRANDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(412, 10, 'MORALES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(413, 10, 'PADILLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(414, 10, 'PÁEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(415, 10, 'PATIA (EL BORDO)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(416, 10, 'PIAMONTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(417, 10, 'PIENDAMO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(418, 10, 'POPAYÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(419, 10, 'PUERTO TEJADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(420, 10, 'PURACE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(421, 10, 'ROSAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(422, 10, 'SAN SEBASTIÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(423, 10, 'SANTA ROSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(424, 10, 'SANTANDER DE QUILICHAO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(425, 10, 'SILVIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(426, 10, 'SOTARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(427, 10, 'SUÁREZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(428, 10, 'SUCRE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(429, 10, 'TIMBÍO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(430, 10, 'TIMBIQUÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(431, 10, 'TORIBIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(432, 10, 'TOTORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(433, 10, 'VILLA RICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(434, 11, 'AGUACHICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(435, 11, 'AGUSTÍN CODAZZI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(436, 11, 'ASTREA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(437, 11, 'BECERRIL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(438, 11, 'BOSCONIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(439, 11, 'CHIMICHAGUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(440, 11, 'CHIRIGUANÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(441, 11, 'CURUMANÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(442, 11, 'EL COPEY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(443, 11, 'EL PASO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(444, 11, 'GAMARRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(445, 11, 'GONZÁLEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(446, 11, 'LA GLORIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(447, 11, 'LA JAGUA IBIRICO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(448, 11, 'MANAURE BALCÓN DEL CESAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(449, 11, 'PAILITAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(450, 11, 'PELAYA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(451, 11, 'PUEBLO BELLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(452, 11, 'RÍO DE ORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(453, 11, 'ROBLES (LA PAZ)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(454, 11, 'SAN ALBERTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(455, 11, 'SAN DIEGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(456, 11, 'SAN MARTÍN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(457, 11, 'TAMALAMEQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(458, 11, 'VALLEDUPAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(459, 12, 'ACANDI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(460, 12, 'ALTO BAUDO (PIE DE PATO)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(461, 12, 'ATRATO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(462, 12, 'BAGADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(463, 12, 'BAHIA SOLANO (MUTIS)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(464, 12, 'BAJO BAUDO (PIZARRO)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(465, 12, 'BOJAYA (BELLAVISTA)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(466, 12, 'CANTON DE SAN PABLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(467, 12, 'CARMEN DEL DARIEN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(468, 12, 'CERTEGUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(469, 12, 'CONDOTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(470, 12, 'EL CARMEN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(471, 12, 'ISTMINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(472, 12, 'JURADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(473, 12, 'LITORAL DEL SAN JUAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(474, 12, 'LLORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(475, 12, 'MEDIO ATRATO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(476, 12, 'MEDIO BAUDO (BOCA DE PEPE)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(477, 12, 'MEDIO SAN JUAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(478, 12, 'NOVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(479, 12, 'NUQUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(480, 12, 'QUIBDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(481, 12, 'RIO IRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(482, 12, 'RIO QUITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(483, 12, 'RIOSUCIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(484, 12, 'SAN JOSE DEL PALMAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(485, 12, 'SIPI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(486, 12, 'TADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(487, 12, 'UNGUIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(488, 12, 'UNIÓN PANAMERICANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(489, 13, 'AYAPEL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(490, 13, 'BUENAVISTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(491, 13, 'CANALETE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(492, 13, 'CERETÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(493, 13, 'CHIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(494, 13, 'CHINÚ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(495, 13, 'CIENAGA DE ORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(496, 13, 'COTORRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(497, 13, 'LA APARTADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(498, 13, 'LORICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(499, 13, 'LOS CÓRDOBAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(500, 13, 'MOMIL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(501, 13, 'MONTELÍBANO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(502, 13, 'MONTERÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(503, 13, 'MOÑITOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(504, 13, 'PLANETA RICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(505, 13, 'PUEBLO NUEVO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(506, 13, 'PUERTO ESCONDIDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(507, 13, 'PUERTO LIBERTADOR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(508, 13, 'PURÍSIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(509, 13, 'SAHAGÚN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(510, 13, 'SAN ANDRÉS SOTAVENTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(511, 13, 'SAN ANTERO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(512, 13, 'SAN BERNARDO VIENTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(513, 13, 'SAN CARLOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(514, 13, 'SAN PELAYO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(515, 13, 'TIERRALTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(516, 13, 'VALENCIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(517, 14, 'AGUA DE DIOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(518, 14, 'ALBAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(519, 14, 'ANAPOIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(520, 14, 'ANOLAIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(521, 14, 'ARBELAEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(522, 14, 'BELTRÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(523, 14, 'BITUIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(524, 14, 'BOGOTÁ DC', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(525, 14, 'BOJACÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(526, 14, 'CABRERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(527, 14, 'CACHIPAY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(528, 14, 'CAJICÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(529, 14, 'CAPARRAPÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(530, 14, 'CAQUEZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(531, 14, 'CARMEN DE CARUPA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(532, 14, 'CHAGUANÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(533, 14, 'CHIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(534, 14, 'CHIPAQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(535, 14, 'CHOACHÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(536, 14, 'CHOCONTÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(537, 14, 'COGUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(538, 14, 'COTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(539, 14, 'CUCUNUBÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(540, 14, 'EL COLEGIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(541, 14, 'EL PEÑÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(542, 14, 'EL ROSAL1', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(543, 14, 'FACATATIVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(544, 14, 'FÓMEQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(545, 14, 'FOSCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(546, 14, 'FUNZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(547, 14, 'FÚQUENE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(548, 14, 'FUSAGASUGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(549, 14, 'GACHALÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(550, 14, 'GACHANCIPÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(551, 14, 'GACHETA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(552, 14, 'GAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(553, 14, 'GIRARDOT', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(554, 14, 'GRANADA2', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(555, 14, 'GUACHETÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(556, 14, 'GUADUAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(557, 14, 'GUASCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(558, 14, 'GUATAQUÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(559, 14, 'GUATAVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(560, 14, 'GUAYABAL DE SIQUIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(561, 14, 'GUAYABETAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(562, 14, 'GUTIÉRREZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(563, 14, 'JERUSALÉN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(564, 14, 'JUNÍN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(565, 14, 'LA CALERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(566, 14, 'LA MESA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(567, 14, 'LA PALMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(568, 14, 'LA PEÑA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(569, 14, 'LA VEGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(570, 14, 'LENGUAZAQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(571, 14, 'MACHETÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(572, 14, 'MADRID', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(573, 14, 'MANTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(574, 14, 'MEDINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(575, 14, 'MOSQUERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(576, 14, 'NARIÑO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(577, 14, 'NEMOCÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(578, 14, 'NILO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(579, 14, 'NIMAIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(580, 14, 'NOCAIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(581, 14, 'OSPINA PÉREZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(582, 14, 'PACHO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(583, 14, 'PAIME', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(584, 14, 'PANDI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(585, 14, 'PARATEBUENO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(586, 14, 'PASCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(587, 14, 'PUERTO SALGAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(588, 14, 'PULÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(589, 14, 'QUEBRADANEGRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(590, 14, 'QUETAME', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(591, 14, 'QUIPILE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(592, 14, 'RAFAEL REYES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(593, 14, 'RICAURTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(594, 14, 'SAN  ANTONIO DEL  TEQUENDAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(595, 14, 'SAN BERNARDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(596, 14, 'SAN CAYETANO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(597, 14, 'SAN FRANCISCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(598, 14, 'SAN JUAN DE RIOSECO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(599, 14, 'SASAIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(600, 14, 'SESQUILÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(601, 14, 'SIBATÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(602, 14, 'SILVANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(603, 14, 'SIMIJACA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(604, 14, 'SOACHA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(605, 14, 'SOPO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(606, 14, 'SUBACHOQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(607, 14, 'SUESCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(608, 14, 'SUPATÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(609, 14, 'SUSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(610, 14, 'SUTATAUSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(611, 14, 'TABIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(612, 14, 'TAUSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(613, 14, 'TENA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(614, 14, 'TENJO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(615, 14, 'TIBACUY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(616, 14, 'TIBIRITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(617, 14, 'TOCAIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(618, 14, 'TOCANCIPÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(619, 14, 'TOPAIPÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(620, 14, 'UBALÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(621, 14, 'UBAQUE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(622, 14, 'UBATÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(623, 14, 'UNE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(624, 14, 'UTICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(625, 14, 'VERGARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(626, 14, 'VIANI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(627, 14, 'VILLA GOMEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(628, 14, 'VILLA PINZÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(629, 14, 'VILLETA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(630, 14, 'VIOTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(631, 14, 'YACOPÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(632, 14, 'ZIPACÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(633, 14, 'ZIPAQUIRÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(634, 15, 'BARRANCO MINAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(635, 15, 'CACAHUAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(636, 15, 'INÍRIDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(637, 15, 'LA GUADALUPE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(638, 15, 'MAPIRIPANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(639, 15, 'MORICHAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(640, 15, 'PANA PANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(641, 15, 'PUERTO COLOMBIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(642, 15, 'SAN FELIPE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(643, 16, 'CALAMAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(644, 16, 'EL RETORNO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(645, 16, 'MIRAFLOREZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(646, 16, 'SAN JOSÉ DEL GUAVIARE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(647, 17, 'ACEVEDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(648, 17, 'AGRADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(649, 17, 'AIPE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(650, 17, 'ALGECIRAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(651, 17, 'ALTAMIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(652, 17, 'BARAYA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(653, 17, 'CAMPO ALEGRE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(654, 17, 'COLOMBIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(655, 17, 'ELIAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(656, 17, 'GARZÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(657, 17, 'GIGANTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(658, 17, 'GUADALUPE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(659, 17, 'HOBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(660, 17, 'IQUIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(661, 17, 'ISNOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(662, 17, 'LA ARGENTINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(663, 17, 'LA PLATA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(664, 17, 'NATAGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(665, 17, 'NEIVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(666, 17, 'OPORAPA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(667, 17, 'PAICOL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(668, 17, 'PALERMO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(669, 17, 'PALESTINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(670, 17, 'PITAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(671, 17, 'PITALITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(672, 17, 'RIVERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(673, 17, 'SALADO BLANCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(674, 17, 'SAN AGUSTÍN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(675, 17, 'SANTA MARIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(676, 17, 'SUAZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(677, 17, 'TARQUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(678, 17, 'TELLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(679, 17, 'TERUEL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(680, 17, 'TESALIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(681, 17, 'TIMANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(682, 17, 'VILLAVIEJA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(683, 17, 'YAGUARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(684, 18, 'ALBANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(685, 18, 'BARRANCAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(686, 18, 'DIBULLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(687, 18, 'DISTRACCIÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(688, 18, 'EL MOLINO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(689, 18, 'FONSECA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(690, 18, 'HATO NUEVO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(691, 18, 'LA JAGUA DEL PILAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(692, 18, 'MAICAO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(693, 18, 'MANAURE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(694, 18, 'RIOHACHA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(695, 18, 'SAN JUAN DEL CESAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(696, 18, 'URIBIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(697, 18, 'URUMITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(698, 18, 'VILLANUEVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(699, 19, 'ALGARROBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(700, 19, 'ARACATACA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(701, 19, 'ARIGUANI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(702, 19, 'CERRO SAN ANTONIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(703, 19, 'CHIVOLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(704, 19, 'CIENAGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(705, 19, 'CONCORDIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(706, 19, 'EL BANCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(707, 19, 'EL PIÑON', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(708, 19, 'EL RETEN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(709, 19, 'FUNDACION', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(710, 19, 'GUAMAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(711, 19, 'NUEVA GRANADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(712, 19, 'PEDRAZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(713, 19, 'PIJIÑO DEL CARMEN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(714, 19, 'PIVIJAY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(715, 19, 'PLATO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(716, 19, 'PUEBLO VIEJO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(717, 19, 'REMOLINO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(718, 19, 'SABANAS DE SAN ANGEL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(719, 19, 'SALAMINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(720, 19, 'SAN SEBASTIAN DE BUENAVISTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(721, 19, 'SAN ZENON', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(722, 19, 'SANTA ANA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(723, 19, 'SANTA BARBARA DE PINTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(724, 19, 'SANTA MARTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(725, 19, 'SITIONUEVO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(726, 19, 'TENERIFE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(727, 19, 'ZAPAYAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(728, 19, 'ZONA BANANERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(729, 20, 'ACACIAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(730, 20, 'BARRANCA DE UPIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(731, 20, 'CABUYARO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(732, 20, 'CASTILLA LA NUEVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(733, 20, 'CUBARRAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(734, 20, 'CUMARAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(735, 20, 'EL CALVARIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(736, 20, 'EL CASTILLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(737, 20, 'EL DORADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(738, 20, 'FUENTE DE ORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(739, 20, 'GRANADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(740, 20, 'GUAMAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(741, 20, 'LA MACARENA', '2017-08-03 18:32:08', '2017-08-03 18:32:08');
INSERT INTO `municipios` (`id`, `departamentos_id`, `nombre`, `created_at`, `updated_at`) VALUES
(742, 20, 'LA URIBE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(743, 20, 'LEJANÍAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(744, 20, 'MAPIRIPÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(745, 20, 'MESETAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(746, 20, 'PUERTO CONCORDIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(747, 20, 'PUERTO GAITÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(748, 20, 'PUERTO LLERAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(749, 20, 'PUERTO LÓPEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(750, 20, 'PUERTO RICO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(751, 20, 'RESTREPO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(752, 20, 'SAN  JUAN DE ARAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(753, 20, 'SAN CARLOS GUAROA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(754, 20, 'SAN JUANITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(755, 20, 'SAN MARTÍN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(756, 20, 'VILLAVICENCIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(757, 20, 'VISTA HERMOSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(758, 21, 'ALBAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(759, 21, 'ALDAÑA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(760, 21, 'ANCUYA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(761, 21, 'ARBOLEDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(762, 21, 'BARBACOAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(763, 21, 'BELEN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(764, 21, 'BUESACO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(765, 21, 'CHACHAGUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(766, 21, 'COLON (GENOVA)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(767, 21, 'CONSACA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(768, 21, 'CONTADERO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(769, 21, 'CORDOBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(770, 21, 'CUASPUD', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(771, 21, 'CUMBAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(772, 21, 'CUMBITARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(773, 21, 'EL CHARCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(774, 21, 'EL PEÑOL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(775, 21, 'EL ROSARIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(776, 21, 'EL TABLÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(777, 21, 'EL TAMBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(778, 21, 'FUNES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(779, 21, 'GUACHUCAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(780, 21, 'GUAITARILLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(781, 21, 'GUALMATAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(782, 21, 'ILES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(783, 21, 'IMUES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(784, 21, 'IPIALES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(785, 21, 'LA CRUZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(786, 21, 'LA FLORIDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(787, 21, 'LA LLANADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(788, 21, 'LA TOLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(789, 21, 'LA UNION', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(790, 21, 'LEIVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(791, 21, 'LINARES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(792, 21, 'LOS ANDES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(793, 21, 'MAGUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(794, 21, 'MALLAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(795, 21, 'MOSQUEZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(796, 21, 'NARIÑO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(797, 21, 'OLAYA HERRERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(798, 21, 'OSPINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(799, 21, 'PASTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(800, 21, 'PIZARRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(801, 21, 'POLICARPA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(802, 21, 'POTOSI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(803, 21, 'PROVIDENCIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(804, 21, 'PUERRES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(805, 21, 'PUPIALES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(806, 21, 'RICAURTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(807, 21, 'ROBERTO PAYAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(808, 21, 'SAMANIEGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(809, 21, 'SAN BERNARDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(810, 21, 'SAN LORENZO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(811, 21, 'SAN PABLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(812, 21, 'SAN PEDRO DE CARTAGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(813, 21, 'SANDONA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(814, 21, 'SANTA BARBARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(815, 21, 'SANTACRUZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(816, 21, 'SAPUYES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(817, 21, 'TAMINANGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(818, 21, 'TANGUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(819, 21, 'TUMACO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(820, 21, 'TUQUERRES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(821, 21, 'YACUANQUER', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(822, 22, 'ABREGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(823, 22, 'ARBOLEDAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(824, 22, 'BOCHALEMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(825, 22, 'BUCARASICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(826, 22, 'CÁCHIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(827, 22, 'CÁCOTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(828, 22, 'CHINÁCOTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(829, 22, 'CHITAGÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(830, 22, 'CONVENCIÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(831, 22, 'CÚCUTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(832, 22, 'CUCUTILLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(833, 22, 'DURANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(834, 22, 'EL CARMEN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(835, 22, 'EL TARRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(836, 22, 'EL ZULIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(837, 22, 'GRAMALOTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(838, 22, 'HACARI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(839, 22, 'HERRÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(840, 22, 'LA ESPERANZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(841, 22, 'LA PLAYA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(842, 22, 'LABATECA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(843, 22, 'LOS PATIOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(844, 22, 'LOURDES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(845, 22, 'MUTISCUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(846, 22, 'OCAÑA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(847, 22, 'PAMPLONA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(848, 22, 'PAMPLONITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(849, 22, 'PUERTO SANTANDER', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(850, 22, 'RAGONVALIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(851, 22, 'SALAZAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(852, 22, 'SAN CALIXTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(853, 22, 'SAN CAYETANO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(854, 22, 'SANTIAGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(855, 22, 'SARDINATA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(856, 22, 'SILOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(857, 22, 'TEORAMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(858, 22, 'TIBÚ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(859, 22, 'TOLEDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(860, 22, 'VILLA CARO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(861, 22, 'VILLA DEL ROSARIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(862, 23, 'COLÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(863, 23, 'MOCOA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(864, 23, 'ORITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(865, 23, 'PUERTO ASÍS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(866, 23, 'PUERTO CAYCEDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(867, 23, 'PUERTO GUZMÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(868, 23, 'PUERTO LEGUÍZAMO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(869, 23, 'SAN FRANCISCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(870, 23, 'SAN MIGUEL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(871, 23, 'SANTIAGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(872, 23, 'SIBUNDOY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(873, 23, 'VALLE DEL GUAMUEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(874, 23, 'VILLAGARZÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(875, 24, 'ARMENIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(876, 24, 'BUENAVISTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(877, 24, 'CALARCÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(878, 24, 'CIRCASIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(879, 24, 'CÓRDOBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(880, 24, 'FILANDIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(881, 24, 'GÉNOVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(882, 24, 'LA TEBAIDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(883, 24, 'MONTENEGRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(884, 24, 'PIJAO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(885, 24, 'QUIMBAYA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(886, 24, 'SALENTO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(887, 25, 'APIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(888, 25, 'BALBOA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(889, 25, 'BELÉN DE UMBRÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(890, 25, 'DOS QUEBRADAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(891, 25, 'GUATICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(892, 25, 'LA CELIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(893, 25, 'LA VIRGINIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(894, 25, 'MARSELLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(895, 25, 'MISTRATO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(896, 25, 'PEREIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(897, 25, 'PUEBLO RICO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(898, 25, 'QUINCHÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(899, 25, 'SANTA ROSA DE CABAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(900, 25, 'SANTUARIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(901, 26, 'PROVIDENCIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(902, 26, 'SAN ANDRES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(903, 26, 'SANTA CATALINA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(904, 27, 'AGUADA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(905, 27, 'ALBANIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(906, 27, 'ARATOCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(907, 27, 'BARBOSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(908, 27, 'BARICHARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(909, 27, 'BARRANCABERMEJA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(910, 27, 'BETULIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(911, 27, 'BOLÍVAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(912, 27, 'BUCARAMANGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(913, 27, 'CABRERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(914, 27, 'CALIFORNIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(915, 27, 'CAPITANEJO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(916, 27, 'CARCASI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(917, 27, 'CEPITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(918, 27, 'CERRITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(919, 27, 'CHARALÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(920, 27, 'CHARTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(921, 27, 'CHIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(922, 27, 'CHIPATÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(923, 27, 'CIMITARRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(924, 27, 'CONCEPCIÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(925, 27, 'CONFINES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(926, 27, 'CONTRATACIÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(927, 27, 'COROMORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(928, 27, 'CURITÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(929, 27, 'EL CARMEN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(930, 27, 'EL GUACAMAYO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(931, 27, 'EL PEÑÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(932, 27, 'EL PLAYÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(933, 27, 'ENCINO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(934, 27, 'ENCISO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(935, 27, 'FLORIÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(936, 27, 'FLORIDABLANCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(937, 27, 'GALÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(938, 27, 'GAMBITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(939, 27, 'GIRÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(940, 27, 'GUACA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(941, 27, 'GUADALUPE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(942, 27, 'GUAPOTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(943, 27, 'GUAVATÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(944, 27, 'GUEPSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(945, 27, 'HATO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(946, 27, 'JESÚS MARIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(947, 27, 'JORDÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(948, 27, 'LA BELLEZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(949, 27, 'LA PAZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(950, 27, 'LANDAZURI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(951, 27, 'LEBRIJA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(952, 27, 'LOS SANTOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(953, 27, 'MACARAVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(954, 27, 'MÁLAGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(955, 27, 'MATANZA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(956, 27, 'MOGOTES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(957, 27, 'MOLAGAVITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(958, 27, 'OCAMONTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(959, 27, 'OIBA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(960, 27, 'ONZAGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(961, 27, 'PALMAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(962, 27, 'PALMAS DEL SOCORRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(963, 27, 'PÁRAMO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(964, 27, 'PIEDECUESTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(965, 27, 'PINCHOTE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(966, 27, 'PUENTE NACIONAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(967, 27, 'PUERTO PARRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(968, 27, 'PUERTO WILCHES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(969, 27, 'RIONEGRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(970, 27, 'SABANA DE TORRES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(971, 27, 'SAN ANDRÉS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(972, 27, 'SAN BENITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(973, 27, 'SAN GIL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(974, 27, 'SAN JOAQUÍN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(975, 27, 'SAN JOSÉ DE MIRANDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(976, 27, 'SAN MIGUEL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(977, 27, 'SAN VICENTE DE CHUCURÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(978, 27, 'SANTA BÁRBARA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(979, 27, 'SANTA HELENA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(980, 27, 'SIMACOTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(981, 27, 'SOCORRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(982, 27, 'SUAITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(983, 27, 'SUCRE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(984, 27, 'SURATA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(985, 27, 'TONA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(986, 27, 'VALLE SAN JOSÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(987, 27, 'VÉLEZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(988, 27, 'VETAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(989, 27, 'VILLANUEVA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(990, 27, 'ZAPATOCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(991, 28, 'BUENAVISTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(992, 28, 'CAIMITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(993, 28, 'CHALÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(994, 28, 'COLOSO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(995, 28, 'COROZAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(996, 28, 'EL ROBLE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(997, 28, 'GALERAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(998, 28, 'GUARANDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(999, 28, 'LA UNIÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1000, 28, 'LOS PALMITOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1001, 28, 'MAJAGUAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1002, 28, 'MORROA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1003, 28, 'OVEJAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1004, 28, 'PALMITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1005, 28, 'SAMPUES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1006, 28, 'SAN BENITO ABAD', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1007, 28, 'SAN JUAN DE BETULIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1008, 28, 'SAN MARCOS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1009, 28, 'SAN ONOFRE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1010, 28, 'SAN PEDRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1011, 28, 'SINCÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1012, 28, 'SINCELEJO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1013, 28, 'SUCRE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1014, 28, 'TOLÚ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1015, 28, 'TOLUVIEJO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1016, 29, 'ALPUJARRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1017, 29, 'ALVARADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1018, 29, 'AMBALEMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1019, 29, 'ANZOATEGUI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1020, 29, 'ARMERO (GUAYABAL)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1021, 29, 'ATACO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1022, 29, 'CAJAMARCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1023, 29, 'CARMEN DE APICALÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1024, 29, 'CASABIANCA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1025, 29, 'CHAPARRAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1026, 29, 'COELLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1027, 29, 'COYAIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1028, 29, 'CUNDAY', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1029, 29, 'DOLORES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1030, 29, 'ESPINAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1031, 29, 'FALÁN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1032, 29, 'FLANDES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1033, 29, 'FRESNO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1034, 29, 'GUAMO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1035, 29, 'HERVEO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1036, 29, 'HONDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1037, 29, 'IBAGUÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1038, 29, 'ICONONZO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1039, 29, 'LÉRIDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1040, 29, 'LÍBANO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1041, 29, 'MARIQUITA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1042, 29, 'MELGAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1043, 29, 'MURILLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1044, 29, 'NATAGAIMA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1045, 29, 'ORTEGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1046, 29, 'PALOCABILDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1047, 29, 'PIEDRAS PLANADAS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1048, 29, 'PRADO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1049, 29, 'PURIFICACIÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1050, 29, 'RIOBLANCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1051, 29, 'RONCESVALLES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1052, 29, 'ROVIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1053, 29, 'SALDAÑA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1054, 29, 'SAN ANTONIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1055, 29, 'SAN LUIS', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1056, 29, 'SANTA ISABEL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1057, 29, 'SUÁREZ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1058, 29, 'VALLE DE SAN JUAN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1059, 29, 'VENADILLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1060, 29, 'VILLAHERMOSA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1061, 29, 'VILLARRICA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1062, 30, 'ALCALÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1063, 30, 'ANDALUCÍA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1064, 30, 'ANSERMA NUEVO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1065, 30, 'ARGELIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1066, 30, 'BOLÍVAR', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1067, 30, 'BUENAVENTURA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1068, 30, 'BUGA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1069, 30, 'BUGALAGRANDE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1070, 30, 'CAICEDONIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1071, 30, 'CALI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1072, 30, 'CALIMA (DARIEN)', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1073, 30, 'CANDELARIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1074, 30, 'CARTAGO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1075, 30, 'DAGUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1076, 30, 'EL AGUILA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1077, 30, 'EL CAIRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1078, 30, 'EL CERRITO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1079, 30, 'EL DOVIO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1080, 30, 'FLORIDA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1081, 30, 'GUACARI', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1082, 30, 'JAMUNDÍ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1083, 30, 'LA CUMBRE', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1084, 30, 'LA UNIÓN', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1085, 30, 'LA VICTORIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1086, 30, 'OBANDO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1087, 30, 'PALMIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1088, 30, 'PRADERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1089, 30, 'RESTREPO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1090, 30, 'RIO FRÍO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1091, 30, 'ROLDANILLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1092, 30, 'SAN PEDRO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1093, 30, 'SEVILLA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1094, 30, 'TORO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1095, 30, 'TRUJILLO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1096, 30, 'TULUÁ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1097, 30, 'ULLOA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1098, 30, 'VERSALLES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1099, 30, 'VIJES', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1100, 30, 'YOTOCO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1101, 30, 'YUMBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1102, 30, 'ZARZAL', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1103, 31, 'CARURÚ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1104, 31, 'MITÚ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1105, 31, 'PACOA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1106, 31, 'PAPUNAUA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1107, 31, 'TARAIRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1108, 31, 'YAVARATÉ', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1109, 32, 'CUMARIBO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1110, 32, 'LA PRIMAVERA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1111, 32, 'PUERTO CARREÑO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1112, 32, 'SANTA ROSALIA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1113, 30, 'AMAIME', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1114, 30, 'ROZO', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1115, 30, 'CALI - PLANTA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1116, 30, 'VILLA GORGONA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1117, 30, 'GINEBRA', '2017-08-03 18:32:08', '2017-08-03 18:32:08'),
(1119, 30, 'LA PAILA', '2017-08-03 18:32:08', '2017-08-03 18:32:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ID_menu` int(10) UNSIGNED NOT NULL,
  `ID_rol` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `ID_menu`, `ID_rol`, `created_at`, `updated_at`) VALUES
(30, 16, 1, '2018-02-21 15:15:36', '2018-02-21 15:15:36'),
(32, 1, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(33, 2, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(34, 3, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(35, 10, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(36, 5, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(37, 11, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(38, 12, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(39, 13, 1, '2018-02-21 15:15:37', '2018-02-21 15:15:37'),
(44, 206, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(46, 208, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(50, 212, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(51, 213, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(52, 214, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(53, 215, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(54, 217, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(56, 216, 1, '2018-05-09 16:50:44', '2018-05-09 16:50:44'),
(57, 218, 1, '2018-05-09 16:50:45', '2018-05-09 16:50:45'),
(60, 219, 1, '2018-06-22 15:54:28', '2018-06-22 15:54:28'),
(61, 223, 1, '2018-09-25 14:39:38', '2018-09-25 14:39:38'),
(62, 224, 1, '2018-09-27 13:03:41', '2018-09-27 13:03:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `habilitado` tinyint(1) DEFAULT '1' COMMENT '(0) Deshabilitado\n(1) Habilitado',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 1, '2017-01-19 00:25:34', '2017-01-19 02:24:44'),
(2, 'Gerencia', 1, '2017-01-19 00:25:42', '2017-01-19 00:25:42'),
(4, 'Calidad', 1, '2018-02-22 13:14:56', '2018-02-22 13:14:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `ID_rol` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `habilitado` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ID_rol`, `name`, `email`, `password`, `remember_token`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 1, 'Administrador', 'admin@correo.com', '$2y$10$s5nsUy6OmN/l5m2EFNnGweJY3AoMU0U0aNdSYHH4XAwqPh/0.aqI2', 'xlaicWSve5WzUktQmA4PslHF94rY1v0kivk5e0bfydW4VAY8OtoOG1FEGJ2A', 1, '2017-01-07 01:56:09', '2018-09-27 13:04:33'),
(2, 2, 'Julie Pulgarin', 'asistente@proalba.com.co', '$2y$10$tQcVg65OEh8fU83Z0cXm.u3ZEvRENcs18wRn3/sDCCOGxsNy5txtW', NULL, 0, '2017-01-17 01:29:58', '2017-01-18 02:41:21');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compras_productos`
--
ALTER TABLE `compras_productos`
  ADD PRIMARY KEY (`id`,`compras_tipo_producto_id`,`general_unidad_medida_id`),
  ADD KEY `fk_foods_compras_productos_foods_compras_tipo_producto1_idx` (`compras_tipo_producto_id`),
  ADD KEY `fk_compras_productos_general_unidad_medida1_idx` (`general_unidad_medida_id`);

--
-- Indices de la tabla `compras_tipo_producto`
--
ALTER TABLE `compras_tipo_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`,`municipios_id`),
  ADD KEY `fk_empresa_municipios1_idx` (`municipios_id`);

--
-- Indices de la tabla `general_unidad_medida`
--
ALTER TABLE `general_unidad_medida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`,`compras_tipo_producto_id`,`compras_productos_id`,`inventario_bodegas_id`,`users_id`,`roles_id`),
  ADD KEY `fk_inventario_users1_idx` (`users_id`),
  ADD KEY `fk_inventario_roles1_idx` (`roles_id`),
  ADD KEY `fk_inventario_inventario_bodegas1_idx` (`inventario_bodegas_id`),
  ADD KEY `fk_inventario_compras_productos1_idx` (`compras_productos_id`),
  ADD KEY `fk_inventario_compras_tipo_producto1_idx` (`compras_tipo_producto_id`);

--
-- Indices de la tabla `inventario_bodegas`
--
ALTER TABLE `inventario_bodegas`
  ADD PRIMARY KEY (`id`,`inventario_tipo_almacenamiento_id`),
  ADD KEY `fk_inventario_bodegas_inventario_tipo_almacenamiento1_idx` (`inventario_tipo_almacenamiento_id`);

--
-- Indices de la tabla `inventario_configuracion_productos`
--
ALTER TABLE `inventario_configuracion_productos`
  ADD PRIMARY KEY (`id`,`compras_productos_id`),
  ADD KEY `fk_inventario_configuracion_productos_compras_productos1_idx` (`compras_productos_id`);

--
-- Indices de la tabla `inventario_motivos_movimiento`
--
ALTER TABLE `inventario_motivos_movimiento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario_movimientos`
--
ALTER TABLE `inventario_movimientos`
  ADD PRIMARY KEY (`id`,`inventario_id`,`compras_tipo_producto_id`,`compras_productos_id`,`inventario_bodegas_id`,`inventario_motivos_movimiento_id`,`users_id`,`roles_id`),
  ADD KEY `fk_inventario_movimientos_inventario1_idx` (`inventario_id`),
  ADD KEY `fk_inventario_movimientos_users1_idx` (`users_id`),
  ADD KEY `fk_inventario_movimientos_roles1_idx` (`roles_id`),
  ADD KEY `fk_inventario_movimientos_inventario_motivos_movimiento1_idx` (`inventario_motivos_movimiento_id`),
  ADD KEY `fk_inventario_movimientos_inventario_bodegas1_idx` (`inventario_bodegas_id`),
  ADD KEY `fk_inventario_movimientos_inventario_bodegas2_idx` (`inventario_bodegas_id_destino`);

--
-- Indices de la tabla `inventario_tipo_almacenamiento`
--
ALTER TABLE `inventario_tipo_almacenamiento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`,`departamentos_id`),
  ADD KEY `fk_municipios_departamentos1_idx` (`departamentos_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`,`ID_menu`,`ID_rol`),
  ADD KEY `fk_permisos_menu1_idx` (`ID_menu`),
  ADD KEY `fk_permisos_roles1_idx` (`ID_rol`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`ID_rol`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `fk_users_roles1_idx` (`ID_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `compras_productos`
--
ALTER TABLE `compras_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `compras_tipo_producto`
--
ALTER TABLE `compras_tipo_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `general_unidad_medida`
--
ALTER TABLE `general_unidad_medida`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventario_bodegas`
--
ALTER TABLE `inventario_bodegas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `inventario_configuracion_productos`
--
ALTER TABLE `inventario_configuracion_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `inventario_motivos_movimiento`
--
ALTER TABLE `inventario_motivos_movimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `inventario_movimientos`
--
ALTER TABLE `inventario_movimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventario_tipo_almacenamiento`
--
ALTER TABLE `inventario_tipo_almacenamiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1120;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras_productos`
--
ALTER TABLE `compras_productos`
  ADD CONSTRAINT `fk_compras_productos_general_unidad_medida1` FOREIGN KEY (`general_unidad_medida_id`) REFERENCES `general_unidad_medida` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_foods_compras_productos_foods_compras_tipo_producto1` FOREIGN KEY (`compras_tipo_producto_id`) REFERENCES `compras_tipo_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `fk_empresa_municipios1` FOREIGN KEY (`municipios_id`) REFERENCES `municipios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `fk_inventario_compras_productos1` FOREIGN KEY (`compras_productos_id`) REFERENCES `compras_productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_compras_tipo_producto1` FOREIGN KEY (`compras_tipo_producto_id`) REFERENCES `compras_tipo_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_inventario_bodegas1` FOREIGN KEY (`inventario_bodegas_id`) REFERENCES `inventario_bodegas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_roles1` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inventario_bodegas`
--
ALTER TABLE `inventario_bodegas`
  ADD CONSTRAINT `fk_inventario_bodegas_inventario_tipo_almacenamiento1` FOREIGN KEY (`inventario_tipo_almacenamiento_id`) REFERENCES `inventario_tipo_almacenamiento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inventario_configuracion_productos`
--
ALTER TABLE `inventario_configuracion_productos`
  ADD CONSTRAINT `fk_inventario_configuracion_productos_compras_productos1` FOREIGN KEY (`compras_productos_id`) REFERENCES `compras_productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inventario_movimientos`
--
ALTER TABLE `inventario_movimientos`
  ADD CONSTRAINT `fk_inventario_movimientos_inventario1` FOREIGN KEY (`inventario_id`) REFERENCES `inventario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_movimientos_inventario_bodegas1` FOREIGN KEY (`inventario_bodegas_id`) REFERENCES `inventario_bodegas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_movimientos_inventario_bodegas2` FOREIGN KEY (`inventario_bodegas_id_destino`) REFERENCES `inventario_bodegas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_movimientos_inventario_motivos_movimiento1` FOREIGN KEY (`inventario_motivos_movimiento_id`) REFERENCES `inventario_motivos_movimiento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_movimientos_roles1` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventario_movimientos_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD CONSTRAINT `fk_municipios_departamentos1` FOREIGN KEY (`departamentos_id`) REFERENCES `departamentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD CONSTRAINT `fk_permisos_menu1` FOREIGN KEY (`ID_menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_permisos_roles1` FOREIGN KEY (`ID_rol`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_roles1` FOREIGN KEY (`ID_rol`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
