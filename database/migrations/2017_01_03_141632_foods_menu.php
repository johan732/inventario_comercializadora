<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FoodsMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('foods_menu', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre');
            $table->string('src');
            $table->integer('orden');
            $table->string('icon');
            $table->integer('id_padre');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods_menu');
        //
    }
}
